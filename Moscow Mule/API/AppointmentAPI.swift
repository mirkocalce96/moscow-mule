//
//  AppointmentAPI.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 22/02/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class AppointmentAPI {

    fileprivate let coreDataController: CoreDataController!
    fileprivate var context: NSManagedObjectContext!
    fileprivate let notificationAPI: NotificationAPI!
    
    class var sharedInstance: AppointmentAPI {
        struct Singleton {
            static let instance = AppointmentAPI()
        }

        return Singleton.instance
    }

    
    init() {
        print("\(Date()) | Class: AppointmentAPI | Function: init() | Input: void | Status: Started")
        
        self.coreDataController = CoreDataController.sharedInstance
        self.context = coreDataController.getContext()
        self.notificationAPI = NotificationAPI.sharedInstance
        
        print("\(Date()) | Class: AppointmentAPI | Function: init() | Input: void | Status: Finished | Return: void")
    }

    func create(attributes: AppointmentAttributes) -> Appointment? {
        let identifier: String
        
        if attributes.identifier == nil {
            identifier = IdentifierAPI.sharedInstance.generateAppointment()
        } else {
            identifier = attributes.identifier!
        }
        print("\(Date()) | Class: PersonAPI | Function: create() | Input: \(identifier) | Status: Started")

        print("\(Date()) | Class: AppointmentAPI | Function: create() | Input: \(identifier) | Status: Started")
        
        var check = true
        guard let entity = NSEntityDescription.entity(forEntityName: "Appointment", in: self.context) else { return nil }
        let new = Appointment(entity: entity, insertInto: self.context)
    
        new.setValue(attributes.checked, forKey: "checked")
        new.setValue(attributes.date, forKey: "date")
        new.setValue(identifier, forKey: "identifier")
        new.setValue(attributes.name, forKey: "name")
        new.setValue(attributes.notes, forKey: "notes")
        new.setValue(attributes.place, forKey: "place")
        new.setValue(attributes.notifications, forKey: "notifications")
        new.setValue(attributes.alerts, forKey: "alerts")
        
        attributes.person!.addToAppointments(new)
        
        do {
            try self.context.save()
            
            check = notificationAPI.createAppointment(appointment: new)
            
            print("\(Date()) | Class: AppointmentAPI | Function: create() | Input: \(identifier) | Status: Finished | Return: \(check)")
        } catch let errore {
            print("\(Date()) | Class: AppointmentAPI | Function: create() | Input: \(identifier) | Status: Error | Description: \n\(errore)\n")
            
            return nil
        }
        
        return new
    }
    
    func update(attributes: AppointmentAttributes, appointment: Appointment) -> Appointment? {
        print("\(Date()) | Class: AppointmentAPI | Function: update() | Input: \(appointment.identifier!) | Status: Started")
        
        var check = notificationAPI.deleteAppointment(appointment: appointment)
        
        appointment.setValue(attributes.checked, forKey: "checked")
        appointment.setValue(attributes.date, forKey: "date")
        appointment.setValue(attributes.name, forKey: "name")
        appointment.setValue(attributes.notes, forKey: "notes")
        appointment.setValue(attributes.place, forKey: "place")
        appointment.setValue(attributes.notifications, forKey: "notifications")
        appointment.setValue(attributes.alerts, forKey: "alerts")
        appointment.setValue(attributes.person, forKey: "person")
        
        do {
            try self.context.save()

            check = notificationAPI.createAppointment(appointment: appointment)
            
            print("\(Date()) | Class: AppointmentAPI | Function: update() | Input: \(appointment.identifier!) | Status: Finished | Return: \(check)")
        } catch let errore {
            print("\(Date()) | Class: AppointmentAPI | Function: update() | Input: \(appointment.identifier!) | Status: Error | Description: \n\(errore)\n")
            
            return nil
        }
        
        return appointment
    }
    
    func read(id: String) -> Appointment? {
        print("\(Date()) | Class: AppointmentAPI | Function: read() | Input: \(id) | Status: Started")
        
        var check: Appointment? = nil
        let fetchRequest: NSFetchRequest<Appointment> = Appointment.fetchRequest()
        let predicate = NSPredicate(format: "identifier = %@", id)  //per prendere tutti quelli con l'id dato in input
        
        fetchRequest.predicate = predicate
        
        do {
            var array: [Appointment] = []
            array = try self.context.fetch(fetchRequest)

            if (array.count > 0) {
                check = array[0]
            }

            print("\(Date()) | Class: AppointmentAPI | Function: read() | Input: \(id) | Status: Finished | Return: \(String(describing: check))")
        } catch let errore {
            print("\(Date()) | Class: AppointmentAPI | Function: read() | Input: \(id) | Status: Error | Description: \n\(errore)\n")
        }
        
        return check
    }
    
    func delete(id: String) -> Bool {
        print("\(Date()) | Class: AppointmentAPI | Function: delete() | Input: \(id) | Status: Started")
        
        var check = true
        let fetchRequest: NSFetchRequest<Appointment> = Appointment.fetchRequest()
        
        fetchRequest.predicate = NSPredicate.init(format: "identifier = %@", id)
        
        if let result = try? context.fetch(fetchRequest) {
            for x in result {
                if !notificationAPI.deleteAppointment(appointment: x) {
                    check = false
                }
                
                context.delete(x)
            }
            
            do {
                try self.context.save()
            } catch let errore {
                check = false
                
                print("\(Date()) | Class: AppointmentAPI | Function: delete() | Input: \(id) | Status: Error | Description: \n\(errore)\n")
            }
        }
        
        print("\(Date()) | Class: AppointmentAPI | Function: delete() | Input: \(id) | Status: Finished | Return: \(check)")
        
        return check
    }
    
    func getAll() -> [Appointment] {
        print("\(Date()) | Class: AppointmentAPI | Function: getAll() | Input: void | Status: Started")
        
        var array: [Appointment] = []
        let fetchRequest: NSFetchRequest<Appointment> = Appointment.fetchRequest()
        
        do {
            array = try self.context.fetch(fetchRequest)
            array = array.sorted() {$0.date! < $1.date!}

            print("\(Date()) | Class: AppointmentAPI | Function: getAll() | Input: void | Status: Finished | Return: \(array.count) elements")
        } catch let errore {
            print("\(Date()) | Class: AppointmentAPI | Function: getAll() | Input: void | Status: Error | Description: \n\(errore)\n")
        }
        
        return array
    }
    
    func getAllNotChecked() -> [Appointment] {
        print("\(Date()) | Class: AppointmentAPI | Function: getAllNotChecked() | Input: void | Status: Started")
        
        var array: [Appointment] = []
        let fetchRequest: NSFetchRequest<Appointment> = Appointment.fetchRequest()
        
        fetchRequest.predicate = NSPredicate.init(format: "checked = false")
        
        do {
            array = try self.context.fetch(fetchRequest)
            array = array.sorted() {$0.date! < $1.date!}

            print("\(Date()) | Class: AppointmentAPI | Function: getAllNotChecked() | Input: void | Status: Finished | Return: \(array.count) elements")
        } catch let errore {
            print("\(Date()) | Class: AppointmentAPI | Function: getAllNotChecked() | Input: void | Status: Error | Description: \n\(errore)\n")
        }
        
        return array
    }
    
    func getAllNotCheckedInAWeek() -> Int {
        print("\(Date()) | Class: AppointmentAPI | Function: getAllNotCheckedInAWeek() | Input: void | Status: Started")
        
        var array: [Appointment] = []
        let fetchRequest: NSFetchRequest<Appointment> = Appointment.fetchRequest()
        let calendar = NSCalendar.current
        let endDay = calendar.date(byAdding: .day, value: 6, to: Date(), wrappingComponents: true)
        let startDate = calendar.date(bySettingHour: 0, minute: 0, second: 0, of: Date())
        let endDate = calendar.date(bySettingHour: 23, minute: 59, second: 59, of: endDay!)
        
        fetchRequest.predicate = NSPredicate.init(format: "checked = false and %@ >= date and %@ <= date", endDate! as CVarArg, startDate! as CVarArg)

        do {
            array = try self.context.fetch(fetchRequest)

            print("\(Date()) | Class: AppointmentAPI | Function: getAllNotCheckedInAWeek() | Input: void | Status: Finished | Return: \(array.count) elements")
        } catch let errore {
            print("\(Date()) | Class: AppointmentAPI | Function: getAllNotCheckedInAWeek() | Input: void | Status: Error | Description: \n\(errore)\n")
        }
        
        return array.count
    }
    
    func getAllNotCheckedOfPerson(id: String) -> [Appointment] {
        print("\(Date()) | Class: AppointmentAPI | Function: getAllNotCheckedOfPerson() | Input: \(id) | Status: Started")
        
        var array: [Appointment] = []
        let fetchRequest: NSFetchRequest<Appointment> = Appointment.fetchRequest()
        
        fetchRequest.predicate = NSPredicate.init(format: "person.personId = %@ and checked = false", id)
        
        do {
            array = try self.context.fetch(fetchRequest)
            array = array.sorted() {$0.date! < $1.date!}

            print("\(Date()) | Class: AppointmentAPI | Function: getAllNotCheckedOfPerson() | Input: \(id) | Status: Finished | Return: \(array.count) elements")
        } catch let errore {
            print("\(Date()) | Class: AppointmentAPI | Function: getAllNotCheckedOfPerson() | Input: \(id) | Status: Error | Description: \n\(errore)\n")
        }
        
        return array
    }
    
    func getAllInDayOfPerson(id: String, date: Date) -> [Appointment] {
        print("\(Date()) | Class: AppointmentAPI | Function: getAllInDayOfPerson() | Input: \(id) | Status: Started")
        
        var array: [Appointment] = []
        let fetchRequest: NSFetchRequest<Appointment> = Appointment.fetchRequest()
        let calendar = NSCalendar.current
        let startDate = calendar.date(bySettingHour: 0, minute: 0, second: 0, of: date)
        let endDate = calendar.date(bySettingHour: 23, minute: 59, second: 59, of: date)
        
        fetchRequest.predicate = NSPredicate.init(format: "person.personId = %@ and %@ >= date and %@ <= date", id, endDate! as CVarArg, startDate! as CVarArg)
        
        do {
            array = try self.context.fetch(fetchRequest)
            array = array.sorted() {$0.date! < $1.date!}

            print("\(Date()) | Class: AppointmentAPI | Function: getAllInDayOfPerson() | Input: \(id) | Status: Finished | Return: \(array.count) elements")
        } catch let errore {
            print("\(Date()) | Class: AppointmentAPI | Function: getAllInDayOfPerson() | Input: \(id) | Status: Error | Description: \n\(errore)\n")
        }
        
        return array
    }
    
    func getAllOfPerson(id: String) -> [Appointment] {
        print("\(Date()) | Class: AppointmentAPI | Function: getAllOfPerson() | Input: \(id) | Status: Started")
        
        var array: [Appointment] = []
        let fetchRequest: NSFetchRequest<Appointment> = Appointment.fetchRequest()
        
        fetchRequest.predicate = NSPredicate.init(format: "person.personId = %@", id)
        
        do {
            array = try self.context.fetch(fetchRequest)
            array = array.sorted() {$0.date! < $1.date!}

            print("\(Date()) | Class: AppointmentAPI | Function: getAllOfPerson() | Input: \(id) | Status: Finished | Return: \(array.count) elements")
        } catch let errore {
            print("\(Date()) | Class: AppointmentAPI | Function: getAllOfPerson() | Input: \(id) | Status: Error | Description: \n\(errore)\n")
        }
        
        return array
    }
    
    func getAllByFilter(filter: String) -> [Appointment] {
        print("\(Date()) | Class: AppointmentAPI | Function: getAllByFilter() | Input: \(filter) | Status: Started")
        
        var array: [Appointment] = []
        let fetchRequest: NSFetchRequest<Appointment> = Appointment.fetchRequest()
        
        fetchRequest.predicate = NSPredicate.init(format: "person.name contains[c] %@ OR name contains[c] %@ OR notes contains[c] %@ OR place contains[c] %@", filter, filter, filter, filter)
        
        do {
            array = try self.context.fetch(fetchRequest)
            array = array.sorted() {$0.date! < $1.date!}

            print("\(Date()) | Class: AppointmentAPI | Function: getAllByFilter() | Input: \(filter) | Status: Finished | Return: \(array.count) elements")
        } catch let errore {
            print("\(Date()) | Class: AppointmentAPI | Function: getAllByFilter() | Input: \(filter) | Status: Error | Description: \n\(errore)\n")
        }
        
        return array
    }

    func check(id: String) -> Bool {
        print("\(Date()) | Class: AppointmentAPI | Function: check() | Input: \(id) | Status: Started")
        
        var check = true
        let fetchRequest: NSFetchRequest<Appointment> = Appointment.fetchRequest()
        let predicate = NSPredicate(format: "identifier = %@", id)  //per prendere tutti quelli con l'id dato in input
        
        fetchRequest.predicate = predicate
        
        do {
            var array: [Appointment] = []
            array = try self.context.fetch(fetchRequest)

            if (array.count > 0) {
                if (array[0].checked == true) {
                    check = notificationAPI.createAppointment(appointment: array[0])
                    array[0].setValue(false, forKey: "checked")
                } else {
                    check = notificationAPI.deleteAppointment(appointment: array[0])
                    array[0].setValue(true, forKey: "checked")
                }
            }
            
            try self.context.save()

            print("\(Date()) | Class: AppointmentAPI | Function: check() | Input: \(id) | Status: Finished | Return: \(check)")
        } catch let errore {
            check = false
            
            print("\(Date()) | Class: AppointmentAPI | Function: check() | Input: \(id) | Status: Error | Description: \n\(errore)\n")
        }
        
        return check
    }
    
    func appointmentNumber() -> Int {
        let fetchRequest: NSFetchRequest<Appointment> = Appointment.fetchRequest()
        do {
            let count = try self.context.count(for:fetchRequest)
            return count
        } catch let error as NSError {
            print("Error: \(error.localizedDescription)")
            return 0
        }
    }
}
