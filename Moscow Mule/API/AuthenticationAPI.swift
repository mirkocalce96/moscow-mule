//
//  AuthenticationAPI.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 16/07/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import CoreData
import Foundation
import UIKit

class AuthenticationAPI {
    
    fileprivate let coreDataController: CoreDataController!
    fileprivate var context: NSManagedObjectContext!
    
    class var sharedInstance: AuthenticationAPI {
        struct Singleton {
            static let instance = AuthenticationAPI()
        }

        return Singleton.instance
    }

    
    init() {
        print("\(Date()) | Class: AuthenticationAPI | Function: init() | Input: void | Status: Started")
        
        self.coreDataController = CoreDataController.sharedInstance
        self.context = coreDataController.getContext()
        
        print("\(Date()) | Class: AuthenticationAPI | Function: init() | Input: void | Status: Finished | Return: void")
    }
    
    func login(username: String, password: String, token: String) -> Authentication? {
        print("\(Date()) | Class: AuthenticationAPI | Function: login() | Input: void | Status: Started")
        guard let entity = NSEntityDescription.entity(forEntityName: "Authentication", in: self.context) else { return nil }
        let new = Authentication(entity: entity, insertInto: self.context)
        let identifier = "authentication"
        
        new.setValue(username, forKey: "username")
        new.setValue(password, forKey: "password")
        new.setValue(token, forKey: "token")
        new.setValue(identifier, forKey: "identifier")
        
        do {
            try self.context.save()
            
            authentication = new
            
            print("\(Date()) | Class: AuthenticationAPI | Function: login() | Input:  | Status: Finished | Return: \(new.username!)")
        } catch let errore {
            print("\(Date()) | Class: AuthenticationAPI | Function: login() | Input:  | Status: Error | Description: \n\(errore)\n")
            
            return nil
        }
        
        return new
    }
    
    func logout() {
        print("\(Date()) | Class: AuthenticationAPI | Function: logout() | Input: void | Status: Started")
        
        var check = true
        let fetchRequest: NSFetchRequest<Authentication> = Authentication.fetchRequest()
        let identifier = "authentication"
        
        fetchRequest.predicate = NSPredicate.init(format: "identifier = %@", identifier)
        
        if let result = try? context.fetch(fetchRequest) {
            for x in result {
                context.delete(x)
            }
            
            do {
                try self.context.save()
                
                authentication = nil
                synchronizationAPI.deleteAll()
                synchronizationRest.setOffBackgroundSynchronization()
                
            } catch let errore {
                check = false
                
                print("\(Date()) | Class: AuthenticationAPI | Function: logout() | Input: void | Status: Error | Description: \n\(errore)\n")
            }
        }
        
        print("\(Date()) | Class: AuthenticationAPI | Function: logout() | Input: void | Status: Finished | Return: \(check)")
    }
    
    func read() -> Authentication? {
        print("\(Date()) | Class: AuthenticationAPI | Function: isAuthenticated() | Input: void | Status: Started")
        
        var check: Authentication? = nil
        let fetchRequest: NSFetchRequest<Authentication> = Authentication.fetchRequest()
        let identifier = "authentication"
        
        fetchRequest.predicate = NSPredicate.init(format: "identifier = %@", identifier)
        
        if let result = try? context.fetch(fetchRequest) {
            if !result.isEmpty {
                check = result.first
            }
        }
        
        print("\(Date()) | Class: AuthenticationAPI | Function: isAuthenticated() | Input: void | Status: Finished | Return: \(String(describing: check))")
        
        return check
    }
    
    func isAuthenticated() -> Bool {
        return (authentication != nil)
    }
    
}
