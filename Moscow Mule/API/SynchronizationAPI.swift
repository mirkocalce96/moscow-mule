//
//  SynchronizationAPI.swift
//  Moscow Mule
//
//  Created by Luigi Ascione on 15/07/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class SynchronizationAPI {
    
    fileprivate let coreDataController: CoreDataController!
    fileprivate var context: NSManagedObjectContext!
    
    class var sharedInstance: SynchronizationAPI {
        struct Singleton {
            static let instance = SynchronizationAPI()
        }

        return Singleton.instance
    }
    
    init() {
        
        self.coreDataController = CoreDataController.sharedInstance
        self.context = coreDataController.getContext()
        
    }
    
    func create(id: String, op: Int) {
        print("\(Date()) | Class: SyncronizationAPI | Function: create() | Input: \(id), \(op)| Status: Started")
        var check = true
        guard let entity = NSEntityDescription.entity(forEntityName: "Synchronization", in: self.context) else { return }
        let new = NSManagedObject(entity: entity, insertInto: self.context)

        guard !id.isEmpty else {
            return
        }
        new.setValue(id, forKey: "identifier")
        new.setValue(op, forKey: "operation")
        
        do {
            try self.context.save()
            
        } catch let errore {
            check = false
            print("\(Date()) | Class: SyncronizationAPI | Function: create() | Input: \(id), \(op)| Status: Error | Description: \n\(errore)\n")
        }
        
        print("\(Date()) | Class: SyncronizationAPI | Function: create() | Input: \(id), \(op)| Status: Finished | Return: \(check)")
    }
    
    func read(id: String) -> Synchronization? {
        print("\(Date()) | Class: SyncronizationAPI | Function: read() | Input: \(id) | Status: Started")
        
        var check: Synchronization? = nil
        let fetchRequest: NSFetchRequest<Synchronization> = Synchronization.fetchRequest()
        fetchRequest.predicate = NSPredicate.init(format: "identifier = %@", id)
        
        do {
            var array: [Synchronization] = []
            array = try self.context.fetch(fetchRequest)

            if (array.count > 0) {
                check = array[0]
            }

            print("\(Date()) | Class: SyncronizationAPI | Function: read() | Input: \(id) | Status: Finished | Return: \(String(describing: check))")
        } catch let errore {
            print("\(Date()) | Class: SyncronizationAPI | Function: read() | Input: \(id) | Status: Error | Description: \n\(errore)\n")
        }
        
        return check
    }
    
    func delete (id: String) -> Bool {
        print("\(Date()) | Class: SyncronizationAPI | Function: delete() | Input: \(id) | Status: Started")
        var check = true
        let fetchRequest: NSFetchRequest<Synchronization> = Synchronization.fetchRequest()
        
        fetchRequest.predicate = NSPredicate.init(format: "identifier = %@", id)
        
        if let result = try? context.fetch(fetchRequest) {
            for x in result {
                context.delete(x)
            }
            
            do {
                try self.context.save()
                
            } catch let errore {
                check = false
                
                print("\(Date()) | Class: SyncronizationAPI | Function: delete() | Input: \(id) | Status: Error | Description: \n\(errore)\n")
            }
        }
        
        print("\(Date()) | Class: SyncronizationAPI | Function: delete() | Input: \(id) | Status: Finished | Return: \(check)")
        
        return check
    }
    
    func deleteAll() -> Bool {
        print("\(Date()) | Class: SyncronizationAPI | Function: deleteAll() | Input: | Status: Started")
        
        var check = true
        let fetchRequest: NSFetchRequest<Synchronization> = Synchronization.fetchRequest()
        var array: [Synchronization] = []

        do {
            array = try self.context.fetch(fetchRequest)
          
            for sync in array {
                check = delete(id: sync.identifier!)
            }
            
            print("\(Date()) | Class: SyncronizationAPI | Function: deleteAll() | Input: | Status: Finished")
        } catch let errore {
            check = false
            print("\(Date()) | Class: SyncronizationAPI | Function: deleteAll() | Input: | Status: Error | Description: \n\(errore)\n")
        }

        return check
    }
    
    func getAll() -> [Synchronization] {
        print("\(Date()) | Class: SynchronizationAPI | Function: getAll() | Input: void | Status: Started")
        
        var array: [Synchronization] = []
        let fetchRequest: NSFetchRequest<Synchronization> = Synchronization.fetchRequest()
        
        do {
            array = try self.context.fetch(fetchRequest)

            print("\(Date()) | Class: SynchronizationAPI | Function: getAll() | Input: void | Status: Finished | Return: \(array.count) elements")
        } catch let errore {
            print("\(Date()) | Class: SynchronizationAPI | Function: getAll() | Input: void | Status: Error | Description: \n\(errore)\n")
        }
        
        return array
    }
    
}
