//
//  UpdateAPI.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 15/04/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class UpdateAPI {

    fileprivate let coreDataController: CoreDataController!
    fileprivate var context: NSManagedObjectContext!
    let dictionary: [String : [String]] = [
        "symptoms" : [NSLocalizedString("Flu", comment: ""), NSLocalizedString("Headache", comment: ""), NSLocalizedString("Toothache", comment: ""), NSLocalizedString("Stomachache", comment: ""), NSLocalizedString("Nausea", comment: ""), NSLocalizedString("Diarrhoea", comment: ""), NSLocalizedString("Joint pain", comment: ""), NSLocalizedString("Redness", comment: ""), NSLocalizedString("Skin patches", comment: ""), NSLocalizedString("Rash", comment: ""), NSLocalizedString("Burning eyes", comment: ""), NSLocalizedString("Dizziness", comment: ""), NSLocalizedString("Convulsions", comment: ""), NSLocalizedString("Sore throat", comment: ""), NSLocalizedString("Itch", comment: ""), NSLocalizedString("Hiccup", comment: ""), NSLocalizedString("Constipation", comment: ""), NSLocalizedString("Breathing problems", comment: "")],
        "fever" : [NSLocalizedString("No fever", comment: ""), "\(NSLocalizedString("Temperature", comment: "")) 36", "\(NSLocalizedString("Temperature", comment: "")) 36.5", "\(NSLocalizedString("Temperature", comment: "")) 37", "\(NSLocalizedString("Temperature", comment: "")) 37.5", "\(NSLocalizedString("Temperature", comment: "")) 38", "\(NSLocalizedString("Temperature", comment: "")) 38.5", "\(NSLocalizedString("Temperature", comment: "")) 39", "\(NSLocalizedString("Temperature", comment: "")) 39.5", "\(NSLocalizedString("Temperature", comment: "")) 40", "\(NSLocalizedString("Temperature", comment: "")) 40+"],
        "sleep" : ["\(NSLocalizedString("Slept", comment: "")) 0/3 \(NSLocalizedString("hours", comment: ""))", "\(NSLocalizedString("Slept", comment: "")) 3/6 \(NSLocalizedString("hours", comment: ""))", "\(NSLocalizedString("Slept", comment: "")) 6/9 \(NSLocalizedString("hours", comment: ""))", "\(NSLocalizedString("Slept", comment: "")) 9+ \(NSLocalizedString("hours", comment: ""))"],
        "food" : [NSLocalizedString("Sweet craving", comment: ""), NSLocalizedString("Salty craving", comment: ""), NSLocalizedString("Normal diet", comment: ""), NSLocalizedString("Lack of appetite", comment: "")]
    ]

    class var sharedInstance: UpdateAPI {
        struct Singleton {
            static let instance = UpdateAPI()
        }

        return Singleton.instance
    }

    
    init() {
        print("\(Date()) | Class: UpdateAPI | Function: init() | Input: void | Status: Started")
        
        self.coreDataController = CoreDataController.sharedInstance
        self.context = coreDataController.getContext()
        
        print("\(Date()) | Class: UpdateAPI | Function: init() | Input: void | Status: Finished | Return: void")
    }

    func create(name: String, date: Date, person: Person, id: String?) -> Update? {
        let check = true
        var new: Update!
        var identifier = id
        
        print("\(Date()) | Class: UpdateAPI | Function: create() | Input: \(identifier!) | Status: Started")
        
        if let update = read(id: identifier) {
            new = update
        } else {
            identifier = IdentifierAPI.sharedInstance.generateUpdate()
            guard let entity = NSEntityDescription.entity(forEntityName: "Update", in: self.context) else { return nil }
            new = Update(entity: entity, insertInto: self.context)
        }
        
        new.setValue(date, forKey: "date")
        new.setValue(identifier, forKey: "identifier")
        new.setValue(name, forKey: "name")
        
        person.addToUpdates(new)
        
        do {
            try self.context.save()
            
            print("\(Date()) | Class: UpdateAPI | Function: create() | Input: \(identifier!) | Status: Finished | Return: \(check)")
        } catch let errore {
            print("\(Date()) | Class: UpdateAPI | Function: create() | Input: \(identifier!) | Status: Error | Description: \n\(errore)\n")
            
            return nil
        }
        
        return new
    }
    
    /*func update(id: String) -> Bool { //Pass in input UpdateDetails
        print("\(Date()) | Class: UpdateAPI | Function: update() | Input: \(id) | Status: Started")
        
        var check = true
        let fetchRequest: NSFetchRequest<Update> = Update.fetchRequest()
        let predicate = NSPredicate(format: "identifier = %@", id)  //per prendere tutti quelli con l'id dato in input
        
        fetchRequest.predicate = predicate
        
        do {
            var array: [Update] = []
            array = try self.context.fetch(fetchRequest)

            if (array.count > 0) {
                if (array[0].checked == true) {
                    array[0].setValue(false, forKey: "checked")
                } else {
                    array[0].setValue(true, forKey: "checked")
                }
            }
            
            try self.context.save()

            print("\(Date()) | Class: UpdateAPI | Function: update() | Input: \(id) | Status: Finished | Return: \(check)")
        } catch let errore {
            check = false
            
            print("\(Date()) | Class: UpdateAPI | Function: update() | Input: \(id) | Status: Error | Description: \n\(errore)\n")
        }
        
        return check
    }*/
    
    func read(id: String?) -> Update? {
        print("\(Date()) | Class: UpdateAPI | Function: read() | Input: \(String(describing: id)) | Status: Started")
        
        guard id != nil else {
            return nil
        }
        
        var check: Update? = nil
        let fetchRequest: NSFetchRequest<Update> = Update.fetchRequest()
        let predicate = NSPredicate(format: "identifier = %@", id!)  //per prendere tutti quelli con l'id dato in input
        
        fetchRequest.predicate = predicate
        
        do {
            var array: [Update] = []
            array = try self.context.fetch(fetchRequest)

            if (array.count > 0) {
                check = array[0]
            }

            print("\(Date()) | Class: UpdateAPI | Function: read() | Input: \(String(describing: id)) | Status: Finished | Return: \(String(describing: check))")
        } catch let errore {
            print("\(Date()) | Class: UpdateAPI | Function: read() | Input: \(String(describing: id)) | Status: Error | Description: \n\(errore)\n")
        }
        
        return check
    }
    
    func delete(id: String) -> Bool {
        print("\(Date()) | Class: UpdateAPI | Function: delete() | Input: \(id) | Status: Started")
        
        var check = true
        let fetchRequest: NSFetchRequest<Update> = Update.fetchRequest()
        
        fetchRequest.predicate = NSPredicate.init(format: "identifier = %@", id)
        
        if let result = try? context.fetch(fetchRequest) {
            var count = 0
            for x in result {
                context.delete(x)
                count += 1
            }
            
            do {
                try self.context.save()
            } catch let errore {
                check = false
                
                print("\(Date()) | Class: UpdateAPI | Function: delete() | Input: \(id) | Status: Error | Description: \n\(errore)\n")
            }
        }
        
        print("\(Date()) | Class: UpdateAPI | Function: delete() | Input: \(id) | Status: Finished | Return: \(check)")
        
        return check
    }
    
    /*func getAll() -> [Update] {
        print("\(Date()) | Class: UpdateAPI | Function: getAll() | Input: void | Status: Started")
        
        var array: [Update] = []
        let fetchRequest: NSFetchRequest<Update> = Update.fetchRequest()
        
        do {
            array = try self.context.fetch(fetchRequest)
            array = array.sorted() {$0.date! < $1.date!}

            print("\(Date()) | Class: UpdateAPI | Function: getAll() | Input: void | Status: Finished | Return: \(array.count) elements")
        } catch let errore {
            print("\(Date()) | Class: UpdateAPI | Function: getAll() | Input: void | Status: Error | Description: \n\(errore)\n")
        }
        
        return array
    }*/
    
    func getAllOfPerson(id: String) -> [Update] {
        print("\(Date()) | Class: UpdateAPI | Function: getAllOfPerson() | Input: \(id) | Status: Started")
        
        var array: [Update] = []
        let fetchRequest: NSFetchRequest<Update> = Update.fetchRequest()
        
        fetchRequest.predicate = NSPredicate.init(format: "person.personId = %@", id)
        
        do {
            array = try self.context.fetch(fetchRequest)
            array = array.sorted() {$0.date! < $1.date!}

            print("\(Date()) | Class: UpdateAPI | Function: getAllOfPerson() | Input: \(id) | Status: Finished | Return: \(array.count) elements")
        } catch let errore {
            print("\(Date()) | Class: UpdateAPI | Function: getAllOfPerson() | Input: \(id) | Status: Error | Description: \n\(errore)\n")
        }
        
        return array
    }
    
    func getAllInDayOfPerson(id: String, date: Date) -> [Update] {
           print("\(Date()) | Class: UpdateAPI | Function: getAllInDayOfPerson() | Input: \(id) | Status: Started")
           
           var array: [Update] = []
           let fetchRequest: NSFetchRequest<Update> = Update.fetchRequest()
           let calendar = NSCalendar.current
           let startDate = calendar.date(bySettingHour: 0, minute: 0, second: 0, of: date)
           let endDate = calendar.date(bySettingHour: 23, minute: 59, second: 59, of: date)
           
           fetchRequest.predicate = NSPredicate.init(format: "person.personId = %@ and %@ >= date and %@ <= date", id, endDate! as CVarArg, startDate! as CVarArg)
           
           do {
               array = try self.context.fetch(fetchRequest)
               array = array.sorted() {$0.date! < $1.date!}

               print("\(Date()) | Class: UpdateAPI | Function: getAllInDayOfPerson() | Input: \(id) | Status: Finished | Return: \(array.count) elements")
           } catch let errore {
               print("\(Date()) | Class: UpdateAPI | Function: getAllInDayOfPerson() | Input: \(id) | Status: Error | Description: \n\(errore)\n")
           }
           
           return array
       }
    
}
