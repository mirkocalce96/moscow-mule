//
//  PersonAPI.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 18/02/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class PersonAPI {
    
    fileprivate let coreDataController: CoreDataController!
    fileprivate var context: NSManagedObjectContext!
    fileprivate let notificationAPI: NotificationAPI!

    class var sharedInstance: PersonAPI {
        struct Singleton {
            static let instance = PersonAPI()
        }

        return Singleton.instance
    }

    
    init() {
        print("\(Date()) | Class: PersonAPI | Function: init() | Input: void | Status: Started")
        
        self.coreDataController = CoreDataController.sharedInstance
        self.context = coreDataController.getContext()
        self.notificationAPI = NotificationAPI.sharedInstance
        
        print("\(Date()) | Class: PersonAPI | Function: init() | Input: void | Status: Finished | Return: void")
    }
    
    func create(attributes: PersonAttributes) -> Person? {
        let identifier: String
        
        if attributes.id == nil {
            identifier = IdentifierAPI.sharedInstance.generatePerson()
        } else {
            identifier = attributes.id!
        }
        print("\(Date()) | Class: PersonAPI | Function: create() | Input: \(identifier) | Status: Started")
        
        guard let entity = NSEntityDescription.entity(forEntityName: "Person", in: self.context) else { return nil }
        let new = NSManagedObject(entity: entity, insertInto: self.context)

        new.setValue(attributes.color, forKey: "color")
        new.setValue(attributes.dateOfBirth, forKey: "dateOfBirth")
        new.setValue(attributes.gender, forKey: "gender")
        new.setValue(attributes.name, forKey: "name")
        new.setValue(identifier, forKey: "personId")
        
        do {
            try self.context.save()
            
            print("\(Date()) | Class: PersonAPI | Function: create() | Input: \(identifier) | Status: Finished | Return: \(new)")
        } catch let errore {
            print("\(Date()) | Class: PersonAPI | Function: create() | Input: \(identifier) | Status: Error | Description: \n\(errore)\n")
            
            return nil
        }
        
        return (new as! Person)
    }
    
    func read(id: String) -> Person? {
        print("\(Date()) | Class: PersonAPI | Function: read() | Input: \(id) | Status: Started")
        
        var check: Person? = nil
        let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()
        let predicate = NSPredicate(format: "personId = %@", id)  //per prendere tutti quelli con l'id dato in input
        
        fetchRequest.predicate = predicate
        
        do {
            var array: [Person] = []
            array = try self.context.fetch(fetchRequest)

            if (array.count > 0) {
                check = array[0]
            }

            print("\(Date()) | Class: PersonAPI | Function: read() | Input: \(id) | Status: Finished | Return: \(String(describing: check))")
        } catch let errore {
            print("\(Date()) | Class: PersonAPI | Function: read() | Input: \(id) | Status: Error | Description: \n\(errore)\n")
        }
        
        return check
    }
    
    func delete(id: String) -> Bool {
        print("\(Date()) | Class: PersonAPI | Function: delete() | Input: \(id) | Status: Started")
        
        var check = true
        let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()
        
        fetchRequest.predicate = NSPredicate.init(format: "personId = %@", id)
        
        if let result = try? context.fetch(fetchRequest) {
            var count = 0
            
            let apps = AppointmentAPI.sharedInstance.getAllOfPerson(id: id)
            for x in apps {
                if (x.person?.personId == id) {
                    if !notificationAPI.deleteAppointment(appointment: x) {
                        check = false
                    }
                    
                    context.delete(x)
                }
            }
            
            let updates = UpdateAPI.sharedInstance.getAllOfPerson(id: id)
            for x in updates {
                if (x.person?.personId == id) {
                    context.delete(x)
                }
            }
            
            for x in result {
                context.delete(x)
                count += 1
            }
            
            do {
                try self.context.save()
            } catch let errore {
                check = false
                
                print("\(Date()) | Class: PersonAPI | Function: delete() | Input: \(id) | Status: Error | Description: \n\(errore)\n")
            }
        }
        
        print("\(Date()) | Class: PersonAPI | Function: delete() | Input: \(id) | Status: Finished | Return: \(check)")
        
        return check
    }
    
    func getAll() -> [Person] {
        print("\(Date()) | Class: PersonAPI | Function: getAll() | Input: void | Status: Started")
        
        let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()
        var array: [Person] = []
        
        do {
            array = try self.context.fetch(fetchRequest)

            print("\(Date()) | Class: PersonAPI | Function: getAll() | Input: void | Status: Finished | Return: \(array.count) elements")
        } catch let errore {
            print("\(Date()) | Class: PersonAPI | Function: getAll() | Input: void | Status: Error | Description: \n\(errore)\n")
        }
        
        return array
    }
    
    func update (attributes: PersonAttributes, person: Person) -> Person? {
        print("\(Date()) | Class: PersonAPI | Function: update() | Input: \(person.personId!) | Status: Started")
        
        person.setValue(attributes.color, forKey: "color")
        person.setValue(attributes.dateOfBirth, forKey: "dateOfBirth")
        person.setValue(attributes.gender, forKey: "gender")
        person.setValue(attributes.name, forKey: "name")
        
        do {
            try self.context.save()
            
            print("\(Date()) | Class: PersonAPI | Function: update() | Input: \(person.personId!) | Status: Finished | Return: \(person)")
        } catch let errore {
            print("\(Date()) | Class: PersonAPI | Function: update() | Input: \(person.personId!) | Status: Error | Description: \n\(errore)\n")
            
            return nil
        }
        
        return person
    }
    
    func personNumber() -> Int {
        let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()
        do {
            let count = try self.context.count(for:fetchRequest)
            return count
        } catch let error as NSError {
            print("Error: \(error.localizedDescription)")
            return 0
        }
    }
}
