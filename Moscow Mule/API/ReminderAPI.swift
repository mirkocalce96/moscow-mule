//
//  ReminderAPI.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 23/06/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import Foundation
import UIKit

class ReminderAPI {
    var weekReminder: Timer?

    class var sharedInstance: ReminderAPI {
        struct Singleton {
            static let instance = ReminderAPI()
        }

        return Singleton.instance
    }
    
    init() {
        print("\(Date()) | Class: AppointmentAPI | Function: init() | Input: void | Status: Started")
        print("\(Date()) | Class: AppointmentAPI | Function: init() | Input: void | Status: Finished | Return: void")
    }
    
    func prova() {
        weekReminder = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(repeatWeekReminder), userInfo: nil, repeats: true)
    }
    
    @objc func repeatWeekReminder() {
        let result = AppointmentAPI.sharedInstance.getAllNotCheckedInAWeek()
        
//        Calendar.current.nextDate(after: Date(), matching: .init(), matchingPolicy: .nextTime)
//        switch result {
//        case 0:
//            <#code#>
//        default:
//            <#code#>
//        }
        print(Date())
    }
    
    func setOnWeekReminder() {
        if weekReminder != nil {
            print("esiste ")
            if !weekReminder!.isValid {
                print("attivo")
            }
        } else {
            weekReminder = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(repeatWeekReminder), userInfo: nil, repeats: true)
        }
//        DETERMINARE I SECONDI PER ATTIVARLO
    }
    
    func setOffWeekReminder() {
        weekReminder?.invalidate()
    }
}
