//
//  ColorAPI.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 10/03/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import Foundation
import UIKit

class ColorAPI {

    var colors: [PersonColor] = [PersonColor(first: UIColor(red: 45 / 255, green: 69 / 255 , blue: 82 / 255, alpha: 1), second: UIColor(red: 45 / 255, green: 69 / 255 , blue: 82 / 255, alpha: 1)), PersonColor(first: UIColor(red: 78 / 255, green: 153 / 255 , blue: 142 / 255, alpha: 1), second: UIColor(red: 78 / 255, green: 153 / 255 , blue: 142 / 255, alpha: 1)), PersonColor(first: UIColor(red: 227 / 255, green: 197 / 255 , blue: 119 / 255, alpha: 1), second: UIColor(red: 227 / 255, green: 197 / 255 , blue: 119 / 255, alpha: 1)), PersonColor(first: UIColor(red: 232 / 255, green: 166 / 255 , blue: 108 / 255, alpha: 1), second: UIColor(red: 232 / 255, green: 166 / 255 , blue: 108 / 255, alpha: 1)), PersonColor(first: UIColor(red: 216 / 255, green: 117 / 255 , blue: 89 / 255, alpha: 1), second: UIColor(red: 216 / 255, green: 117 / 255 , blue: 89 / 255, alpha: 1))]

    class var sharedInstance: ColorAPI {
        struct Singleton {
            static let instance = ColorAPI()
        }

        return Singleton.instance
    }
    
    
    init() {
        print("\(Date()) | Class: ColorAPI | Function: init() | Input: void | Status: Started")
        print("\(Date()) | Class: ColorAPI | Function: init() | Input: void | Status: Finished | Return: void")
    }
    
    func isEqual(firstColor: PersonColor, secondColor: PersonColor) -> Bool {
        if firstColor.first == secondColor.first && firstColor.second == secondColor.second {
            return true
        }
        return false
    }
    
    func personColorToString(color: PersonColor) -> String {
        var colore: String
        colore = hexStringFromColor(color: color.first)
        return colore
    }
    
    func hexStringFromColor(color: UIColor) -> String {
       let components = color.cgColor.components
       let r: CGFloat = components?[0] ?? 0.0
       let g: CGFloat = components?[1] ?? 0.0
       let b: CGFloat = components?[2] ?? 0.0

       let hexString = String.init(format: "#%02lX%02lX%02lX", lroundf(Float(r * 255)), lroundf(Float(g * 255)), lroundf(Float(b * 255)))
       return hexString
    }
    
    func stringToPersonColor(color: String) -> PersonColor {
        guard !color.isEmpty else {
            return colors.first!
        }
        
        let colore = PersonColor(first: colorWithHexString(hexString: color), second: colorWithHexString(hexString: color))
        
        return colore
    }
    
    func colorWithHexString(hexString: String) -> UIColor {
        var colorString = hexString.trimmingCharacters(in: .whitespacesAndNewlines)
        colorString = colorString.replacingOccurrences(of: "#", with: "").uppercased()

        let alpha: CGFloat = 1.0
        let red: CGFloat = self.colorComponentFrom(colorString: colorString, start: 0, length: 2)
        let green: CGFloat = self.colorComponentFrom(colorString: colorString, start: 2, length: 2)
        let blue: CGFloat = self.colorComponentFrom(colorString: colorString, start: 4, length: 2)

        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }

    func colorComponentFrom(colorString: String, start: Int, length: Int) -> CGFloat {
        let startIndex = colorString.index(colorString.startIndex, offsetBy: start)
        let endIndex = colorString.index(startIndex, offsetBy: length)
        let subString = colorString[startIndex..<endIndex]
        let fullHexString = length == 2 ? subString : "\(subString)\(subString)"
        var hexComponent: UInt64 = 0

        guard Scanner(string: String(fullHexString)).scanHexInt64(&hexComponent) else {
            return 0
        }
        let hexFloat: CGFloat = CGFloat(hexComponent)
        let floatValue: CGFloat = CGFloat(hexFloat / 255.0)

        return floatValue
    }
}
