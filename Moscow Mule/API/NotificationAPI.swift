//
//  NotificationAPI.swift
//  Moscow Mule
//
//  Created by Luigi Ascione on 06/05/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import Foundation
import UserNotifications


class NotificationAPI {
  
    fileprivate var identifierAPI: IdentifierAPI!

    class var sharedInstance: NotificationAPI {
        struct Singleton {
            static let instance = NotificationAPI()
        }

        return Singleton.instance
    }
    
    
    init() {
        print("\(Date()) | Class: NotificationAPI | Function: init() | Input: void | Status: Started")
        
        identifierAPI = IdentifierAPI.sharedInstance
        
        print("\(Date()) | Class: NotificationAPI | Function: init() | Input: void | Status: Finished | Return: void")
    }
    
    func create(title: String?, subtitle: String?, body: String?, identifier: String, date: Date, threadIdentifier: String, summaryArgument: String) -> Void {
        print("\(Date()) | Class: NotificationAPI | Function: create() | Input: \(identifier) | Status: Started")
        let content = UNMutableNotificationContent()
        
        if title != nil {
            content.title = title!
        }
        
        if subtitle != nil {
            content.subtitle = subtitle!
        }
        
        if body != nil {
            content.body = body!
        }
        
        content.sound = UNNotificationSound.default
        content.threadIdentifier = threadIdentifier
        content.summaryArgument = summaryArgument

        let dateComponents = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: date)
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        center.add(request) { (error) in
             if error != nil {
                print("\(Date()) | Class: NotificationAPI | Function: create() | Input: \(identifier) | Status: Error | Description: \n\(error!)\n")
             }
        }
        print("\(Date()) | Class: NotificationAPI | Function: create() | Input: \(identifier) | Status: Finished | Return: true")
    }
    
    func createAppointment(appointment: Appointment) -> Bool {
        print("\(Date()) | Class: NotificationAPI | Function: createAppointment() | Input: \(appointment.identifier!) | Status: Started")
        
        if appointment.notifications {
            let dateFormatter = DateFormatter(); dateFormatter.dateFormat = "EEEE dd MMMM"
            let hourFormatter = DateFormatter(); hourFormatter.dateFormat = "HH:mm"
            let today = dateFormatter.string(from: appointment.date!)
            let notificationBody = "\(NSLocalizedString("Today at", comment: "")) \(hourFormatter.string(from: appointment.date!))\n\(appointment.person!.name!) \(NSLocalizedString("has an appointment now", comment: ""))"
            let notificationIdentifier = identifierAPI.getNotification(appointment: appointment)
            
            create(title: appointment.name, subtitle: nil, body: notificationBody, identifier: notificationIdentifier, date: appointment.date!, threadIdentifier: appointment.person!.personId!, summaryArgument: appointment.person!.name!)
                        
            if appointment.alerts.count >= 1 {
                let firstAlertIdentifier = identifierAPI.getFirstAlert(appointment: appointment)
                let firstAlertDate = Calendar.current.date(byAdding: .minute, value: (-appointment.alerts[0]), to: appointment.date!)
                let firstAlertDateString = dateFormatter.string(from: firstAlertDate!)
                var firstAlertBody: String
                
                if today == firstAlertDateString {
                    firstAlertBody = "\(NSLocalizedString("Today at", comment: "")) \(hourFormatter.string(from: appointment.date!))"
                } else {
                    firstAlertBody = "\(dateFormatter.string(from: appointment.date!)) \(NSLocalizedString("at", comment: "")) \(hourFormatter.string(from: appointment.date!))"
                }
                
                firstAlertBody += "\n\(appointment.person!.name!) \(NSLocalizedString("has an appointment in", comment: "")) \(self.alertStringFromValue(minutes: appointment.alerts[0]))"
                
                create(title: appointment.name, subtitle: nil, body: firstAlertBody, identifier: firstAlertIdentifier, date: firstAlertDate!, threadIdentifier: appointment.person!.personId!, summaryArgument: appointment.person!.name!)
            }
            
            if appointment.alerts.count >= 2 {
                let secondAlertIdentifier = identifierAPI.getSecondAlert(appointment: appointment)
                let secondAlertDate = Calendar.current.date(byAdding: .minute, value: (-appointment.alerts[1]), to: appointment.date!)
                let secondAlertDateString = dateFormatter.string(from: secondAlertDate!)
                var secondAlertBody: String
                
                if today == secondAlertDateString {
                    secondAlertBody = "\(NSLocalizedString("Today at", comment: "")) \(hourFormatter.string(from: appointment.date!))"
                } else {
                    secondAlertBody = "\(dateFormatter.string(from: appointment.date!)) \(NSLocalizedString("at", comment: "")) \(hourFormatter.string(from: appointment.date!))"
                }
                
                secondAlertBody += "\n\(appointment.person!.name!) \(NSLocalizedString("has an appointment in", comment: "")) \(self.alertStringFromValue(minutes: appointment.alerts[1]))"
                
                create(title: appointment.name, subtitle: nil, body: secondAlertBody, identifier: secondAlertIdentifier, date: secondAlertDate!, threadIdentifier: appointment.person!.personId!, summaryArgument: appointment.person!.name!)
            }
        }
        
        print("\(Date()) | Class: NotificationAPI | Function: createAppointment() | Input: \(appointment.identifier!) | Status: Finished | Return: true")
        
        return true
    }
    
    func deleteAppointment(appointment: Appointment) -> Bool {
        print("\(Date()) | Class: NotificationAPI | Function: deleteAppointment() | Input: \(appointment.identifier!) | Status: Started")
        
        let notification  = IdentifierAPI.sharedInstance.getNotification(appointment: appointment)
        let firstAlert = IdentifierAPI.sharedInstance.getFirstAlert(appointment: appointment)
        let secondAlert = IdentifierAPI.sharedInstance.getSecondAlert(appointment: appointment)
        
        center.removePendingNotificationRequests(withIdentifiers: [notification, firstAlert, secondAlert])
        center.removeDeliveredNotifications(withIdentifiers: [notification, firstAlert, secondAlert])
        
        print("\(Date()) | Class: NotificationAPI | Function: deleteAppointment() | Input: \(appointment.identifier!) | Status: Finished | Return: true")
        
        return true
    }
    
    func alertStringFromValue(minutes: Int) -> String {
        var string = ""
        var value = 0

        if ((minutes % 525600) == 0) {
            value = minutes / 525600

            if value == 1 {
                string = NSLocalizedString("year", comment: "")
            } else {
                string = NSLocalizedString("years", comment: "")
            }
        } else if ((minutes % 10080) == 0) {
            value = minutes / 10080

            if value == 1 {
                string = NSLocalizedString("week", comment: "")
            } else {
                string = NSLocalizedString("weeks", comment: "")
            }
        } else if ((minutes % 1440) == 0) {
            value = minutes / 1440

            if value == 1 {
                string = NSLocalizedString("day", comment: "")
            } else {
                string = NSLocalizedString("days", comment: "")
            }
        } else if ((minutes % 60) == 0) {
            value = minutes / 60

            if value == 1 {
                string = NSLocalizedString("hour", comment: "")
            } else {
                string = NSLocalizedString("hours", comment: "")
            }
        } else {
            value = minutes
            
            if value == 1 {
                string = NSLocalizedString("minute", comment: "")
            } else {
                string = NSLocalizedString("minutes", comment: "")
            }
        }
        
        return "\(value) \(string)"
    }
 
}

