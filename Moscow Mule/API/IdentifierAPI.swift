//
//  IdentifierAPI.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 20/02/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import Foundation

class IdentifierAPI {
    
    class var sharedInstance: IdentifierAPI {
        struct Singleton {
            static let instance = IdentifierAPI()
        }

        return Singleton.instance
    }
    
    func generatePerson() -> String {
        print("\(Date()) | Class: IdentifierAPI | Function: generatePerson() | Input: void | Status: Started")

        let currentDateTime = Date()
        let userCalendar = Calendar.current
        let requestedComponents: Set<Calendar.Component> = [
            .year,
            .month,
            .day,
            .hour,
            .minute,
            .second
        ]
        let dateTimeComponents = userCalendar.dateComponents(requestedComponents, from: currentDateTime)
        let identifier: String = "PE\(dateTimeComponents.year!)\(dateTimeComponents.month!)\(dateTimeComponents.day!)\(dateTimeComponents.hour!)\(dateTimeComponents.minute!)\(dateTimeComponents.second!)"

        
        print("\(Date()) | Class: IdentifierAPI | Function: generatePerson() | Input: void | Status: Finished | Return: \(identifier)")
        
        return identifier
    }
    
    func generateAppointment() -> String {
        print("\(Date()) | Class: IdentifierAPI | Function: generateAppointment() | Input: void | Status: Started")

        let currentDateTime = Date()
        let userCalendar = Calendar.current
        let requestedComponents: Set<Calendar.Component> = [
            .year,
            .month,
            .day,
            .hour,
            .minute,
            .second
        ]
        let dateTimeComponents = userCalendar.dateComponents(requestedComponents, from: currentDateTime)
        let identifier: String = "AP\(dateTimeComponents.year!)\(dateTimeComponents.month!)\(dateTimeComponents.day!)\(dateTimeComponents.hour!)\(dateTimeComponents.minute!)\(dateTimeComponents.second!)"

        
        print("\(Date()) | Class: IdentifierAPI | Function: generateAppointment() | Input: void | Status: Finished | Return: \(identifier)")
        
        return identifier
    }
    
    func generateUpdate() -> String {
        print("\(Date()) | Class: IdentifierAPI | Function: generateUpdate() | Input: void | Status: Started")

        let currentDateTime = Date()
        let userCalendar = Calendar.current
        let requestedComponents: Set<Calendar.Component> = [
            .year,
            .month,
            .day,
            .hour,
            .minute,
            .second
        ]
        let dateTimeComponents = userCalendar.dateComponents(requestedComponents, from: currentDateTime)
        let identifier: String = "UP\(dateTimeComponents.year!)\(dateTimeComponents.month!)\(dateTimeComponents.day!)\(dateTimeComponents.hour!)\(dateTimeComponents.minute!)\(dateTimeComponents.second!)"

        
        print("\(Date()) | Class: IdentifierAPI | Function: generateUpdate() | Input: void | Status: Finished | Return: \(identifier)")
        
        return identifier
    }
    
    func getNotification(appointment: Appointment) -> String {
        print("\(Date()) | Class: IdentifierAPI | Function: getNotification() | Input: \(appointment.identifier!) | Status: Started")
        
        let identifier = "NT\(appointment.person!.personId!)\(appointment.identifier!)"
        
        print("\(Date()) | Class: IdentifierAPI | Function: getNotification() | Input: \(appointment.identifier!) | Status: Finished | Return: \(identifier)")
        
        return identifier
    }
    
    func getFirstAlert(appointment: Appointment) -> String {
        print("\(Date()) | Class: IdentifierAPI | Function: getFirstAlert() | Input: \(appointment.identifier!) | Status: Started")
        
        let identifier = "A1\(appointment.person!.personId!)\(appointment.identifier!)"
        
        print("\(Date()) | Class: IdentifierAPI | Function: getFirstAlert() | Input: \(appointment.identifier!) | Status: Finished | Return: \(identifier)")
        
        return identifier
    }
    
    func getSecondAlert(appointment: Appointment) -> String {
        print("\(Date()) | Class: IdentifierAPI | Function: getSecondAlert() | Input: \(appointment.identifier!) | Status: Started")
        
        let identifier = "A2\(appointment.person!.personId!)\(appointment.identifier!)"
        
        print("\(Date()) | Class: IdentifierAPI | Function: getSecondAlert() | Input: \(appointment.identifier!) | Status: Finished | Return: \(identifier)")
        
        return identifier
    }
    
}
