//
//  InfoPersonTableViewCell.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 05/05/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import UIKit

class InfoPersonTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var personColorView: UIView!
    @IBOutlet weak var firstLetterNamePersonLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
