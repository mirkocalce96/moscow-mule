//
//  ProfileViewController.swift
//  Moscow Mule
//
//  Created by Luigi Ascione on 13/07/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import CoreData
import UIKit

class ProfileViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var confirmButton: UIBarButtonItem!
    @IBOutlet weak var textPassword: UITextField!
    @IBOutlet weak var textConfirmPassword: UITextField!
    private var change: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        confirmButton.title = NSLocalizedString("Confirm", comment: "")
        confirmButton.isEnabled = false
        // Do any additional setup after loading the view.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            if change == false {
                return 1
            } else {
                return 3
            }
        case 1:
            return 2
        default:
            return 3
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            if change == false {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "EmailIdentifier", for: indexPath)
                    if (authentication.username != nil) {
                        cell.textLabel?.text = authentication.username
                    }
                    return cell
            } else {
                switch indexPath.row {
                case 0:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "EmailIdentifier", for: indexPath)
                    cell.textLabel?.text = authentication.username
                    return cell
                case 1:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "PasswordIdentifier", for: indexPath)
                                        
                    return cell
                default:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ConfirmPasswordIdentifier", for: indexPath)
                    
                    //confirmPassword.placeholder = NSLocalizedString("Confirm password", comment: "")
                    
                    return cell
                }
            }
        case 1:
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "AppointmentIdentifier", for: indexPath)
                
                cell.textLabel!.text = NSLocalizedString("Appointment", comment: "")
                cell.detailTextLabel!.text = "\(appointmentAPI.appointmentNumber())"

                return cell
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "DiaryIdentifier", for: indexPath)
                
                cell.textLabel!.text = NSLocalizedString("Diary", comment: "")
                cell.detailTextLabel!.text = "\(personAPI.personNumber())"
                
                return cell
            }
        default:
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "DeletePersonIdentifier", for: indexPath)
                
                cell.textLabel!.text = NSLocalizedString("Delete all diaries", comment: "")
                
                return cell
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "DeleteAppointmentIdentifier", for: indexPath)
                
                cell.textLabel!.text = NSLocalizedString("Delete all appointments", comment: "")

                return cell
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "ChangePasswordIdentifier", for: indexPath)
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 44
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                print("Hai cliccato su email")
            } else if indexPath.row == 1 {
                print("Hai cliccato su password")
            } else if indexPath.row == 2 {
                print("Hai cliccato su conferma password")
            }
        } else if indexPath.section == 1 {
            if indexPath.row == 0 {
                print("Hai cliccato su app")
            } else if indexPath.row == 1 {
                print("Hai cliccato su diary")
            }
        } else if indexPath.section == 2 {
            if indexPath.row == 0 {
                print("Hai cliccato su delete person")
                let personAlert = UIAlertController(title: NSLocalizedString("Warning", comment: ""), message: NSLocalizedString("All diary will be deleted", comment: ""), preferredStyle: UIAlertController.Style.alert)
                personAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                    let persons = personAPI.getAll()
                    for person in persons {
                        if personAPI.delete(id: person.personId!) {
                            if authenticationAPI.isAuthenticated() {
                                synchronizationAPI.create(id: person.personId!, op: -1)
                            }
                        }
                    }
                    tableView.reloadData()
                }))
                personAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in }))
                present(personAlert, animated: true, completion: nil)
            } else if indexPath.row == 1 {
                print("Hai cliccato su delete appointment")
                let appointmentAlert = UIAlertController(title: NSLocalizedString("Warning", comment: ""), message: NSLocalizedString("All appointments will be deleted", comment: ""), preferredStyle: UIAlertController.Style.alert)
                appointmentAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                    let appointments = appointmentAPI.getAll()
                    for appointment in appointments {
                        if appointmentAPI.delete(id: appointment.identifier!) {
                            if authenticationAPI.isAuthenticated() {
                                synchronizationAPI.create(id: appointment.identifier!, op: -1)
                            }
                        }
                    }
                    tableView.reloadData()
                }))
                appointmentAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in }))
                present(appointmentAlert, animated: true, completion: nil)
            } else if indexPath.row == 2 {
                print("Hai cliccato su change pass")
                if change == false {
                    change = true
                    confirmButton.isEnabled = true
                    tableView.reloadData()
                } else {
                    change = false
                    confirmButton.isEnabled = false
                    tableView.reloadData()
                }
            }
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 28
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 28
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer: UITableViewHeaderFooterView = UITableViewHeaderFooterView()
        footer.contentView.backgroundColor = .systemGroupedBackground
        return footer
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header: UITableViewHeaderFooterView = UITableViewHeaderFooterView()
        header.contentView.backgroundColor = .systemGroupedBackground
        return header
    }


    @IBAction func confirmAction(_ sender: Any) {
        if (textPassword == textConfirmPassword) && (textPassword != nil) {
            print("Le password sono uguali possiamo interrogare il database")
        }
    }
    

}
