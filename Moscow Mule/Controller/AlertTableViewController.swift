//
//  AlertTableViewController.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 08/05/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import UIKit

class AlertTableViewController: UITableViewController {
    var indexPassed: Int!
    var alertsDates: [Int] = []
    var operation: Int!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        switch section {
        case 0:
            return 1
        default:
            return 9
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.00001
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 28
    }

    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer :UITableViewHeaderFooterView = UITableViewHeaderFooterView()
        
        footer.contentView.backgroundColor = .systemGroupedBackground
        return footer
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header :UITableViewHeaderFooterView = UITableViewHeaderFooterView()
        
        header.contentView.backgroundColor = .systemGroupedBackground
        return header
    }
    
   override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            if ((!self.alertsDates.isEmpty) && ((self.alertsDates.count - 1) >= indexPassed)) {
                self.alertsDates.remove(at: indexPassed)
            }
        default:
            var item: Int!

            switch indexPath.row {
            case 0:
                item = 5
            case 1:
                item = 10
            case 2:
                item = 15
            case 3:
                item = 30
            case 4:
                item = 60
            case 5:
                item = 120
            case 6:
                item = 1440
            case 7:
                item = 2880
            default:
                item = 10080
            }

            if self.alertsDates.count - 1 >= indexPassed {
                self.alertsDates[indexPassed] = item
            } else {
                self.alertsDates.insert(item, at: indexPassed)
            }
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch operation {
        case 0:
            performSegue(withIdentifier: "backFromAlertToNewAppointment", sender: nil)
        default:
            performSegue(withIdentifier: "backFromAlertToAppointment", sender: nil)
        }
    }

}
