//
//  AutenticationViewController.swift
//  Moscow Mule
//
//  Created by Luigi Ascione on 09/07/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import UIKit

class AutenticationViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    
    var settingsController: SettingsViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.hideKeyboardWhenTappedAround()
        
        username.delegate = self
        username.tag = 0
        password.delegate = self
        password.tag = 1
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag >= 0 {
            guard checkUsername() else {
                let alert = UIAlertController(title: NSLocalizedString("Warning", comment: ""), message: NSLocalizedString("Email field is mandatory", comment: ""), preferredStyle: .alert)
                let ok = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .cancel, handler: nil)

                alert.addAction(ok)
                self.present(alert, animated: true)

                return false
            }
            
            if textField.tag >= 1 {
                guard checkPassword() else {
                    let alert = UIAlertController(title: NSLocalizedString("Warning", comment: ""), message: NSLocalizedString("Password field is mandatory", comment: ""), preferredStyle: .alert)
                    let ok = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .cancel, handler: nil)
                
                    alert.addAction(ok)
                    self.present(alert, animated: true)
                
                    return false
                }
            }
        }
        
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            login()
            textField.resignFirstResponder()
        }
        
        return false
    }
    
    func login() {
        activityIndicator.startAnimating()
        let semaphore = DispatchSemaphore (value: 0)
        var request = URLRequest(url: URL(string: "\(server)/login?username=\(username.text!)&password=\(password.text!)")!,timeoutInterval: Double.infinity)
        
        request.httpMethod = "POST"
        request.timeoutInterval = 10

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            DispatchQueue.main.async {
                guard let data = data else {
                    let alert = UIAlertController(title: NSLocalizedString("Error", comment: ""), message: NSLocalizedString(error!.localizedDescription, comment: ""), preferredStyle: .alert)
                    let ok = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .cancel, handler: nil)
                
                    alert.addAction(ok)
                    self.present(alert, animated: true)
                    
                    self.activityIndicator.stopAnimating()
                    return
                }
            
                let body = String(data: data, encoding: .utf8)!
                let httpResponse = response as? HTTPURLResponse

                guard httpResponse?.statusCode == 200 else {
                    let alert = UIAlertController(title: NSLocalizedString("Error", comment: ""), message: NSLocalizedString(body, comment: ""), preferredStyle: .alert)
                    let ok = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .cancel, handler: nil)
                
                    alert.addAction(ok)
                    self.present(alert, animated: true)
                    
                    self.activityIndicator.stopAnimating()
                    return
                }
                
                authentication = authenticationAPI.login(username: self.username.text!, password: self.password.text!, token: body)
                self.activityIndicator.stopAnimating()
                self.settingsController.tableView.reloadSections(IndexSet(IndexPath(row: 0, section: 0)), with: .automatic)
                
                if authenticationAPI.isAuthenticated() {
                    synchronizationRest.synchronization()
                }

                self.navigationController!.popViewController(animated: true)
            }
            semaphore.signal()
        }

        task.resume()
        semaphore.wait()
    }
    
    func checkUsername() -> Bool {
        if username.text!.isEmpty{
            return false
        }
        
        return true
    }
    
    func checkPassword() -> Bool {
        if password.text!.isEmpty{
            return false
        }
        
        return true
    }
    
    @IBAction func forgetPassword(_ sender: Any) {
        let alert = UIAlertController(title: NSLocalizedString("Password recovery", comment: ""), message: NSLocalizedString("Enter the email address you registered with, you will receive an email with instructions for recovering your password.", comment: ""), preferredStyle: .alert)

        alert.addTextField { (textField) in
            textField.placeholder = "Email"
            textField.textAlignment = .center
            textField.keyboardType = .emailAddress
            textField.textContentType = .emailAddress
        }

        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel))
        alert.addAction(UIAlertAction(title: NSLocalizedString("Send", comment: ""), style: .default, handler: { [weak alert] (_) in
            
            let textField = alert?.textFields![0] // Cosa fa quando clicchi sul bottone
            print("Text field: \(textField!.text!)")
            
        }))

        self.present(alert, animated: true, completion: nil)
    }
    
}
