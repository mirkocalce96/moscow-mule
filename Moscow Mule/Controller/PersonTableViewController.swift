//
//  PersonViewController.swift
//  Moscow Mule
//
//  Created by Luigi Ascione on 16/05/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import UIKit

class PersonTableViewController: UITableViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, UICollectionViewDelegate, UICollectionViewDataSource {
    
    let personAPI = PersonAPI.sharedInstance
    private var dpShowDateVisible = false
    private var dpShowGenderVisible = false
    let colors = ColorAPI.sharedInstance.colors
    let attributes = PersonAttributes()
    var personPassed: Person!
    var color: Int!
    var firstTime = true
    var personDetailsViewController: PersonDetailsViewController!

    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var dateOfBirthDetail: UILabel!
    @IBOutlet weak var dateOfBirth: UIDatePicker!
    @IBOutlet weak var genderDetail: UILabel!
    @IBOutlet weak var gender: UIPickerView!
    @IBOutlet weak var colorCollection: UICollectionView!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var firstLetter: UILabel!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.hideKeyboardWhenTappedAround()
        
        gender.delegate = self
        gender.dataSource = self
        
        colorCollection.register(UINib.init(nibName: "ColorViewCell", bundle: nil), forCellWithReuseIdentifier: "ColorIdentifier")
        colorCollection.delegate = self
        colorCollection.dataSource = self
        
        dateOfBirth.maximumDate = Date()

        tableView.estimatedRowHeight = 150
        tableView.rowHeight = UITableView.automaticDimension
        
        colorView.layer.cornerRadius = colorView.frame.height / 2
        colorView.layer.masksToBounds = true

        color = checkColor()
        if color == nil {
            color = 0
            
            let alertName = UIAlertController(title: NSLocalizedString("Warning", comment: ""), message: NSLocalizedString("The previously chosen color can no longer be used", comment: ""), preferredStyle: .alert)
            alertName.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        }
        
        name.text = personPassed.name
        dateOfBirth.date = personPassed.dateOfBirth!
        
        let gen = checkGender()
        if gen != nil {
            gender.selectRow(gen!, inComponent: 0, animated: false)
        }
        
        doneButton.isEnabled = false
        isModalInPresentation = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        dpShowColorChanged()
        dpShowNameChanged()
        dpShowDateChanged()
        dpShowGenderChanged()
        
        firstTime = false
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return gend.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return gend[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        dpShowGenderChanged()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
     
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 1:
            return 5
        default:
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let errorTitle = NSLocalizedString("Error", comment: "")
        let errorMessage = NSLocalizedString("The elimination was not successful", comment: "")
        let error = UIAlertController(title: errorTitle, message: errorMessage, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            _ = self.personDetailsViewController.navigationController?.popViewController(animated: true)
        }
        error.addAction(okAction)
        if indexPath.section == 1 && indexPath.row == 1 {
            toggleShowDateDatepicker()
            dpShowDateChanged()
        } else if (indexPath.section == 1 && indexPath.row == 3) {
            toggleShowGenderPickerview()
            dpShowGenderChanged()
        } else if (indexPath.section == 2 && indexPath.row == 0) {
           let alert = UIAlertController(title: NSLocalizedString("Warning", comment: ""), message: NSLocalizedString("Are you sure you want to delete this person?", comment: ""), preferredStyle: .alert)
           let yes = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default) { (placehorder) in
               if (PersonAPI.sharedInstance.delete(id: self.personPassed.personId!)) == true {
                    self.dismiss(animated: true, completion: nil)
                    _ = self.personDetailsViewController.navigationController?.popViewController(animated: true)
               } else {
                    self.present(error, animated: true)
               }
           }
           let no = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .cancel, handler: nil)

           alert.addAction(yes)
           alert.addAction(no)
           
           self.present(alert, animated: true);
       }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if !dpShowDateVisible && indexPath.section == 1 &&  indexPath.row == 2 {
            return 0
        } else if !dpShowGenderVisible && indexPath.section == 1 &&  indexPath.row == 4 {
            return 0
        } else {
            return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }

    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.00001
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 28
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer: UITableViewHeaderFooterView = UITableViewHeaderFooterView()
        
        footer.contentView.backgroundColor = .systemGroupedBackground
        
        return footer
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header: UITableViewHeaderFooterView = UITableViewHeaderFooterView()
        
        header.contentView.backgroundColor = .systemGroupedBackground
        
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return colors.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ColorIdentifier", for: indexPath) as! ColorViewCell
        
        cell.colorView.insertGradientEffect(firstColor: colors[indexPath.row].first, secondColor: colors[indexPath.row].second, thirdColor: nil, cornerRadius: cell.colorView.frame.height / 2)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        color = indexPath.row
        dpShowColorChanged()
    }
    
    private func toggleShowDateDatepicker() {
         dpShowDateVisible = !dpShowDateVisible
     
         tableView.beginUpdates()
         tableView.endUpdates()
    }
    
    private func toggleShowGenderPickerview() {
         dpShowGenderVisible = !dpShowGenderVisible
     
         tableView.beginUpdates()
         tableView.endUpdates()
    }
    
    private func dpShowGenderChanged() {
        genderDetail.text = gend[gender.selectedRow(inComponent: 0)]

        switch gender.selectedRow(inComponent: 0) {
        case 1:
            attributes.gender = "Male"
        case 2:
            attributes.gender = "Female"
        default:
            attributes.gender = "Not Specified"
        }
        
        dpShowDoneChanged()
    }
    
    private func dpShowDateChanged() {
        let dateFormatter = DateFormatter(); dateFormatter.dateFormat = "d MMMM yyyy"
        let mydt = dateFormatter.string(from: dateOfBirth.date)
        
        dateOfBirthDetail.text = mydt
        attributes.dateOfBirth = dateOfBirth.date
        
        dpShowDoneChanged()
    }
    
    private func dpShowColorChanged() {
        colorView.insertGradientEffect(firstColor: colors[color].first, secondColor: colors[color].second, thirdColor: nil, cornerRadius: colorView.frame.height / 2)
        attributes.color = colors[color]
        
        dpShowDoneChanged()
    }

    private func dpShowNameChanged() {
        self.title = name.text
        
        if (name.text!.isEmpty) {
            firstLetter.text = ""
        } else {
            let first: Character = name.text![name.text!.startIndex]
            
            firstLetter.text = String(first)
        }
        
        attributes.name = name.text!
        
        dpShowDoneChanged()
    }

    func checkColor() -> Int? {
        for i in 0..<colors.count {
            if ColorAPI.sharedInstance.isEqual(firstColor: colors[i], secondColor: personPassed.color) == true {
                return i
            }
        }
        return nil
    }
    
    func checkGender() -> Int? {
        for i in 0..<gend.count {
            if gend[i] == personPassed.gender {
                return i
            }
        }
        
        return nil
    }
    
    func dpShowDoneChanged() {
        if !doneButton.isEnabled && !firstTime {
            doneButton.isEnabled = true
            isModalInPresentation = true
        }
    }
    
    @IBAction func dpShowNameAction(_ sender: Any) {
        dpShowNameChanged()
    }
    
    @IBAction func dpShowDateAction(_ sender: UIDatePicker) {
        dpShowDateChanged()
    }
    
    
    @IBAction func updateAction(_ sender: Any) {
        let alertName = UIAlertController(title: NSLocalizedString("Warning", comment: ""), message: NSLocalizedString("Name field is mandatory", comment: ""), preferredStyle: .alert)
        alertName.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        
        let errorTitle = NSLocalizedString("Error", comment: "")
          let errorMessage = NSLocalizedString("The update was not successful", comment: "")
          let error = UIAlertController(title: errorTitle, message: errorMessage, preferredStyle: .alert)
          let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
              UIAlertAction in
              self.performSegue(withIdentifier: "backFromPerson", sender: self)
          }
          error.addAction(okAction)
        
        guard (name.hasText) else { self.present(alertName, animated: true); return }
        
        if let person = personAPI.update(attributes: attributes, person: personPassed) {
            persons = personAPI.getAll()
            
            if authenticationAPI.isAuthenticated() {
                synchronizationAPI.create(id: person.personId!, op: 0)
            }
           
            self.performSegue(withIdentifier: "backFromPerson", sender: self)
        } else {
            self.present(error, animated: true)
        }
    }

}
