//
//  LoadableFromXibView.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 04/03/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import UIKit


open class LoadableFromXibView: UIView {

    @IBOutlet public weak var view: UIView!


    // MARK: - Inits

    override public init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }


    // MARK: - Setup

    private func xibSetup() {
        let podBundle = Bundle(for: ScrollableDatepicker.self)
        let bundlePath = podBundle.path(forResource: String(describing: type(of: self)), ofType: "bundle")
        var bundle: Bundle? = nil

        if bundlePath != nil {
            bundle = Bundle(path: bundlePath!)
        }

        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        view = nib.instantiate(withOwner: self, options: nil).first as? UIView
        addSubview(view)

        view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[view]-0-|", options: .directionLeadingToTrailing, metrics: nil, views: ["view": view!]))
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[view]-0-|", options: .directionLeadingToTrailing, metrics: nil, views: ["view": view!]))
    }
    
}
