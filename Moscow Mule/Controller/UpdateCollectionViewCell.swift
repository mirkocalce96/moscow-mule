//
//  UpdateCollectionViewCell.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 11/03/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import UIKit

class UpdateCollectionViewCell: UICollectionViewCell {
 
    @IBOutlet weak var personColorView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var hourLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()

        // Initialization code
        personColorView.layer.cornerRadius = 12
        personColorView.layer.shadowColor = UIColor.black.cgColor
        personColorView.layer.shadowOpacity = 0.3
        personColorView.layer.shadowRadius = 2
        personColorView.layer.shadowOffset = CGSize(width: 0, height: 1)
        
    }
    
}
