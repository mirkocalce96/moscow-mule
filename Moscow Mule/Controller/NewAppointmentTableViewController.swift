//
//  NewAppointmentTableViewController.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 05/03/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications

class NewAppointmentTableViewController: UITableViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, UITextViewDelegate {
    
    @IBOutlet weak var actionButton: UIBarButtonItem!
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var date: UIDatePicker!
    @IBOutlet weak var location: UITextField!
    @IBOutlet weak var person: UIPickerView!
    @IBOutlet weak var personDetail: UILabel!
    @IBOutlet weak var dateDetail: UILabel!
    @IBOutlet weak var notes: UITextView!
    @IBOutlet weak var notificationsSwitch: UISwitch!
    @IBOutlet weak var firstAlertLabel: UILabel!
    @IBOutlet weak var secondAlertLabel: UILabel!
    
    let attributes = AppointmentAttributes()
    private var dpShowDateVisible = false
    private var dpShowPersonVisible = false
    private var dpShowNotificatonVisible = true
    var dpShowFirstAlert = true
    var dpShowSecondAlert = false
    var indexPassed = -1
    var storyBoard: UIStoryboard!
    var alertTableViewController: AlertTableViewController!
    var firstTime = true
    let notesString = NSLocalizedString("Notes", comment: "")
    var datePassed: Date? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.hideKeyboardWhenTappedAround()
        
        person.delegate = self
        person.dataSource = self
        notes.delegate = self
        
        tableView.rowHeight = UITableView.automaticDimension

        notes.text = notesString
        notes.textColor = UIColor.placeholderText
        notes.selectedTextRange = notes.textRange(from: notes.beginningOfDocument, to: notes.beginningOfDocument)
        notes.textContainerInset = UIEdgeInsets(top: 12, left: 0, bottom: 12, right: 0)
        notes.textContainer.lineFragmentPadding = 0.0

        if (indexPassed != -1) {
            person.selectRow(indexPassed, inComponent: 0, animated: false)
            person.isUserInteractionEnabled = false
        }
        
        storyBoard = UIStoryboard(name: "Main", bundle: nil)
        alertTableViewController = (self.storyBoard.instantiateViewController(withIdentifier: "Alert") as! AlertTableViewController)
        alertTableViewController.operation = 0
        
        actionButton.isEnabled = false
        isModalInPresentation = false
        
        switch datePassed {
        case nil:
            date.date = Date().nextDate(roundedTo: 5)
        default:
            date.date = datePassed!.nextDate(roundedTo: 5)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let alertTitle = NSLocalizedString("Warning", comment: "")
        let alertMessage = NSLocalizedString("There are no people to associate an appointment", comment: "")
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default) { (placehorder) in
            self.dismiss(animated: true, completion: nil)
        }

        alert.addAction(ok)
        guard persons.count > 0 else { self.present(alert, animated: true); return }
        
        dpShowNameChanged()
        dpShowLocationChanged()
        dpShowNotesChanged()
        dpShowDateChanged()
        dpShowPersonChanged()
        notificationChanged()
        dpShowAlertChanged()
        
        firstTime = false
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return persons.count
    }
 
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return persons[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        dpShowPersonChanged()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
     
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 3
        case 3:
            return 3
        default:
            return 2
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 && indexPath.row == 0 {
            toggleShowDateDatepicker()
            dpShowDateChanged()
        } else if (indexPath.section == 2 && indexPath.row == 0) {
            toggleShowPersonPickerview()
            dpShowPersonChanged()
        } else if (indexPath.section == 3 && indexPath.row == 1) {
            alertTableViewController.indexPassed = 0
            alertTableViewController.navigationItem.title = NSLocalizedString("First alert", comment: "")

            self.navigationController!.pushViewController(alertTableViewController, animated: true)
        } else if (indexPath.section == 3 && indexPath.row == 2) {
            alertTableViewController.indexPassed = 1
            alertTableViewController.navigationItem.title = NSLocalizedString("Second alert", comment: "")

            self.navigationController!.pushViewController(alertTableViewController, animated: true)
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if !dpShowDateVisible && indexPath.section == 1 &&  indexPath.row == 1 {
            return 0
        } else if !dpShowPersonVisible && indexPath.section == 2 &&  indexPath.row == 1 {
            return 0
        } else if (indexPath.section == 0 && indexPath.row == 2) {
            return UITableView.automaticDimension
        } else if (!dpShowNotificatonVisible && indexPath.section == 3 && indexPath.row == 1) {
            return 0
        } else if ((!dpShowNotificatonVisible || !dpShowSecondAlert) && (indexPath.section == 3 && indexPath.row == 2)) {
            return 0
        } else if (indexPath.section == 3) {
            return 44
        } else {
            return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }

    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch section {
        case 3:
            return 50
        default:
            return 0.00001
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 28
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer :UITableViewHeaderFooterView = UITableViewHeaderFooterView()
        
        footer.contentView.backgroundColor = .systemGroupedBackground
        
        if section == 3 {
            let footerView = UITextView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 46))
        
            footerView.isEditable = false
            footerView.isSelectable = false
            footerView.isScrollEnabled = false
            footerView.textColor = UIColor.gray
            footerView.font = UIFont.systemFont(ofSize: 13)
            footerView.backgroundColor = .systemGroupedBackground
            footerView.text = NSLocalizedString("By enabling notifications you will be notified when the event starts independently of the alerts.", comment: "")
            footerView.textContainerInset = UIEdgeInsets(top: 8, left: 20, bottom: 8, right: 20)
            footerView.textContainer.lineFragmentPadding = 0.0

            return footerView
        }
        
        return footer
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header :UITableViewHeaderFooterView = UITableViewHeaderFooterView()
        
        header.contentView.backgroundColor = .systemGroupedBackground
        return header
    }
    
    func textViewDidChange(_ textView: UITextView) {
        dpShowNotesChanged()

        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText:String = textView.text
        let updatedText = (currentText as NSString).replacingCharacters(in: range, with: text)

        if updatedText.isEmpty {
            textView.text = notesString
            textView.textColor = UIColor.placeholderText
            textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
        } else if textView.textColor == UIColor.placeholderText && !text.isEmpty {
            textView.textColor = UIColor.label
            textView.text = text
        } else {
            return true
        }

        return false
    }
    
    func textViewDidChangeSelection(_ textView: UITextView) {
        if self.view.window != nil {
            if textView.textColor == UIColor.placeholderText {
                textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
            }
        }
    }

    private func toggleShowDateDatepicker() {
         dpShowDateVisible = !dpShowDateVisible
     
         tableView.beginUpdates()
         tableView.endUpdates()
    }
    
    private func toggleShowPersonPickerview() {
         dpShowPersonVisible = !dpShowPersonVisible
     
         tableView.beginUpdates()
         tableView.endUpdates()
    }
    
    private func dpShowDateChanged() {
        let dateFormatter = DateFormatter(); dateFormatter.dateFormat = "EEEE d MMMM yyyy, HH:mm"
        let mydt = dateFormatter.string(from: date.date)

        dateDetail.text = mydt
        attributes.date = self.date.date
        
        dpShowAddChanged()
    }
    
    private func dpShowPersonChanged() {
        personDetail.text = persons[person.selectedRow(inComponent: 0)].name!
        attributes.person = persons[person.selectedRow(inComponent: 0)]
        
        dpShowAddChanged()
    }
    
    func dpShowAlertChanged() {
        if alertTableViewController.alertsDates.isEmpty {
            dpShowSecondAlert = false
            firstAlertLabel.text = NSLocalizedString("None", comment: "")
            secondAlertLabel.text = NSLocalizedString("None", comment: "")
        } else {
            dpShowSecondAlert = true
            let beforeString = NSLocalizedString("before", comment: "")
            
            firstAlertLabel.text = "\(NotificationAPI.sharedInstance.alertStringFromValue(minutes: alertTableViewController.alertsDates[0]))"
            firstAlertLabel.text! += " \(beforeString)"
            
            if alertTableViewController.alertsDates.count == 2 {
                secondAlertLabel.text = "\(NotificationAPI.sharedInstance.alertStringFromValue(minutes: alertTableViewController.alertsDates[1]))"
                secondAlertLabel.text! += " \(beforeString)"
            } else {
                secondAlertLabel.text = NSLocalizedString("None", comment: "")
            }
        }
        
        dpShowAddChanged()
        
    }
    
    func dpShowNameChanged() {
        attributes.name = self.name.text!
        
        dpShowAddChanged()
    }
    
    func dpShowLocationChanged() {
        attributes.place = self.location.text!
        
        dpShowAddChanged()
    }
    
    func notificationChanged() {
        dpShowNotificatonVisible = notificationsSwitch.isOn
        attributes.notifications = notificationsSwitch.isOn
        
        dpShowAddChanged()
    }
    
    func dpShowNotesChanged() {
        if ((notes.text != notesString) && (notes!.textColor != UIColor.placeholderText)) {
            attributes.notes = self.notes.text
        } else {
            attributes.notes = ""
        }

        dpShowAddChanged()
    }
    
    func dpShowAddChanged() {
        if !actionButton.isEnabled && !firstTime {
            actionButton.isEnabled = true
            isModalInPresentation = true
        }
    }
    
    
    @IBAction func dpShowLocationAction(_ sender: Any) {
        dpShowLocationChanged()
    }
    
    @IBAction func dpShowTitleAction(_ sender: Any) {
        dpShowNameChanged()
    }
    
    @IBAction func notificationsAction(_ sender: Any) {
        dpShowNotificatonVisible = notificationsSwitch.isOn
        
        notificationChanged()
        
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    @IBAction func add(_ sender: Any) {
        let alertTitle = NSLocalizedString("Warning", comment: "")
        let alertMessage = NSLocalizedString("Title field is mandatory", comment: "")
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))

        let errorTitle = NSLocalizedString("Error", comment: "")
        let errorMessage = NSLocalizedString("The insertion was not successful", comment: "")
        let error = UIAlertController(title: errorTitle, message: errorMessage, preferredStyle: .alert)
        let okActionHome = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.performSegue(withIdentifier: "backFromNewAppointment", sender: self)
        }
        let okActionDiary = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.performSegue(withIdentifier: "backFromNewAppointmentToDiary", sender: self)
        }
                
        guard (name.hasText) else { self.present(alert, animated: true); return }
            
        attributes.alerts = alertTableViewController.alertsDates
        
        if ((notes.text != notesString) && (notes!.textColor != UIColor.placeholderText)) {
            attributes.notes = self.notes.text
        }
        
        if let appointment = appointmentAPI.create(attributes: attributes) {
            if authenticationAPI.isAuthenticated() {
                synchronizationAPI.create(id: appointment.identifier!, op: 1)
            }
            
            switch indexPassed {
            case -1:
                self.performSegue(withIdentifier: "backFromNewAppointment", sender: self)
            default:
                self.performSegue(withIdentifier: "backFromNewAppointmentToDiary", sender: self)
            }
        } else {
            switch indexPassed {
            case -1:
                error.addAction(okActionHome)
                self.present(error, animated: true)
            default:
                error.addAction(okActionDiary)
                self.present(error, animated: true)

            }
        }
    }
    
    @IBAction private func dpShowDateAction(sender: UIDatePicker) {
         dpShowDateChanged()
    }

    @IBAction func unwindFromAlertToNewAppointment(_ unwindSegue: UIStoryboardSegue) {
        // Use data from the view controller which initiated the unwind segue
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                self.tableView.beginUpdates()
                self.tableView.endUpdates()
            }
        }
    }
    
}
