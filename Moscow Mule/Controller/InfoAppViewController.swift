//
//  InfoAppViewController.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 06/05/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import UIKit

class InfoAppViewController: UIViewController {
    
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var versionDetailLabel: UILabel!
    @IBOutlet weak var buildDetailLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        logoImageView.layer.cornerRadius = logoImageView.layer.frame.height / 5
        versionDetailLabel.text = NSLocalizedString("Version", comment: "")
        versionDetailLabel.text! += " \(appVersion)"
        buildDetailLabel.text = "Build \(appBuild)"
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
