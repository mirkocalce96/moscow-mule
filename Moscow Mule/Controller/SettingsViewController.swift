//
//  SettingsViewController.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 05/05/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import StoreKit
import UIKit

extension UIApplication {
    class func tryURL(urls: [String]) {
        let application = UIApplication.shared
        for url in urls {
            if application.canOpenURL(URL(string: url)!) {
                if #available(iOS 13.0, *) {
                    application.open(URL(string: url)!, options: [:], completionHandler: nil)
                }
                else {
                    application.openURL(URL(string: url)!)
                }
                return
            }
        }
    }
}

class SettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIAdaptivePresentationControllerDelegate {

    @IBOutlet weak var tableView: UITableView!
    var controller: UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        persons = personAPI.getAll()
        self.tableView.reloadData()
    }
    
    // MARK: - Table view data source

    func presentationControllerDidAttemptToDismiss(_ presentationController: UIPresentationController) {
        let alertTitle = NSLocalizedString("Do you confirm that you want to discard the changes made?", comment: "")
        let alertContinue = NSLocalizedString("Continue to edit", comment: "")
        let alertDiscard =  NSLocalizedString("Discard", comment: "")
        let alertController = UIAlertController(title: alertTitle, message: "", preferredStyle: .actionSheet)
        let actionContinue = UIAlertAction(title: alertContinue, style: .cancel, handler: nil)
        let actionDelete = UIAlertAction(title: alertDiscard, style: .default){ (placehorder) in
            self.controller.dismiss(animated: true, completion: nil)
        }
        
        actionContinue.setValue(UIColor.label, forKey: "titleTextColor")
        actionDelete.setValue(UIColor.systemRed, forKey: "titleTextColor")
        alertController.addAction(actionContinue)
        alertController.addAction(actionDelete)

        controller.present(alertController, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "NewPersonSegueIdentifier"  {
            if let navigationController = segue.destination as? UINavigationController {
                if let newPersonViewController = navigationController.topViewController as? NewPersonTableViewController {
                    controller = newPersonViewController
                    segue.destination.presentationController?.delegate = self
                }
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 5
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        switch section {
        case 0:
            switch authenticationAPI.isAuthenticated() {
            case false:
                return 1
            default:
                return 2
            }
        case 1:
            return persons.count
        case 2:
            return 1
        case 3:
            return 5
        default:
            return 2
        }
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            if (authenticationAPI.isAuthenticated()) {
                switch indexPath.row {
                case 0:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileIdentifier", for: indexPath)
                    
                    cell.textLabel!.text = NSLocalizedString("Profile", comment: "")
                    cell.detailTextLabel!.text = authentication.username
                    
                    return cell
                case 1:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "LogoutIdentifier", for: indexPath)
                    
                    cell.textLabel?.text = NSLocalizedString("Logout", comment: "")
                    
                    return cell
                default:
                    return UITableViewCell()
                }
            } else {
                switch indexPath.row {
                case 0:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileIdentifier", for: indexPath)
                    
                    cell.textLabel?.text = NSLocalizedString("Login", comment: "")
                    cell.detailTextLabel!.text = ""
                    
                    return cell
                default:
                    return UITableViewCell()
                }
            }
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "InfoPersonIdentifier", for: indexPath) as! InfoPersonTableViewCell
            let first: Character = persons[indexPath.row].name![persons[indexPath.row].name!.startIndex]
            
            dateFormatter.dateFormat = "dd MMMM yyyy"
            
            cell.firstLetterNamePersonLabel.text = String(first)
            cell.nameLabel.text = persons[indexPath.row].name!
            cell.ageLabel.text = dateFormatter.string(from: persons[indexPath.row].dateOfBirth!)
            cell.personColorView.backgroundColor = persons[indexPath.row].color.first
            cell.personColorView.layer.cornerRadius = cell.personColorView.frame.height / 2
            
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewPersonIdentifier", for: indexPath)
            
            return cell
        case 3:
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "LanguageIdentifier", for: indexPath)
                
                return cell
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "InfoAppIdentifier", for: indexPath) as! InfoAppTableViewCell
                
                return cell
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "DevelopersIdentifier", for: indexPath)

                return cell
            case 3:
                let cell = UITableViewCell(style: .default, reuseIdentifier: "PrivacyIdentifier")
                
                cell.textLabel?.text = NSLocalizedString("Terms & Conditions", comment: "")
                cell.backgroundColor = .secondarySystemGroupedBackground
                
                return cell
            default:
                let cell = UITableViewCell(style: .default, reuseIdentifier: "PrivacyIdentifier")
                
                cell.textLabel?.text = NSLocalizedString("Privacy", comment: "")
                cell.backgroundColor = .secondarySystemGroupedBackground
                
                return cell
            }
        default:
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "ContactsIdentifier", for: indexPath)
                
                return cell
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewIdentifier", for: indexPath)
                
                return cell
            }
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 1:
            return 62
        default:
            return 44
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 28
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 28
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer: UITableViewHeaderFooterView = UITableViewHeaderFooterView()
        let personsFooterString = NSLocalizedString("Here you can see all the people present.", comment: "")
        footer.contentView.backgroundColor = .systemGroupedBackground
        
        
        if section == 1 {
            footer.textLabel?.text = personsFooterString
        }
        
        return footer
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header: UITableViewHeaderFooterView = UITableViewHeaderFooterView()
        let personsHeaderString = NSLocalizedString("PERSONS", comment: "")
        header.contentView.backgroundColor = .systemGroupedBackground
        
        if section == 0 {
            header.textLabel?.text = "ACCOUNT"
        } else if section == 1 {
            header.textLabel?.text = personsHeaderString
        } else if section == 3 {
            header.textLabel?.text = "DOCTARY"
        } else if section == 4 {
            header.textLabel?.text = "FEEDBACK"
        }
        
        return header
    }
    
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else { return }
        
        header.textLabel?.textColor = UIColor.gray
        header.textLabel?.font = UIFont.systemFont(ofSize: 13)
        header.textLabel?.frame = header.frame
        header.textLabel?.textAlignment = .left
    }
    
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        guard let footer = view as? UITableViewHeaderFooterView else { return }
        
        footer.textLabel?.textColor = UIColor.gray
        footer.textLabel?.font = UIFont.systemFont(ofSize: 13)
        footer.textLabel?.frame = footer.frame
        footer.textLabel?.textAlignment = .left
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            if authenticationAPI.isAuthenticated() {
                switch indexPath.row {
                case 0:
                   let authenticationController = storyboard!.instantiateViewController(identifier: "ProfileIdentifier")
                   
                   self.navigationController!.pushViewController(authenticationController, animated: true)
                case 1:
                    let alertController = UIAlertController(title: NSLocalizedString("Do you confirm that you want to logout?", comment: ""), message: "", preferredStyle: .actionSheet)
                    let actionContinue = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
                    let actionLogOut = UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .default){ (placehorder) in
                        authenticationAPI.logout()
                        tableView.reloadSections(IndexSet(indexPath), with: .automatic)
                    }
                    
                    actionContinue.setValue(UIColor.label, forKey: "titleTextColor")
                    actionLogOut.setValue(UIColor.systemRed, forKey: "titleTextColor")
                    alertController.addAction(actionContinue)
                    alertController.addAction(actionLogOut)

                    self.present(alertController, animated: true, completion: nil)
                default:
                   break
                }
            } else {
                switch indexPath.row {
                case 0:
                    let authenticationController = storyboard!.instantiateViewController(identifier: "AuthenticationIdentifier") as AutenticationViewController
                    
                    authenticationController.settingsController = self
                    self.navigationController!.pushViewController(authenticationController, animated: true)
                default:
                    break
                }
            }
        } else if indexPath.section == 1 {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let personDetailsViewController = storyBoard.instantiateViewController(identifier: "PersonDetailsIdentifier") as! PersonDetailsViewController

            personDetailsViewController.personPassed = persons[indexPath.row]
            self.navigationController!.pushViewController(personDetailsViewController, animated: true)
        } else if indexPath.section == 3 {
            if indexPath.row == 0 {
                if let url = URL(string:UIApplication.openSettingsURLString) {
                    if UIApplication.shared.canOpenURL(url) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }
                }
            } else if indexPath.row == 3 {
                if let url = URL(string: appTerms) {
                    if UIApplication.shared.canOpenURL(url) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }
                }
            } else if indexPath.row == 4 {
                if let url = URL(string: appPrivacy) {
                    if UIApplication.shared.canOpenURL(url) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }
                }
            }
        } else if indexPath.section == 4 && indexPath.row == 1 {
            rateApp()
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }

    func rateApp() {
        if #available(iOS 13, *) {
            SKStoreReviewController.requestReview()
        } else if let url = URL(string: "itms-apps://itunes.apple.com/app/\(appID)") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func unwindFromNewPerson(segue: UIStoryboardSegue) {
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    @IBAction func settingsAction(_ sender: Any) {
        self.navigationController!.popToRootViewController(animated: true)
    }
}
