//
//  UpdateDetailsView.swift
//  Moscow Mule
//
//  Created by Luigi Ascione on 02/06/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import UIKit

class UpdateDetailsView: UIView {

    @IBOutlet weak var titleUpdateDetails: UILabel!
    @IBOutlet weak var dateUpdateDetails: UILabel!
    @IBOutlet weak var hourUpdateDetails: UILabel!
    @IBOutlet weak var separatorUpdateDetails: UIView!
    @IBOutlet weak var personColorView: UIView!
    @IBOutlet weak var firstLetterNamePersonButton: UIButton!
    @IBOutlet var contentView: UIView!

    @IBOutlet weak var updateTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var updateLabelDetails: UILabel!
    @IBOutlet weak var titleTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var dateTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var hourBottomConstraint: NSLayoutConstraint!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("UpdateDetailsView", owner: self, options: nil)
        
        addSubview(contentView)
        
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    
    func getNeededHeight() -> CGFloat {
        var height = CGFloat(0)

        height += updateTopConstraint.constant
        height += updateLabelDetails.frame.height
        height += titleTopConstraint.constant
        height += titleUpdateDetails.frame.height
        height += dateUpdateDetails.layer.frame.origin.y - (titleUpdateDetails.layer.frame.origin.y + titleUpdateDetails.layer.frame.height)
        height += dateUpdateDetails.frame.height
        height += hourUpdateDetails.frame.height
        height += hourBottomConstraint.constant

        return height
    }

}
