//
//  UpdateDetailsViewController.swift
//  Moscow Mule
//
//  Created by Luigi Ascione on 03/06/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import Foundation
import UIKit

class UpdateDetailsViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var titleUpdateDetails: UILabel!
    @IBOutlet weak var dateUpdateDetails: UILabel!
    @IBOutlet weak var hourUpdateDetails: UILabel!
    @IBOutlet weak var personColorView: UIView!
    @IBOutlet weak var barBottom: UIView!
    @IBOutlet weak var firstLetterNamePersonButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    
    var update: Update!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.systemMaterial)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = barBottom.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        barBottom.insertSubview(blurEffectView, at: 0)
        
        guard update != nil else {return }
        
        self.updateData()
    }
    
    func updateData() {
        let first: Character = self.update.person!.name![self.update.person!.name!.startIndex]
        
        personColorView.backgroundColor = self.update.person!.color.first
        personColorView.layer.cornerRadius = personColorView.frame.height / 2
        firstLetterNamePersonButton.setTitle(String(first), for: .normal)
        titleUpdateDetails.translatesAutoresizingMaskIntoConstraints = false
        titleUpdateDetails.numberOfLines = 0
        titleUpdateDetails.text = update.name
        dateFormatter.dateFormat = "HH:mm"
        hourUpdateDetails.text = dateFormatter.string(from: self.update.date!)
        dateFormatter.dateFormat = "EEEE dd MMMM yyyy"
        dateUpdateDetails.text = dateFormatter.string(from: self.update.date!)
        firstLetterNamePersonButton.isUserInteractionEnabled = false

        self.scrollView.delegate = self
        self.scrollView.contentSize = .init(width: contentView.frame.width, height: 9000)
    }
    
    @IBAction func deleteAction(_ sender: Any) {
        let errorTitle = NSLocalizedString("Error", comment: "")
        let errorMessage = NSLocalizedString("The elimination was not successful", comment: "")
        let error = UIAlertController(title: errorTitle, message: errorMessage, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            _ = self.navigationController?.popViewController(animated: true)
        }
        error.addAction(okAction)
        let alertController = UIAlertController(title: NSLocalizedString("Do you confirm you want to delete this update?", comment: ""), message: "", preferredStyle: .actionSheet)
        let actionContinue = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
        let actionDelete = UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .default){ (placehorder) in
            if updateAPI.delete(id: self.update.identifier!) == true {
                if authenticationAPI.isAuthenticated() {
                    synchronizationAPI.create(id: self.update.identifier!, op: -1)
                }
                _ = self.navigationController?.popViewController(animated: true)
            } else {
                self.present(error, animated: true)
            }
        }
        
        actionContinue.setValue(UIColor.label, forKey: "titleTextColor")
        actionDelete.setValue(UIColor.systemRed, forKey: "titleTextColor")
        alertController.addAction(actionContinue)
        alertController.addAction(actionDelete)

        self.present(alertController, animated: true, completion: nil)
    }
}

