//
//  DevelopersTableViewController.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 06/06/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import UIKit

class DevelopersTableViewController: UITableViewController {

    var developers = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        developers.append("Mirko Calce")
        developers.append("Luigi Ascione")
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 2
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "DevelopersIdentifier")
        
        // Configure the cell...
        cell.backgroundColor = .secondarySystemGroupedBackground
        
        switch indexPath.row {
        case 0:
            cell.textLabel?.text = developers[0]
            cell.detailTextLabel?.text = NSLocalizedString("iOS Developer", comment: "")
        default:
            cell.textLabel?.text = developers[1]
            cell.detailTextLabel?.text = NSLocalizedString("iOS Developer", comment: "")
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 28
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 28
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer: UITableViewHeaderFooterView = UITableViewHeaderFooterView()
        
        footer.contentView.backgroundColor = .systemGroupedBackground
    
        return footer
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header: UITableViewHeaderFooterView = UITableViewHeaderFooterView()
        
        header.contentView.backgroundColor = .systemGroupedBackground
        
        return header
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else { return }
        
        header.textLabel?.textColor = UIColor.gray
        header.textLabel?.font = UIFont.systemFont(ofSize: 13)
        header.textLabel?.frame = header.frame
        header.textLabel?.textAlignment = .left
    }
    
    override func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        guard let footer = view as? UITableViewHeaderFooterView else { return }
        
        footer.textLabel?.textColor = UIColor.gray
        footer.textLabel?.font = UIFont.systemFont(ofSize: 13)
        footer.textLabel?.frame = footer.frame
        footer.textLabel?.textAlignment = .left
    }

}
