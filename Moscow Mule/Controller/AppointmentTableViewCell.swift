//
//  TableViewCell.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 20/02/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import UIKit

extension AppointmentTableViewCell: UIContextMenuInteractionDelegate {
    
    func contextMenuInteraction(_ interaction: UIContextMenuInteraction, configurationForMenuAtLocation location: CGPoint) -> UIContextMenuConfiguration? {
        return UIContextMenuConfiguration(identifier: nil, previewProvider: {
            let previewController = UIViewController()
            let appointmentDetailsView = AppointmentDetailsView()
            let first: Character = self.appointment.person!.name![self.appointment.person!.name!.startIndex]
            
            appointmentDetailsView.separatorAppointmentDetails.backgroundColor = self.appointment.person!.color.first
            appointmentDetailsView.titleAppointmentDetails.text = self.appointment.name!
            appointmentDetailsView.locationAppointmentDetails.text = self.appointment.place!
            appointmentDetailsView.personColorView.backgroundColor = self.appointment.person!.color.first
            appointmentDetailsView.personColorView.layer.cornerRadius = appointmentDetailsView.personColorView.frame.height / 2
            appointmentDetailsView.firstLetterNamePersonButton.setTitle(String(first), for: .normal)
            dateFormatter.dateFormat = "HH:mm"
            appointmentDetailsView.hourAppointmentDetails.text = dateFormatter.string(from: self.appointment.date!)
            dateFormatter.dateFormat = "EEEE dd MMMM yyyy"
            appointmentDetailsView.dateAppointmentDetails.text = dateFormatter.string(from: self.appointment.date!)
            appointmentDetailsView.notesAppointmentDetails.text = self.appointment.notes!
            
            appointmentDetailsView.typeLabel.text! = NSLocalizedString("Appointment", comment: "")
            if self.appointment.checked {
                appointmentDetailsView.typeLabel.text! += " \(NSLocalizedString("completed", comment: ""))"
            }
            
            if self.appointment.notifications {
                appointmentDetailsView.notificationsLabel.text = NSLocalizedString("Notifications On", comment: "")
                
                if self.appointment.alerts.count >= 1 {
                    appointmentDetailsView.firstAlertLabel.text = "\(NSLocalizedString("First alert", comment: "")) \(NotificationAPI.sharedInstance.alertStringFromValue(minutes: self.appointment.alerts[0])) \(NSLocalizedString("before", comment: ""))"
                } else {
                    appointmentDetailsView.firstAlertLabel.text = ""
                }
                
                if self.appointment.alerts.count >= 2 {
                    appointmentDetailsView.secondAlertLabel.text = "\(NSLocalizedString("Second alert", comment: "")) \(NotificationAPI.sharedInstance.alertStringFromValue(minutes: self.appointment.alerts[1])) \(NSLocalizedString("before", comment: ""))"
                } else {
                    appointmentDetailsView.secondAlertLabel.text = ""
                }
            } else {
                appointmentDetailsView.notificationsLabel.text = NSLocalizedString("Notifications Off", comment: "")
                appointmentDetailsView.firstAlertLabel.text = ""
                appointmentDetailsView.secondAlertLabel.text = ""
            }
            
            appointmentDetailsView.sizeToFit()
            
            previewController.view = appointmentDetailsView
            previewController.preferredContentSize = .init(width: appointmentDetailsView.contentView.bounds.width, height: appointmentDetailsView.getNeededHeight())

            return previewController
            }, actionProvider: { suggestedActions in
                return self.makeContextMenu(appointment: self.appointment)
        })
    }
    
    func makeContextMenu(appointment: Appointment) -> UIMenu {
        // Create a UIAction for sharing
        let children: [UIMenuElement]!
        
        let errorTitle = NSLocalizedString("Error", comment: "")
        let errorMessage = NSLocalizedString("The elimination was not successful", comment: "")
        let error = UIAlertController(title: errorTitle, message: errorMessage, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.controller.reloadAppointmentsTableViewSections()
            self.controller.tableView.reloadData()
        }
        error.addAction(okAction)
        
        let completed = UIAction(title: NSLocalizedString("Complete", comment: ""), image: UIImage(systemName: "checkmark.circle")) { action in
            // Show system share sheet
            if (appointmentAPI.check(id: appointment.identifier!) == true) {
                self.controller.reloadAppointmentsTableViewSections()
                self.controller.tableView.reloadData()
            }
        }
        
        let details = UIAction(title: NSLocalizedString("Details", comment: ""), image: UIImage(systemName: "calendar")) { action in
            // Show system share sheet
            let appointmentDetailsViewController = storyBoard.instantiateViewController(withIdentifier: "AppointmentDetailsIdentifier") as! AppointmentDetailsViewController
            
            appointmentDetailsViewController.appointmentPassed = appointment
            appointmentDetailsViewController.indexPerson = self.indexPerson
            self.controller.navigationController!.pushViewController(appointmentDetailsViewController,animated: true)
        }

        let edit = UIAction(title: NSLocalizedString("Edit", comment: ""), image: UIImage(systemName: "square.and.pencil")) { action in
            // Show system share sheet
            let appointmentDetailsViewController = storyBoard.instantiateViewController(withIdentifier: "AppointmentDetailsIdentifier") as! AppointmentDetailsViewController
            
            appointmentDetailsViewController.appointmentPassed = appointment
            appointmentDetailsViewController.indexPerson = self.indexPerson
            
            self.controller.navigationController!.pushViewController(appointmentDetailsViewController,animated: true)
            appointmentDetailsViewController.performSegue(withIdentifier: "AppointmentEditSegueIdentifier", sender: nil)
        }
        
        let confirmDelete = UIAction(title: NSLocalizedString("Delete", comment: ""), image: UIImage(systemName: "trash")){ action in
            if appointmentAPI.delete(id: self.appointment.identifier!) == true {
                if authenticationAPI.isAuthenticated() {
                    synchronizationAPI.create(id: self.appointment.identifier!, op: -1)
                }
                
                self.controller.reloadAppointmentsTableViewSections()
                self.controller.tableView.reloadData()
            } else {
                self.controller.present(error, animated: true)
            }
        }
                
        confirmDelete.attributes = UIMenuElement.Attributes.destructive
                
        let delete = UIMenu(title: NSLocalizedString("Delete", comment: ""), image: UIImage(systemName: "trash"), identifier: nil, options: .destructive, children: [confirmDelete])
        
        children = [completed, details, edit, delete]
        
        // Create and return a UIMenu with the share action
        return UIMenu(title: "", children: children) //TO DO cambiare i constraint dei figli qui
    }
    
}

class AppointmentTableViewCell: UITableViewCell {
    
    var appointment: Appointment!
    var controller: HomePageViewController!
    var indexPath: IndexPath!
    var indexPerson: Int!
    let current = Date()

    @IBOutlet weak var firstLetterNamePersonButton: UIButton!
    @IBOutlet weak var personColorView: UIView!
    @IBOutlet weak var detailsView: UIView!
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var notificationsImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        detailsView.layer.cornerRadius = 12
        detailsView.layer.shadowColor = UIColor.black.cgColor
        detailsView.layer.shadowOpacity = 0.3
        detailsView.layer.shadowRadius = 2
        detailsView.layer.shadowOffset = CGSize(width: 0, height: 1)
        
        let interaction = UIContextMenuInteraction(delegate: self)
        detailsView.addInteraction(interaction)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
        detailsView.addGestureRecognizer(tapGesture)
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer) {
        let appointmentDetailsViewController = storyBoard.instantiateViewController(withIdentifier: "AppointmentDetailsIdentifier") as! AppointmentDetailsViewController
        
        appointmentDetailsViewController.appointmentPassed = appointment
        appointmentDetailsViewController.indexPerson = indexPerson
//        self.controller.searchBarCancelButtonClicked(self.controller.searchController.searchBar)
        self.controller.navigationController!.pushViewController(appointmentDetailsViewController,animated: true)
    }
    
    @IBAction func diaryButton(_ sender: Any) {
        let diaryViewController = storyBoard.instantiateViewController(withIdentifier: "Diary") as! DiaryViewController
        
        diaryViewController.indexPassed = indexPerson
//        self.controller.searchBarCancelButtonClicked(self.controller.searchController.searchBar)
        self.controller.navigationController!.pushViewController(diaryViewController, animated: true)
    }
    
}
