//
//  DayCell.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 04/03/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import UIKit


open class DayCell: UICollectionViewCell {

    @IBOutlet public weak var dateLabel: UILabel!
    @IBOutlet public weak var weekDayLabel: UILabel!
    @IBOutlet public weak var monthLabel: UILabel!
    @IBOutlet public weak var selectorView: UIView!
    @IBOutlet weak var circleView: UIView!
    
    static var ClassName: String {
        return String(describing: self)
    }


    // MARK: - Setup

    func setup(date: Date, style: DayStyleConfiguration, personId: String?) {
        let formatter = DateFormatter()

        formatter.dateFormat = "dd"
        dateLabel.text = formatter.string(from: date)
        dateLabel.font = style.dateTextFont ?? dateLabel.font
        dateLabel.textColor = style.dateTextColor ?? dateLabel.textColor

        formatter.dateFormat = "EEEE"
        weekDayLabel.text = formatter.string(from: date)
        weekDayLabel.font = style.weekDayTextFont ?? weekDayLabel.font
        weekDayLabel.textColor = style.weekDayTextColor ?? weekDayLabel.textColor

        formatter.dateFormat = "MMM"
        monthLabel.text = formatter.string(from: date).uppercased()
        monthLabel.font = style.monthTextFont ?? monthLabel.font
        monthLabel.textColor = style.monthTextColor ?? monthLabel.textColor

        selectorView.layer.cornerRadius = 2
        selectorView.backgroundColor = style.selectorColor ?? UIColor.clear

        circleView.layer.cornerRadius = circleView.layer.frame.height / 2
        circleView.backgroundColor = style.monthTextColor ?? monthLabel.textColor
        circleView.isHidden = true
        
        guard personId != nil else {
            return
        }
        
        if ((appointmentAPI.getAllInDayOfPerson(id: personId!, date: date).count != 0) || (updateAPI.getAllInDayOfPerson(id: personId!, date: date).count != 0)) {
            circleView.isHidden = false
        }
//        else {
//            circleView.isHidden = true
//        }
    }

}
