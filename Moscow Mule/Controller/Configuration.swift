//
//  Configuration.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 04/03/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import UIKit


public struct Configuration {

    public enum DaySizeCalculationStrategy {
        case constantWidth(CGFloat)
        case numberOfVisibleItems(Int)
    }

    public var daySizeCalculation: DaySizeCalculationStrategy = DaySizeCalculationStrategy.numberOfVisibleItems(5)


    // MARK: - Styles

    public var defaultDayStyle: DayStyleConfiguration = {
        var configuration = DayStyleConfiguration()

        configuration.dateTextFont = .systemFont(ofSize: 20, weight: UIFont.Weight.regular)
        configuration.dateTextColor = UIColor.label.withAlphaComponent(0.4)

        configuration.weekDayTextFont = .systemFont(ofSize: 10, weight: UIFont.Weight.regular)
        configuration.weekDayTextColor = UIColor.label.withAlphaComponent(0.4)

        configuration.monthTextFont = .systemFont(ofSize: 10, weight: UIFont.Weight.regular)
        configuration.monthTextColor = UIColor.label.withAlphaComponent(0.4)

        configuration.selectorColor = .clear
        configuration.backgroundColor = .label

        return configuration
    }()

    public var weekendDayStyle: DayStyleConfiguration = {
        var configuration = DayStyleConfiguration()
//        configuration.weekDayTextFont = .systemFont(ofSize: 10, weight: UIFont.Weight.bold)
//        configuration.monthTextFont = .systemFont(ofSize: 10, weight: UIFont.Weight.bold)
//        configuration.dateTextFont = .systemFont(ofSize: 20, weight: UIFont.Weight.bold)
        
        configuration.weekDayTextColor = UIColor.systemRed.withAlphaComponent(0.4)
        configuration.monthTextColor = UIColor.systemRed.withAlphaComponent(0.4)
        configuration.dateTextColor = UIColor.systemRed.withAlphaComponent(0.4)
        
        return configuration
    }()

    public var selectedDayStyle: DayStyleConfiguration = {
        var configuration = DayStyleConfiguration()
        configuration.weekDayTextColor = .label
        configuration.monthTextColor = .label
        configuration.dateTextColor = .label
        configuration.selectorColor = .label
        
        return configuration
    }()


    // MARK: - Configuration
    @available(*, deprecated, message: "Use daySizeCalculation property")
    public var numberOfDatesInOneScreen: Int = 5 {
        didSet {
            daySizeCalculation = .numberOfVisibleItems(numberOfDatesInOneScreen)
        }
    }
    

    // MARK: - Initializer
    public init() {
    }


    // MARK: - Methods

    func calculateDayStyle(isWeekend: Bool, isSelected: Bool) -> DayStyleConfiguration {
        var style = defaultDayStyle

        if isWeekend {
            style = style.merge(with: weekendDayStyle)
        }

        if isSelected {
            style = style.merge(with: selectedDayStyle)
            
            if isWeekend {
                style.weekDayTextColor = .systemRed
                style.monthTextColor = .systemRed
                style.dateTextColor = .systemRed
                style.selectorColor = .systemRed
            }
        }

        return style
    }

}


public struct DayStyleConfiguration {

    public var dateTextFont: UIFont?
    public var dateTextColor: UIColor?

    public var weekDayTextFont: UIFont?
    public var weekDayTextColor: UIColor?

    public var monthTextFont: UIFont?
    public var monthTextColor: UIColor?

    public var selectorColor: UIColor?
    public var backgroundColor: UIColor?


    // MARK: - Initializer
    public init() {
    }


    public func merge(with style: DayStyleConfiguration) -> DayStyleConfiguration {
        var newStyle = DayStyleConfiguration()

        newStyle.dateTextFont = style.dateTextFont ?? dateTextFont
        newStyle.dateTextColor = style.dateTextColor ?? dateTextColor

        newStyle.weekDayTextFont = style.weekDayTextFont ?? weekDayTextFont
        newStyle.weekDayTextColor = style.weekDayTextColor ?? weekDayTextColor

        newStyle.monthTextFont = style.monthTextFont ?? monthTextFont
        newStyle.monthTextColor = style.monthTextColor ?? monthTextColor

        newStyle.selectorColor = style.selectorColor ?? selectorColor
        newStyle.backgroundColor = style.backgroundColor ?? backgroundColor

        return newStyle
    }

}
