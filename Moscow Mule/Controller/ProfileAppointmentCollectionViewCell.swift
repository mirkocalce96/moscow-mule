//
//  ProfileAppointmentCollectionViewCell.swift
//  Moscow Mule
//
//  Created by Luigi Ascione on 11/03/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import UIKit

class ProfileAppointmentCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailsView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var notesTextView: UITextView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
          
        // Initialization code
        detailsView.layer.cornerRadius = 12
        detailsView.layer.shadowColor = UIColor.black.cgColor
        detailsView.layer.shadowOpacity = 0.3
        detailsView.layer.shadowRadius = 2
        detailsView.layer.shadowOffset = CGSize(width: 0, height: 1)
        
        notesTextView.textContainerInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        notesTextView.textContainer.lineFragmentPadding = 0.0
        
    }
}
