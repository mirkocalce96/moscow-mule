//
//  AppointmentTableViewController.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 16/05/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications

class AppointmentTableViewController: UITableViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, UITextViewDelegate, UIAdaptivePresentationControllerDelegate {
    
    @IBOutlet weak var doneButton: UIBarButtonItem!
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var date: UIDatePicker!
    @IBOutlet weak var location: UITextField!
    @IBOutlet weak var person: UIPickerView!
    @IBOutlet weak var personDetail: UILabel!
    @IBOutlet weak var dateDetail: UILabel!
    @IBOutlet weak var notes: UITextView!
    @IBOutlet weak var notificationsSwitch: UISwitch!
    @IBOutlet weak var firstAlertLabel: UILabel!
    @IBOutlet weak var secondAlertLabel: UILabel!
    
    var appointmentPassed: Appointment!
    private var dpShowDateVisible = false
    private var dpShowPersonVisible = false
    private var dpShowNotificatonVisible = true
    var dpShowFirstAlert = true
    var dpShowSecondAlert = false
    var indexPassed = -1
    var alertTableViewController: AlertTableViewController!
    var appointmentDetailsViewController: AppointmentDetailsViewController!
    let attributes = AppointmentAttributes()
    var firstTime = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.hideKeyboardWhenTappedAround()
        
        person.delegate = self
        person.dataSource = self
        notes.delegate = self
        
        tableView.rowHeight = UITableView.automaticDimension
        
        name.text = appointmentPassed.name
        location.text = appointmentPassed.place
        
        if !appointmentPassed.notes!.isEmpty {
            notes.text = appointmentPassed.notes
            notes.textColor = UIColor.label
        }
        
        notes.selectedTextRange = notes.textRange(from: notes.beginningOfDocument, to: notes.beginningOfDocument)
        notes.textContainerInset = UIEdgeInsets(top: 12, left: 0, bottom: 12, right: 0)
        notes.textContainer.lineFragmentPadding = 0.0
        
        date.date = appointmentPassed.date!
        person.selectRow(persons.firstIndex(of: appointmentPassed.person!)!, inComponent: 0, animated: false)
        
        alertTableViewController = (storyBoard.instantiateViewController(withIdentifier: "Alert") as! AlertTableViewController)
        alertTableViewController.alertsDates = appointmentPassed.alerts
        alertTableViewController.operation = 1
        
        notificationsSwitch.isOn = appointmentPassed.notifications
        
        doneButton.isEnabled = false
        isModalInPresentation = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        dpShowNameChanged()
        dpShowLocationChanged()
        dpShowNotesChanged()
        dpShowDateChanged()
        dpShowPersonChanged()
        notificationChanged()
        dpShowAlertChanged()

        firstTime = false
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return persons.count
    }
 
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return persons[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        dpShowPersonChanged()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
     
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 3
        case 3:
            return 3
        case 4:
            return 1
        default:
            return 2
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
         let errorTitle = NSLocalizedString("Error", comment: "")
         let errorMessage = NSLocalizedString("The elimination was not successful", comment: "")
         let error = UIAlertController(title: errorTitle, message: errorMessage, preferredStyle: .alert)
         let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
             UIAlertAction in
             self.dismiss(animated: true, completion: nil)
            _ = self.appointmentDetailsViewController.navigationController?.popToRootViewController(animated: true)
         }
         error.addAction(okAction)
        
        if indexPath.section == 1 && indexPath.row == 0 {
            toggleShowDateDatepicker()
            dpShowDateChanged()
        } else if (indexPath.section == 2 && indexPath.row == 0) {
            toggleShowPersonPickerview()
            dpShowPersonChanged()
        } else if (indexPath.section == 3 && indexPath.row == 1) {
            alertTableViewController.indexPassed = 0
            alertTableViewController.navigationItem.title = NSLocalizedString("First alert", comment: "")

            self.navigationController!.pushViewController(alertTableViewController, animated: true)
        } else if (indexPath.section == 3 && indexPath.row == 2) {
            alertTableViewController.indexPassed = 1
            alertTableViewController.navigationItem.title = NSLocalizedString("Second alert", comment: "")

            self.navigationController!.pushViewController(alertTableViewController, animated: true)
        } else if (indexPath.section == 4 && indexPath.row == 0) {
            let alert = UIAlertController(title: NSLocalizedString("Warning", comment: ""), message: NSLocalizedString("Are you sure you want to delete this appointment?", comment: ""), preferredStyle: .alert)
            let yes = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default) { (placehorder) in
                if (AppointmentAPI.sharedInstance.delete(id: self.appointmentPassed.identifier!)) == true {
                    self.dismiss(animated: true, completion: nil)
                    _ = self.appointmentDetailsViewController.navigationController?.popToRootViewController(animated: true)
                } else {
                    self.present(error, animated: true)
                }
            }
            let no = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .cancel, handler: nil)

            alert.addAction(yes)
            alert.addAction(no)
            
            self.present(alert, animated: true);
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if !dpShowDateVisible && indexPath.section == 1 &&  indexPath.row == 1 {
            return 0
        } else if !dpShowPersonVisible && indexPath.section == 2 &&  indexPath.row == 1 {
            return 0
        } else if (indexPath.section == 0 && indexPath.row == 2) {
            return UITableView.automaticDimension
        } else if (!dpShowNotificatonVisible && indexPath.section == 3 && indexPath.row == 1) {
            return 0
        } else if ((!dpShowNotificatonVisible || !dpShowSecondAlert) && (indexPath.section == 3 && indexPath.row == 2)) {
            return 0
        } else if (indexPath.section == 3) {
            return 44
        } else {
            return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }

    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch section {
        case 3:
            return 50
        default:
            return 0.00001
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 28
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer :UITableViewHeaderFooterView = UITableViewHeaderFooterView()
        
        footer.contentView.backgroundColor = .systemGroupedBackground
        
        if section == 3 {
            let footerView = UITextView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 46))
        
            footerView.isEditable = false
            footerView.isSelectable = false
            footerView.isScrollEnabled = false
            footerView.textColor = UIColor.gray
            footerView.font = UIFont.systemFont(ofSize: 13)
            footerView.backgroundColor = UIColor.systemGroupedBackground
            footerView.text  = NSLocalizedString("By enabling notifications you will be notified when the event starts independently of the alerts.", comment: "")
            footerView.textContainerInset = UIEdgeInsets(top: 8, left: 20, bottom: 8, right: 20)
            footerView.textContainer.lineFragmentPadding = 0.0

            return footerView
        }
        
        return footer
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header :UITableViewHeaderFooterView = UITableViewHeaderFooterView()
        
        header.contentView.backgroundColor = .systemGroupedBackground
        return header
    }
    
    func textViewDidChange(_ textView: UITextView) {
        dpShowNotesChanged()

        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText:String = textView.text
        let updatedText = (currentText as NSString).replacingCharacters(in: range, with: text)

        if updatedText.isEmpty {
            textView.text = NSLocalizedString("Notes", comment: "")
            textView.textColor = UIColor.placeholderText
            textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
        } else if textView.textColor == UIColor.placeholderText && !text.isEmpty {
            textView.textColor = UIColor.label
            textView.text = text
        } else {
            return true
        }

        return false
    }
    
    func textViewDidChangeSelection(_ textView: UITextView) {
        if self.view.window != nil {
            if textView.textColor == UIColor.placeholderText {
                textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
            }
        }
    }

    private func toggleShowDateDatepicker() {
         dpShowDateVisible = !dpShowDateVisible
     
         tableView.beginUpdates()
         tableView.endUpdates()
    }
    
    private func toggleShowPersonPickerview() {
         dpShowPersonVisible = !dpShowPersonVisible
     
         tableView.beginUpdates()
         tableView.endUpdates()
    }
    
    private func dpShowDateChanged() {
        let dateFormatter = DateFormatter(); dateFormatter.dateFormat = "EEEE d MMMM yyyy, HH:mm"
        let mydt = dateFormatter.string(from: date.date)
        
        dateDetail.text = mydt
        attributes.date = self.date.date
        
        dpShowDoneChanged()
    }
    
    private func dpShowPersonChanged() {
        personDetail.text = persons[person.selectedRow(inComponent: 0)].name!
        attributes.person = persons[self.person.selectedRow(inComponent: 0)]
        
        dpShowDoneChanged()
    }
    
    func dpShowAlertChanged() {
        if alertTableViewController.alertsDates.isEmpty {
            dpShowSecondAlert = false
            firstAlertLabel.text = NSLocalizedString("None", comment: "")
            secondAlertLabel.text = NSLocalizedString("None", comment: "")
        } else {
            dpShowSecondAlert = true
            firstAlertLabel.text = "\(NotificationAPI.sharedInstance.alertStringFromValue(minutes: alertTableViewController.alertsDates[0])) \(NSLocalizedString("before", comment: ""))"
            
            if alertTableViewController.alertsDates.count == 2 {
                secondAlertLabel.text = "\(NotificationAPI.sharedInstance.alertStringFromValue(minutes: alertTableViewController.alertsDates[1])) \(NSLocalizedString("before", comment: ""))"
            } else {
                secondAlertLabel.text = NSLocalizedString("None", comment: "")
            }
        }
        
        dpShowDoneChanged()
        
        attributes.alerts = alertTableViewController.alertsDates
    }
    
    func dpShowNameChanged() {
        self.title = name.text
        attributes.name = self.name.text!
        
        dpShowDoneChanged()
    }
    
    func dpShowLocationChanged() {
        attributes.place = self.location.text!
        
        dpShowDoneChanged()
    }
    
    func notificationChanged() {
        dpShowNotificatonVisible = notificationsSwitch.isOn
        attributes.notifications = notificationsSwitch.isOn
        
        dpShowDoneChanged()
    }
    
    func dpShowNotesChanged() {
        if ((notes.text != NSLocalizedString("Notes", comment: "")) && (notes!.textColor != UIColor.placeholderText)) {
            attributes.notes = self.notes.text
        } else {
            attributes.notes = ""
        }

        dpShowDoneChanged()
    }
    
    func dpShowDoneChanged() {
        if !doneButton.isEnabled && !firstTime {
            doneButton.isEnabled = true
            isModalInPresentation = true
        }
    }
    
    @IBAction func dpShowLocationChanged(_ sender: Any) {
        dpShowLocationChanged()
    }
    
    @IBAction func notificationsAction(_ sender: Any) {
        notificationChanged()
        
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    @IBAction func doneAction(_ sender: Any) {
        let alert = UIAlertController(title: NSLocalizedString("Warning", comment: ""), message: NSLocalizedString("Title field is mandatory", comment: ""), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        let errorTitle = NSLocalizedString("Error", comment: "")
        let errorMessage = NSLocalizedString("The update was not successful", comment: "")
        let error = UIAlertController(title: errorTitle, message: errorMessage, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.dismiss(animated: true, completion: nil)
        }
        error.addAction(okAction)
        
        guard (name.hasText) else { self.present(alert, animated: true); return }
        
        if let appointment = appointmentAPI.update(attributes: attributes, appointment: appointmentPassed) {
            if authenticationAPI.isAuthenticated() {
                synchronizationAPI.create(id: appointment.identifier!, op: 0)
            }
            
            self.performSegue(withIdentifier: "backFromUpdate", sender: nil)
        } else {
            self.present(error, animated: true)
        }
    }
    
    @IBAction private func dpShowDateAction(sender: UIDatePicker) {
        dpShowDateChanged()
    }
    
    @IBAction func dpShowNameAction(_ sender: Any) {
        dpShowNameChanged()
    }
    
    @IBAction func unwindFromAlertToAppointment(_ unwindSegue: UIStoryboardSegue) {
        // Use data from the view controller which initiated the unwind segue
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                self.tableView.beginUpdates()
                self.tableView.endUpdates()
            }
        }
    }
}
