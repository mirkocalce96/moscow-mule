//
//  ProfileViewCell.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 24/02/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import UIKit

class ProfileViewCell: UICollectionViewCell {

    @IBOutlet weak var ProfileName: UILabel!
    @IBOutlet weak var ProfileFirstLetterName: UILabel!
    @IBOutlet weak var ProfileView : UIView!


    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        ProfileView.layer.shadowOffset = CGSize(width: 103, height: 125)
        ProfileView.layer.cornerRadius = ProfileView.frame.height / 2
        ProfileView.layer.masksToBounds = true
    }

}
