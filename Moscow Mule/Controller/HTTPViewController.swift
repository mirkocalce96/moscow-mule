//
//  HTTPViewController.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 02/07/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import UIKit

class HTTPViewController: UIViewController {
    @IBOutlet weak var urlTextField: UITextField!
    @IBOutlet weak var responseRequest: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func sendRequestButton(_ sender: Any) {
        print("sendariello")
    //        PROVA HTTP
        let url = URL(string: urlTextField.text!)!

       var request = URLRequest(url: url)
       request.httpMethod = "GET"

//       NSURLConnection.sendAsynchronousRequest(request, queue: OperationQueue.main) {(response, data, error) in
//           guard let data = data else { return }
//           print(String(data: data, encoding: .utf8)!)
//       }

        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            guard let data = data else { return }
            print(String(data: data, encoding: .utf8)!)
        }

        task.resume()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
