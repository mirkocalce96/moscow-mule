//
//  ProfileViewController.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 04/03/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import Foundation
import UIKit

extension DiaryViewController: ScrollableDatepickerDelegate {

    func datepicker(_ datepicker: ScrollableDatepicker, didSelectDate date: Date) {
        showSelectedDate()
    }
    
}

class DiaryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource, UIAdaptivePresentationControllerDelegate {
    
    let userCalendar = Calendar.current
    var indexPassed = Int()
    var dateFormatter = DateFormatter()
    var personalAppointments: [Appointment] = []
    var personalUpdates: [Update] = []
    var alert: UIAlertController!
    var ok: UIAlertAction!
    var controller: UIViewController!
    var rows = [Int:AnyObject]() //chiave = riga nella TableView, valore = appuntamento o update a seconda del caso
    
    @IBOutlet weak var pickerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var datePicker: ScrollableDatepicker! {
        didSet {
            var dates = [Date]()
            for day in -365...365 {
                dates.append(Date(timeIntervalSinceNow: Double(day * 86400)))
            }

            datePicker.dates = dates
            datePicker.selectedDate = Date()
            datePicker.delegate = self

            var configuration = Configuration()

            // weekend customization
//            configuration.weekendDayStyle.dateTextColor = .systemRed
//            configuration.weekendDayStyle.monthTextColor = .systemRed
//            configuration.weekendDayStyle.weekDayTextColor = .systemRed

            // selected date customization
            configuration.selectedDayStyle.backgroundColor = UIColor(white: 0.9, alpha: 1)
            configuration.daySizeCalculation = .numberOfVisibleItems(5)

            datePicker.configuration = configuration
        }
    }

    @IBOutlet weak var firstLetterNamePersonLabel: UILabel!
    @IBOutlet weak var personView: UIView!
    @IBOutlet weak var personsPickerView: UIPickerView!
    @IBOutlet weak var warningLabel: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.translatesAutoresizingMaskIntoConstraints = true
        
        personsPickerView.delegate = self
        personsPickerView.dataSource = self
        
        dateFormatter.dateFormat = "dd MMMM yyyy"
        personView.layer.cornerRadius = personView.frame.height / 2
        personView.layer.masksToBounds = true
        
        alert = UIAlertController(title: NSLocalizedString("Warning", comment: ""), message: NSLocalizedString("There are no people to show diary", comment: ""), preferredStyle: .alert)
        ok = UIAlertAction(title: "Ok", style: .default) { (placehorder) in
            _ = self.navigationController?.popViewController(animated: true)
        }

        alert.addAction(ok)
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.scrollsToTop = true
        
        pickerView.layer.cornerRadius = 12
        pickerView.layer.shadowColor = UIColor.black.cgColor
        pickerView.layer.shadowOpacity = 0.3
        pickerView.layer.shadowRadius = 2
        pickerView.layer.shadowOffset = CGSize(width: 0, height: 1)
    }

    override func viewWillAppear(_ animated: Bool) {
        guard persons.count > 0 else { self.present(alert, animated: true); return }
        guard (persons[indexPassed].personId != nil) else { _ = self.navigationController?.popViewController(animated: true); return }

        personsPickerView.selectRow(indexPassed, inComponent: 0, animated: false)
        dpShowPersonChanged()
        
        DispatchQueue.main.async {
            self.showSelectedDate()
            self.datePicker.scrollToSelectedDate(animated: false)
        }
    }
    
    func presentationControllerDidAttemptToDismiss(_ presentationController: UIPresentationController) {
        let alertController = UIAlertController(title: NSLocalizedString("Do you confirm that you want to discard the changes made?", comment: ""), message: "", preferredStyle: .actionSheet)
        let actionContinue = UIAlertAction(title: NSLocalizedString("Continue to edit", comment: ""), style: .cancel, handler: nil)
        let actionDelete = UIAlertAction(title: NSLocalizedString("Discard", comment: ""), style: .default){ (placehorder) in
            self.controller.dismiss(animated: true, completion: nil)
        }
        
        actionContinue.setValue(UIColor.label, forKey: "titleTextColor")
        actionDelete.setValue(UIColor.systemRed, forKey: "titleTextColor")
        alertController.addAction(actionContinue)
        alertController.addAction(actionDelete)

        controller.present(alertController, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        rows.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DiaryTableViewCell") as! DiaryTableViewCell
        
        cell.content = rows[indexPath.row]
        
        switch cell.content {
        case is Appointment:
            let appointment: Appointment = rows[indexPath.row] as! Appointment
            
            dateFormatter.dateFormat = "HH:mm"
            let hour = dateFormatter.string(from: appointment.date!).capitalized

            cell.indexPerson = persons.firstIndex(of: appointment.person!)
            cell.controller = self
            cell.indexPath = indexPath
            cell.hourLabel.text = "\(hour)"
            cell.separatorView.backgroundColor = appointment.person!.color.first
            cell.titleLabel.text = appointment.name
            cell.selectionStyle = .none
            cell.layer.shadowOffset = CGSize(width: tableView.frame.width, height: 50)
            cell.layer.shadowColor = UIColor.black.cgColor
            
            cell.typeLabel.text = NSLocalizedString("Appointment", comment: "")
            
            if appointment.checked {
                cell.typeLabel.text! += " \(NSLocalizedString("completed", comment: ""))"
            }
            
            cell.notificationsImageView.isHidden = false
            
            switch appointment.notifications {
            case false:
                cell.notificationsImageView.image = UIImage(systemName: "bell.slash")
            default:
                cell.notificationsImageView.image = UIImage(systemName: "bell")
            }
        case is Update:
            let update: Update = rows[indexPath.row] as! Update
            
            dateFormatter.dateFormat = "HH:mm"
            let hour = dateFormatter.string(from: update.date!).capitalized

            cell.indexPerson = persons.firstIndex(of: update.person!)
            cell.controller = self
            cell.indexPath = indexPath
            cell.hourLabel.text = "\(hour)"
            cell.separatorView.backgroundColor = update.person!.color.first
            cell.titleLabel.text = update.name
            cell.selectionStyle = .none
            cell.layer.shadowOffset = CGSize(width: tableView.frame.width, height: 50)
            cell.layer.shadowColor = UIColor.black.cgColor
            
            cell.typeLabel.text = NSLocalizedString("Update", comment: "")
        default:
            return UITableViewCell()
        }
        
        return cell
    }
    
    func tableView(_ : UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let errorTitle = NSLocalizedString("Error", comment: "")
        let errorMessage = NSLocalizedString("The elimination was not successful", comment: "")
        let error = UIAlertController(title: errorTitle, message: errorMessage, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { UIAlertAction in }
        error.addAction(okAction)
        
        switch rows[indexPath.row] {
        case is Appointment:
            let appointment: Appointment = rows[indexPath.row] as! Appointment
            let edit = UIContextualAction(style: .normal, title: NSLocalizedString("Edit", comment: "")) { (action, view, nil) in
                let appointmentDetailsViewController = storyBoard.instantiateViewController(withIdentifier: "AppointmentDetailsIdentifier") as! AppointmentDetailsViewController
                
                appointmentDetailsViewController.appointmentPassed = appointment
                appointmentDetailsViewController.indexPerson = persons.firstIndex(of: appointment.person!)
                
                self.navigationController!.pushViewController(appointmentDetailsViewController,animated: true)
                appointmentDetailsViewController.performSegue(withIdentifier: "AppointmentEditSegueIdentifier", sender: nil)
            }
            let delete = UIContextualAction(style: .destructive, title: NSLocalizedString("Delete", comment: "")) { (action, view, nil) in
                if (appointmentAPI.delete(id: appointment.identifier!)) == true {
                    if authenticationAPI.isAuthenticated() {
                        synchronizationAPI.create(id: appointment.identifier!, op: -1)
                    }
                    
                    self.deleteRow(indexPath: indexPath)
                } else {
                    self.present(error, animated: true)
                }
            }
            
            delete.image = UIImage(systemName: "trash")
            edit.image = UIImage(systemName: "square.and.pencil")
            
            return UISwipeActionsConfiguration(actions: [delete, edit])
        case is Update:
            let update: Update = rows[indexPath.row] as! Update
            let delete = UIContextualAction(style: .destructive, title: NSLocalizedString("Delete", comment: "")) { (action, view, nil) in
                if (updateAPI.delete(id: (update.identifier!))) == true {
                    if authenticationAPI.isAuthenticated() {
                          synchronizationAPI.create(id: update.identifier!, op: -1)
                    }
                    self.deleteRow(indexPath: indexPath)
                } else {
                    self.present(error, animated: true)
                }
            }
            
            delete.image = UIImage(systemName: "trash")
            
            return UISwipeActionsConfiguration(actions: [delete])
        default:
            return nil
        }
    }
    
    func tableView(_ : UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        switch rows[indexPath.row] {
        case is Appointment:
            let appointment: Appointment = rows[indexPath.row] as! Appointment
            let check = UIContextualAction(style: .normal, title: "Completed") { (action, view, completionHandler) in
                if (appointmentAPI.check(id: appointment.identifier!) == true) {
                    DispatchQueue.main.async {
                        self.tableView.reloadRows(at: [indexPath], with: .right)
                    }
                    completionHandler(true)
                }
            }
            
            check.backgroundColor = .systemBlue
            if appointment.checked {
                check.title = NSLocalizedString("Completed", comment: "")
                check.image = UIImage(systemName: "checkmark.circle.fill")
            } else {
                check.title = NSLocalizedString("Complete", comment: "")
                check.image = UIImage(systemName: "checkmark.circle")
            }
            
            return UISwipeActionsConfiguration(actions: [check])
        default:
            return nil
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return persons.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let view = InfoPersonView()

        view.nameLabel.text = persons[row].name!
        view.ageLabel.text = "\(persons[row].dateOfBirth!.totalDistance(from: Date(), resultIn: .year)!) \(NSLocalizedString("years old", comment: ""))"
        
        return view
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 44
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        dpShowPersonChanged()
        tableView.reloadData()
    }
    
    private func dpShowPersonChanged() {
        indexPassed = personsPickerView.selectedRow(inComponent: 0)
        
        let first: Character = persons[indexPassed].name![persons[indexPassed].name!.startIndex]
        firstLetterNamePersonLabel.text = String(first)
        personView.insertGradientEffect(firstColor: persons[indexPassed].color.first, secondColor: persons[indexPassed].color.second, thirdColor: nil, cornerRadius: personView.frame.height / 2)
        
        datePicker.personId = persons[indexPassed].personId
        reloadTableViewRows() //ricarica delle celle

        datePicker.scrollToSelectedDate(animated: true)
        showSelectedDate()
    }
    
    fileprivate func showSelectedDate() {
        guard datePicker.selectedDate != nil else {
            return
        }

        reloadTableViewRows()
        tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "NewUpdateIdentifier"  {
            if let navigationController = segue.destination as? UINavigationController {
                if let newUpdate = navigationController.topViewController as? NewUpdateViewController {
                    newUpdate.indexPassed = self.indexPassed
                    newUpdate.datePassed = datePicker.selectedDate!
                }
            }
        } else if segue.identifier == "NewAppointmentIdentifier" {
            if let navigationController = segue.destination as? UINavigationController {
                if let newAppointment = navigationController.topViewController as? NewAppointmentTableViewController {
                    newAppointment.indexPassed = self.indexPassed
                    newAppointment.datePassed = datePicker.selectedDate!
                    controller = newAppointment
                    segue.destination.presentationController?.delegate = self
                }
            }
        }
    }
    
    func reloadTableViewRows() {
        personalAppointments = appointmentAPI.getAllInDayOfPerson(id: persons[indexPassed].personId!, date: datePicker.selectedDate!)
        personalUpdates = updateAPI.getAllInDayOfPerson(id: persons[indexPassed].personId!, date: datePicker.selectedDate!)
        
        rows = [Int:AnyObject]()
        
        guard (!personalAppointments.isEmpty || !personalUpdates.isEmpty) else { warningLabel.isHidden = false; return }
        warningLabel.isHidden = true
        
        var a = 0, u = 0, index = 0;
        while (a < personalAppointments.count && u < personalUpdates.count) {
            if personalAppointments[a].date! <= personalUpdates[u].date! {
                rows[index] = personalAppointments[a]
                a += 1
            } else {
                rows[index] = personalUpdates[u]
                u += 1
            }
            
            index += 1
        }
        
        if a < personalAppointments.count {
            for i in a...(personalAppointments.count - 1) {
                rows[index] = personalAppointments[i]
                index += 1
            }
        } else if u < personalUpdates.count {
            for i in u...(personalUpdates.count - 1) {
                rows[index] = personalUpdates[i]
                index += 1
            }
        }
    }
    
    func deleteRow(indexPath: IndexPath) {
        self.tableView.beginUpdates()
        reloadTableViewRows()
        self.tableView.deleteRows(at: [indexPath], with: .automatic)
        self.tableView.endUpdates()
        
        warningLabel.isHidden = !rows.isEmpty
    }
    
    func showToday() {
        datePicker.selectedDate = Date()
        datePicker.scrollToSelectedDate(animated: true)
        showSelectedDate()
    }
    
    @IBAction func unwindFromNewUpdate(segue: UIStoryboardSegue) {
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                self.reloadTableViewRows()
                self.tableView.reloadData()
            }
        }
    }
    
    @IBAction func unwindFromNewAppointmentToDiary(segue: UIStoryboardSegue) {
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                self.reloadTableViewRows()
                self.tableView.reloadData()
            }
        }
    }
    
    @IBAction func settingsAction(_ sender: Any) {
          
        self.navigationController!.popToRootViewController(animated: true)
    }
    
    @IBAction func showTodayAction(_ sender: Any) {
        showToday()
    }
}
