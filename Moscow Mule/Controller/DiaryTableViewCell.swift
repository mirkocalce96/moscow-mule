//
//  DiaryTableViewCell.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 29/05/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import UIKit

extension DiaryTableViewCell: UIContextMenuInteractionDelegate {
    
    func contextMenuInteraction(_ interaction: UIContextMenuInteraction, configurationForMenuAtLocation location: CGPoint) -> UIContextMenuConfiguration? {
        return UIContextMenuConfiguration(identifier: nil, previewProvider: {
            switch self.content {
            case is Appointment:
                let appointment = (self.content as! Appointment)
                let previewController = UIViewController()
                let appointmentDetailsView = AppointmentDetailsView()
                let first: Character = appointment.person!.name![appointment.person!.name!.startIndex]
                
                appointmentDetailsView.separatorAppointmentDetails.backgroundColor = appointment.person!.color.first
                appointmentDetailsView.titleAppointmentDetails.text = appointment.name!
                appointmentDetailsView.locationAppointmentDetails.text = appointment.place!
                appointmentDetailsView.personColorView.backgroundColor = appointment.person!.color.first
                appointmentDetailsView.personColorView.layer.cornerRadius = appointmentDetailsView.personColorView.frame.height / 2
                appointmentDetailsView.firstLetterNamePersonButton.setTitle(String(first), for: .normal)
                dateFormatter.dateFormat = "HH:mm"
                appointmentDetailsView.hourAppointmentDetails.text = dateFormatter.string(from: appointment.date!)
                dateFormatter.dateFormat = "EEEE dd MMMM yyyy"
                appointmentDetailsView.dateAppointmentDetails.text = dateFormatter.string(from: appointment.date!)
                appointmentDetailsView.notesAppointmentDetails.text = appointment.notes!
                
                appointmentDetailsView.typeLabel.text! = NSLocalizedString("Appointment", comment: "")
                if appointment.checked {
                    appointmentDetailsView.typeLabel.text! += " \(NSLocalizedString("completed", comment: ""))"
                }
                
                if appointment.notifications {
                    appointmentDetailsView.notificationsLabel.text = NSLocalizedString("Notifications On", comment: "")
                    
                    if appointment.alerts.count >= 1 {
                        appointmentDetailsView.firstAlertLabel.text = "\(NSLocalizedString("First alert", comment: "")) \(NotificationAPI.sharedInstance.alertStringFromValue(minutes: appointment.alerts[0]))"
                    } else {
                        appointmentDetailsView.firstAlertLabel.text = ""
                    }
                    
                    if appointment.alerts.count >= 2 {
                        appointmentDetailsView.secondAlertLabel.text = "\(NSLocalizedString("Second alert", comment: "")) \(NotificationAPI.sharedInstance.alertStringFromValue(minutes: appointment.alerts[1]))"
                    } else {
                        appointmentDetailsView.secondAlertLabel.text = ""
                    }
                } else {
                    appointmentDetailsView.notificationsLabel.text = NSLocalizedString("Notifications Off", comment: "")
                    appointmentDetailsView.firstAlertLabel.text = ""
                    appointmentDetailsView.secondAlertLabel.text = ""
                }
                
                appointmentDetailsView.sizeToFit()
                
                previewController.view = appointmentDetailsView
                previewController.preferredContentSize = .init(width: appointmentDetailsView.contentView.bounds.width, height: appointmentDetailsView.getNeededHeight())
                
                return previewController
            case is Update:
                let update = (self.content as! Update)
                let previewController = UIViewController()
                let updateDetailsView = UpdateDetailsView()
                let first: Character = update.person!.name![update.person!.name!.startIndex]
                
                updateDetailsView.separatorUpdateDetails.backgroundColor = update.person!.color.first
                updateDetailsView.titleUpdateDetails.text = update.name!
                updateDetailsView.personColorView.backgroundColor = update.person!.color.first
                updateDetailsView.personColorView.layer.cornerRadius = updateDetailsView.personColorView.frame.height / 2
                updateDetailsView.firstLetterNamePersonButton.setTitle(String(first), for: .normal)
                dateFormatter.dateFormat = "HH:mm"
                updateDetailsView.hourUpdateDetails.text = dateFormatter.string(from: update.date!)
                dateFormatter.dateFormat = "EEEE dd MMMM yyyy"
                updateDetailsView.dateUpdateDetails.text = dateFormatter.string(from: update.date!)
                
                updateDetailsView.sizeToFit()
                
                previewController.view = updateDetailsView
                previewController.preferredContentSize = .init(width: updateDetailsView.contentView.bounds.width, height: updateDetailsView.getNeededHeight())
                
                return previewController
            default:
                return nil
            }
            }, actionProvider: { suggestedActions in
                return self.makeContextMenu(content: self.content)
        })
    }
    
    func makeContextMenu(content: AnyObject) -> UIMenu {
        let errorTitle = NSLocalizedString("Error", comment: "")
        let errorMessage = NSLocalizedString("The elimination was not successful", comment: "")
        let error = UIAlertController(title: errorTitle, message: errorMessage, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.controller.reloadTableViewRows()
            self.controller.tableView.reloadData()
        }
        error.addAction(okAction)
        
        switch content {
        case is Appointment:
            let appointment = (content as! Appointment)
            // Create a UIAction for sharing
            let children: [UIMenuElement]!
            
            let completed = UIAction(title: NSLocalizedString("Completed", comment: ""), image: UIImage(systemName: "checkmark.circle")) { action in
                // Show system share sheet
                if (appointmentAPI.check(id: appointment.identifier!) == true) {
                    self.controller.reloadTableViewRows()
                    self.controller.tableView.reloadData()
                }
            }
            
            if appointment.checked {
                completed.title = NSLocalizedString("Completed", comment: "")
                completed.image = UIImage(systemName: "checkmark.circle.fill")
            } else {
                completed.title = NSLocalizedString("Complete", comment: "")
                completed.image = UIImage(systemName: "checkmark.circle")
            }
            
            let details = UIAction(title: NSLocalizedString("Details", comment: ""), image: UIImage(systemName: "calendar")) { action in
                // Show system share sheet
                let appointmentDetailsViewController = storyBoard.instantiateViewController(withIdentifier: "AppointmentDetailsIdentifier") as! AppointmentDetailsViewController
                
                appointmentDetailsViewController.appointmentPassed = appointment
                appointmentDetailsViewController.indexPerson = self.indexPerson
                self.controller.navigationController!.pushViewController(appointmentDetailsViewController,animated: true)
            }

            let edit = UIAction(title: NSLocalizedString("Edit", comment: ""), image: UIImage(systemName: "square.and.pencil")) { action in
                // Show system share sheet
                let appointmentDetailsViewController = storyBoard.instantiateViewController(withIdentifier: "AppointmentDetailsIdentifier") as! AppointmentDetailsViewController
                
                appointmentDetailsViewController.appointmentPassed = appointment
                appointmentDetailsViewController.indexPerson = self.indexPerson
                
                self.controller.navigationController!.pushViewController(appointmentDetailsViewController,animated: true)
                appointmentDetailsViewController.performSegue(withIdentifier: "AppointmentEditSegueIdentifier", sender: nil)
            }
            
            let confirmDelete = UIAction(title: NSLocalizedString("Delete", comment: ""), image: UIImage(systemName: "trash")){ action in
                if appointmentAPI.delete(id: appointment.identifier!) == true {
                    if authenticationAPI.isAuthenticated() {
                        synchronizationAPI.create(id: appointment.identifier!, op: -1)
                    }
                    
                    self.controller.reloadTableViewRows()
                    self.controller.tableView.reloadData()
                } else {
                    self.controller.present(error, animated: true)
                }
            }
                    
            confirmDelete.attributes = UIMenuElement.Attributes.destructive
                    
            let delete = UIMenu(title: NSLocalizedString("Delete", comment: ""), image: UIImage(systemName: "trash"), identifier: nil, options: .destructive, children: [confirmDelete])
            
            children = [completed, details, edit, delete]
            
            // Create and return a UIMenu with the share action
            return UIMenu(title: "", children: children) //TO DO cambiare i constraint dei figli qui
        case is Update:
            let update = (content as! Update)
            // Create a UIAction for sharing
            let children: [UIMenuElement]!
            
            let details = UIAction(title: NSLocalizedString("Details", comment: ""), image: UIImage(systemName: "calendar")) { action in
                // Show system share sheet
                let updateDetailsViewController = storyBoard.instantiateViewController(withIdentifier: "UpdateDetailsIdentifier") as! UpdateDetailsViewController

                updateDetailsViewController.update = update
                self.controller.navigationController!.pushViewController(updateDetailsViewController,animated: true)
            }
            
            let confirmDelete = UIAction(title: NSLocalizedString("Delete", comment: ""), image: UIImage(systemName: "trash")){ action in
                if updateAPI.delete(id: update.identifier!) == true {
                    if authenticationAPI.isAuthenticated() {
                          synchronizationAPI.create(id: update.identifier!, op: -1)
                    }
                    
                    self.controller.reloadTableViewRows()
                    self.controller.tableView.reloadData()
                } else {
                    self.controller.present(error, animated: true)
                }
            }
                    
            confirmDelete.attributes = UIMenuElement.Attributes.destructive
                    
            let delete = UIMenu(title: NSLocalizedString("Delete", comment: ""), image: UIImage(systemName: "trash"), identifier: nil, options: .destructive, children: [confirmDelete])
            
            children = [details, delete]
            
            // Create and return a UIMenu with the share action
            return UIMenu(title: "", children: children) //TO DO cambiare i constraint dei figli qui
        default:
            return UIMenu(title: NSLocalizedString("Error", comment: ""))
        }

    }
    
}

class DiaryTableViewCell: UITableViewCell {
    
    var content: AnyObject!
    var controller: DiaryViewController!
    var indexPath: IndexPath!
    var indexPerson: Int!
    let current = Date()
    
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var detailsView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var notificationsImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        detailsView.layer.cornerRadius = 12
        detailsView.layer.shadowColor = UIColor.black.cgColor
        detailsView.layer.shadowOpacity = 0.3
        detailsView.layer.shadowRadius = 2
        detailsView.layer.shadowOffset = CGSize(width: 0, height: 1)
        
        let interaction = UIContextMenuInteraction(delegate: self)
        detailsView.addInteraction(interaction)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapDiary(sender:)))
        detailsView.addGestureRecognizer(tapGesture)
    }
    
    @objc func handleTapDiary(sender: UITapGestureRecognizer) {
        switch content {
        case is Appointment:
            let appointmentDetailsViewController = storyBoard.instantiateViewController(withIdentifier: "AppointmentDetailsIdentifier") as! AppointmentDetailsViewController
            
            appointmentDetailsViewController.appointmentPassed = (content as! Appointment)
            appointmentDetailsViewController.indexPerson = indexPerson

            self.controller.navigationController!.pushViewController(appointmentDetailsViewController,animated: true)
            
//            appointmentDetailsViewController.firstLetterNamePersonButton.isUserInteractionEnabled = false
        case is Update:
            let updateDetailsViewController = storyBoard.instantiateViewController(withIdentifier: "UpdateDetailsIdentifier") as! UpdateDetailsViewController
            
            updateDetailsViewController.update = (content as! Update)
            
            self.controller.navigationController!.pushViewController(updateDetailsViewController,animated: true)
        default:
            return
        }
        
    }

}
