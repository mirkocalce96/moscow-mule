//
//  AppDelegate.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 13/02/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import UIKit
import CoreData

let center = UNUserNotificationCenter.current()
let appID = "1500594697"
let facebookID = "104075287996321"
let facebookName = "Doctary: Your Medical Diary"
let appEmail = "infodoctary@gmail.com"
let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
let appBuild = Bundle.main.infoDictionary!["CFBundleVersion"] as! String
let appPrivacy = "https://www.facebook.com/104075287996321/posts/117539843316532/?d=n"
let appTerms = "https://www.facebook.com/104075287996321/posts/117546486649201/?d=n"
let server = "http://localhost:8080"

let appointmentAPI = AppointmentAPI.sharedInstance
let personAPI = PersonAPI.sharedInstance
let updateAPI = UpdateAPI.sharedInstance
let notificationAPI = NotificationAPI.sharedInstance
let reminderAPI = ReminderAPI.sharedInstance
let authenticationAPI = AuthenticationAPI.sharedInstance
let synchronizationAPI = SynchronizationAPI.sharedInstance
let colorAPI = ColorAPI.sharedInstance

let synchronizationRest = SynchronizationRest.sharedInstance

let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
var authentication: Authentication!

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        let options: UNAuthorizationOptions = [.sound, .alert]
        
        center.requestAuthorization(options: options) {(granted , error) in
            if error != nil {
                print(error!)
            }
        }
        
        center.delegate = self
        let categories = UNNotificationCategory(identifier: "appointment", actions: [], intentIdentifiers: [], hiddenPreviewsBodyPlaceholder: nil, categorySummaryFormat: "%u %@", options: [])
        center.setNotificationCategories([categories])
        
        UIApplication.shared.isStatusBarHidden = false
        
        synchronizationRest.validateToken()
        synchronizationRest.setOnBackgroundSynchronization()
        
        return true
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        saveContext()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandeler: @escaping (UNNotificationPresentationOptions) -> Void){
        completionHandeler([.alert, .badge, .sound])
    }
    
    func sharedInstance() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentCloudKitContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentCloudKitContainer(name: "Moscow_Mule")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}
