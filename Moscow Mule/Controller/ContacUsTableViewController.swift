//
//  ContacUsTableViewController.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 06/06/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import MessageUI
import UIKit

class ContacUsTableViewController: UITableViewController, MFMailComposeViewControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FacebookIdentifier", for: indexPath)

            // Configure the cell...

            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "EmailIdentifier", for: indexPath)

            // Configure the cell...

            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            UIApplication.tryURL(urls: ["fb://profile/\(facebookID)", "http://www.facebook.com/\(facebookID)"])
        default:
            if MFMailComposeViewController.canSendMail() {
                let mail = MFMailComposeViewController()
                
                mail.mailComposeDelegate = self
                mail.setToRecipients(["\(appEmail)"])
                mail.setSubject("[v\(appVersion) b\(appBuild)] \(NSLocalizedString("Application support", comment: ""))")
                mail.setMessageBody("<h1>\(facebookName)</h1>", isHTML: true)
                
                present(mail, animated: true)
            } else {
                let alert = UIAlertController(title: NSLocalizedString("Error", comment: ""), message: NSLocalizedString("Cannot send email", comment: ""), preferredStyle: .alert)
                let ok = UIAlertAction(title: "Ok", style: .default) { (placehorder) in
                    self.dismiss(animated: true, completion: nil)
                }

                alert.addAction(ok)
                self.present(alert, animated: true)
            }
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 28
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 28
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer: UITableViewHeaderFooterView = UITableViewHeaderFooterView()
        
        footer.contentView.backgroundColor = .systemGroupedBackground
        
        if section == 0 {
            footer.textLabel?.text = facebookName
        } else if section == 1 {
            footer.textLabel?.text = appEmail
        }
        
        return footer
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header: UITableViewHeaderFooterView = UITableViewHeaderFooterView()
        
        header.contentView.backgroundColor = .systemGroupedBackground
        
        return header
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else { return }
        
        header.textLabel?.textColor = UIColor.gray
        header.textLabel?.font = UIFont.systemFont(ofSize: 13)
        header.textLabel?.frame = header.frame
        header.textLabel?.textAlignment = .left
    }
    
    override func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        guard let footer = view as? UITableViewHeaderFooterView else { return }
        
        footer.textLabel?.textColor = UIColor.gray
        footer.textLabel?.font = UIFont.systemFont(ofSize: 13)
        footer.textLabel?.frame = footer.frame
        footer.textLabel?.textAlignment = .left
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }

}
