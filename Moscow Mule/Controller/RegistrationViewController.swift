//
//  RegistrationViewController.swift
//  Moscow Mule
//
//  Created by Luigi Ascione on 09/07/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import UIKit

class RegistrationViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var textEmail: UITextField!
    @IBOutlet weak var textPassword: UITextField!
    @IBOutlet weak var textConfirmPassword: UITextField!
    @IBOutlet weak var textDetails: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.hideKeyboardWhenTappedAround()
        
        textEmail.delegate = self
        textEmail.tag = 0
        textPassword.delegate = self
        textPassword.tag = 1
        textConfirmPassword.delegate = self
        textConfirmPassword.tag = 2
        
        textDetails.numberOfLines = 0
        textDetails.textColor = .white
        textDetails.text = NSLocalizedString("The data collected during the registration phase will not be disclosed to third-party apps.", comment: "")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag >= 0 {
            guard checkUsername() else {
                let alert = UIAlertController(title: NSLocalizedString("Warning", comment: ""), message: NSLocalizedString("Email field is mandatory", comment: ""), preferredStyle: .alert)
                let ok = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .cancel, handler: nil)

                alert.addAction(ok)
                self.present(alert, animated: true)

                return false
            }
            
            if textField.tag >= 1 {
                guard checkPassword() else {
                    let alert = UIAlertController(title: NSLocalizedString("Warning", comment: ""), message: NSLocalizedString("Password field is mandatory", comment: ""), preferredStyle: .alert)
                    let ok = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .cancel, handler: nil)
                
                    alert.addAction(ok)
                    self.present(alert, animated: true)
                
                    return false
                }
                
                if textField.tag >= 2 {
                    guard checkConfirmPassword() else {
                        let alert = UIAlertController(title: NSLocalizedString("Warning", comment: ""), message: NSLocalizedString("Passwords do not match", comment: ""), preferredStyle: .alert)
                        let ok = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .cancel, handler: nil)
                    
                        alert.addAction(ok)
                        self.present(alert, animated: true)
                    
                        return false
                    }
                }
            }
        }
        
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            signUp()
            textField.resignFirstResponder()
        }
        
        return false
    }
    
    func checkUsername() -> Bool {
        if textEmail.text!.isEmpty{
            return false
        }
        
        return true
    }
    
    func checkPassword() -> Bool {
        if textPassword.text!.isEmpty{
            return false
        }
        
        return true
    }
    
    func checkConfirmPassword() -> Bool {
        if (textConfirmPassword.text! != textPassword.text!) {
            return false
        }
        
        return true
    }
    
   func signUp() {
        activityIndicator.startAnimating()
        let semaphore = DispatchSemaphore (value: 0)
        var request = URLRequest(url: URL(string: "\(server)/register?username=\(textEmail.text!)&password=\(textConfirmPassword.text!)")!,timeoutInterval: Double.infinity)
        request.httpMethod = "POST"

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            DispatchQueue.main.async {
                guard let data = data else {
                    let alert = UIAlertController(title: NSLocalizedString("Error", comment: ""), message: NSLocalizedString(error!.localizedDescription, comment: ""), preferredStyle: .alert)
                    let ok = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .cancel, handler: nil)
                
                    alert.addAction(ok)
                    self.present(alert, animated: true)
                    
                    self.activityIndicator.stopAnimating()
                    return
                }
            
                let body = String(data: data, encoding: .utf8)!
                let httpResponse = response as? HTTPURLResponse

                guard httpResponse?.statusCode == 200 else {
                    let alert = UIAlertController(title: NSLocalizedString("Warning", comment: ""), message: NSLocalizedString(body, comment: ""), preferredStyle: .alert)
                    let ok = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .cancel, handler: nil)
                
                    alert.addAction(ok)
                    self.present(alert, animated: true)
                    
                    self.activityIndicator.stopAnimating()
                    return
                }
                
                let alert = UIAlertController(title: NSLocalizedString("Registration completed successfully", comment: ""), message: "\(NSLocalizedString("You will receive an email at", comment: "")) \(self.textEmail.text!) \(NSLocalizedString("to confirm your email address", comment: ""))", preferredStyle: .alert)
                let ok = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default) { (UIAlertAction) in
                    self.dismiss(animated: true, completion: nil)
                }
                
                alert.addAction(ok)
                self.present(alert, animated: true)
                self.activityIndicator.stopAnimating()
            }
            semaphore.signal()
        }

        task.resume()
        semaphore.wait()
    }

}
