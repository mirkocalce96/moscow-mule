//
//  NewProfileViewController.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 25/02/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import Foundation
import UIKit

let gend = [NSLocalizedString("Not Specified", comment: ""), NSLocalizedString("Male", comment: ""), NSLocalizedString("Female", comment: "")]

class NewPersonTableViewController: UITableViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, UICollectionViewDelegate, UICollectionViewDataSource {

    let personAPI = PersonAPI.sharedInstance
    private var dpShowDateVisible = false
    private var dpShowGenderVisible = false
    let colors = ColorAPI.sharedInstance.colors
    let attributes = PersonAttributes()
    var firstTime = true
    
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var dateOfBirthDetail: UILabel!
    @IBOutlet weak var dateOfBirth: UIDatePicker!
    @IBOutlet weak var genderDetail: UILabel!
    @IBOutlet weak var gender: UIPickerView!
    @IBOutlet weak var colorCollection: UICollectionView!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var firstLetter: UILabel!
    @IBOutlet weak var actionButton: UIBarButtonItem!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.hideKeyboardWhenTappedAround()
        
        gender.delegate = self
        gender.dataSource = self
        
        colorCollection.register(UINib.init(nibName: "ColorViewCell", bundle: nil), forCellWithReuseIdentifier: "ColorIdentifier")
        colorCollection.delegate = self
        colorCollection.dataSource = self
        
//        dateOfBirth.maximumDate = Date()
        
        tableView.estimatedRowHeight = 150
        tableView.rowHeight = UITableView.automaticDimension
        
        colorView.layer.cornerRadius = colorView.frame.height / 2
        colorView.layer.masksToBounds = true
        
        dpShowColorChanged(index: 0)
        dpShowDateChanged()
        dpShowGenderChanged()
        dpShowNameChanged()
        
        actionButton.isEnabled = false
        isModalInPresentation = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        dpShowColorChanged(index: 0)
        dpShowDateChanged()
        dpShowNameChanged()
        dpShowGenderChanged()

        firstTime = false
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return gend.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return gend[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        dpShowGenderChanged()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
     
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 1:
            return 5
        default:
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 && indexPath.row == 1 {
            toggleShowDateDatepicker()
            dpShowDateChanged()
        } else if (indexPath.section == 1 && indexPath.row == 3) {
            toggleShowGenderPickerview()
            dpShowGenderChanged()
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if !dpShowDateVisible && indexPath.section == 1 &&  indexPath.row == 2 {
            return 0
        } else if !dpShowGenderVisible && indexPath.section == 1 &&  indexPath.row == 4 {
            return 0
        } else {
            return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }

    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.00001
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 28
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer: UITableViewHeaderFooterView = UITableViewHeaderFooterView()
        
        footer.contentView.backgroundColor = .systemGroupedBackground
        
        return footer
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header: UITableViewHeaderFooterView = UITableViewHeaderFooterView()
        
        header.contentView.backgroundColor = .systemGroupedBackground
        
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return colors.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ColorIdentifier", for: indexPath) as! ColorViewCell
        
        cell.colorView.insertGradientEffect(firstColor: colors[indexPath.row].first, secondColor: colors[indexPath.row].second, thirdColor: nil, cornerRadius: cell.colorView.frame.height / 2)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        dpShowColorChanged(index: indexPath.row)
    }
    
    private func toggleShowDateDatepicker() {
         dpShowDateVisible = !dpShowDateVisible
     
         tableView.beginUpdates()
         tableView.endUpdates()
    }
    
    private func toggleShowGenderPickerview() {
         dpShowGenderVisible = !dpShowGenderVisible
     
         tableView.beginUpdates()
         tableView.endUpdates()
    }
    
    private func dpShowGenderChanged() {
        genderDetail.text = gend[gender.selectedRow(inComponent: 0)]
        
        switch gender.selectedRow(inComponent: 0) {
        case 1:
            attributes.gender = "Male"
        case 2:
            attributes.gender = "Female"
        default:
            attributes.gender = "Not Specified"
        }
        
        dpShowAddChanged()
    }
    
    private func dpShowDateChanged() {
        let dateFormatter = DateFormatter(); dateFormatter.dateFormat = "d MMMM yyyy"
        let mydt = dateFormatter.string(from: dateOfBirth.date)

        dateOfBirthDetail.text = mydt
        attributes.dateOfBirth = dateOfBirth.date
        
        dpShowAddChanged()
    }
    
    private func dpShowColorChanged(index: Int) {
        colorView.insertGradientEffect(firstColor: colors[index].first, secondColor: colors[index].second, thirdColor: nil, cornerRadius: colorView.frame.height / 2)
        attributes.color = colors[index]
        
        dpShowAddChanged()
    }
    
    private func dpShowNameChanged() {
        if (name.text!.isEmpty) {
            firstLetter.text = ""
        } else {
            let first: Character = name.text![name.text!.startIndex]
            
            firstLetter.text = String(first)
        }
        
        attributes.name = name.text!
        
        dpShowAddChanged()
    }
    
    func dpShowAddChanged() {
        if !actionButton.isEnabled && !firstTime {
            actionButton.isEnabled = true
            isModalInPresentation = true
        }
    }
    
    @IBAction func dpShowNameAction(_ sender: Any) {
        dpShowNameChanged()
    }
    
    @IBAction private func dpShowDateAction(sender: UIDatePicker) {
        let alert = UIAlertController(title: NSLocalizedString("Warning", comment: ""), message: NSLocalizedString("The date of birth cannot be greater than today's date", comment: ""), preferredStyle: .alert)
        let action = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: nil)
        
        alert.addAction(action)

        let today = Date()

        if dateOfBirth.date > today {
            dateOfBirth.setDate(today, animated: true)
            present(alert, animated: true, completion: nil)
        }
        
        dpShowDateChanged()
    }
    
    @IBAction func add(_ sender: Any) {
        let alertName = UIAlertController(title: NSLocalizedString("Warning", comment: ""), message: NSLocalizedString("Name field is mandatory", comment: ""), preferredStyle: .alert)
        alertName.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        
        let errorTitle = NSLocalizedString("Error", comment: "")
        let errorMessage = NSLocalizedString("The insertion was not successful", comment: "")
        let error = UIAlertController(title: errorTitle, message: errorMessage, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.performSegue(withIdentifier: "backFromNewPerson", sender: self)
        }
        
        guard (name.hasText) else { self.present(alertName, animated: true); return }
        
        if let new = personAPI.create(attributes: attributes) {
            persons = personAPI.getAll()
            
            if authenticationAPI.isAuthenticated() {
                synchronizationAPI.create(id: new.personId!, op: 1)
            }
            
            self.performSegue(withIdentifier: "backFromNewPerson", sender: self)
        } else {
            error.addAction(okAction)
            self.present(error, animated: true)
        }
    }
    
}
