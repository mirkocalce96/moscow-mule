//
//  AppointmentDetailsViewController.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 19/05/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import UIKit

class AppointmentDetailsViewController: UIViewController, UIAdaptivePresentationControllerDelegate {
    @IBOutlet weak var personColorView: UIView!
    @IBOutlet weak var firstLetterNamePersonButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var notificationsLabel: UILabel!
    @IBOutlet weak var firstAlertLabel: UILabel!
    @IBOutlet weak var secondAlertLabel: UILabel!
    @IBOutlet weak var notesTextView: UITextView!
    @IBOutlet weak var barBottom: UIView!
    @IBOutlet weak var typeLabel: UILabel!
    
    var appointmentPassed: Appointment!
    var indexPerson: Int!
    var controller: AppointmentTableViewController!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.systemMaterial)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = barBottom.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        barBottom.insertSubview(blurEffectView, at: 0)
        
        
        guard appointmentPassed != nil else {return }
        
        updateData()
    }
    
    func updateData() {
        let first: Character = self.appointmentPassed.person!.name![self.appointmentPassed.person!.name!.startIndex]
        
        titleLabel.text = appointmentPassed.name
        locationLabel.text = appointmentPassed.place
        personColorView.backgroundColor = self.appointmentPassed.person!.color.first
        personColorView.layer.cornerRadius = personColorView.frame.height / 2
        firstLetterNamePersonButton.setTitle(String(first), for: .normal)
        dateFormatter.dateFormat = "HH:mm"
        hourLabel.text = dateFormatter.string(from: self.appointmentPassed.date!)
        dateFormatter.dateFormat = "EEEE dd MMMM yyyy"
        dateLabel.text = dateFormatter.string(from: self.appointmentPassed.date!)
        
        typeLabel.text! = NSLocalizedString("Appointment", comment: "")
        if appointmentPassed.checked {
            typeLabel.text! += " \(NSLocalizedString("completed", comment: ""))"
        }
        
        if self.appointmentPassed.notifications {
           notificationsLabel.text = NSLocalizedString("Notifications On", comment: "")
           
           if self.appointmentPassed.alerts.count >= 1 {
               firstAlertLabel.text = "\(NSLocalizedString("First alert", comment: "")) \(NotificationAPI.sharedInstance.alertStringFromValue(minutes: self.appointmentPassed.alerts[0])) \(NSLocalizedString("before", comment: ""))"
           } else {
               firstAlertLabel.text = ""
           }
           
           if self.appointmentPassed.alerts.count >= 2 {
               secondAlertLabel.text = "\(NSLocalizedString("Second alert", comment: "")) \(NotificationAPI.sharedInstance.alertStringFromValue(minutes: self.appointmentPassed.alerts[1])) \(NSLocalizedString("before", comment: ""))"
           } else {
               secondAlertLabel.text = ""
           }
        } else {
           notificationsLabel.text = NSLocalizedString("Notifications Off", comment: "")
           firstAlertLabel.text = ""
           secondAlertLabel.text = ""
        }

        notesTextView.text = appointmentPassed.notes
        notesTextView.selectedTextRange = notesTextView.textRange(from: notesTextView.beginningOfDocument, to: notesTextView.beginningOfDocument)
        notesTextView.textContainerInset = UIEdgeInsets(top: 12, left: 0, bottom: 12, right: 0)
        notesTextView.textContainer.lineFragmentPadding = 0.0
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AppointmentEditSegueIdentifier"  {
            if let navigationController = segue.destination as? UINavigationController {
                if let updateViewController = navigationController.topViewController as? AppointmentTableViewController {
                    updateViewController.appointmentDetailsViewController = self
                    updateViewController.appointmentPassed = appointmentPassed
                    controller = updateViewController
                    segue.destination.presentationController?.delegate = self
                }
            }
        }
    }
        
    func presentationControllerDidAttemptToDismiss(_ presentationController: UIPresentationController) {
        let alertController = UIAlertController(title: NSLocalizedString("Do you confirm that you want to discard the changes made?", comment: ""), message: "", preferredStyle: .actionSheet)
        let actionContinue = UIAlertAction(title: NSLocalizedString("Continue to edit", comment: ""), style: .cancel, handler: nil)
        let actionDelete = UIAlertAction(title: NSLocalizedString("Discard", comment: ""), style: .default){ (placehorder) in
            self.controller.dismiss(animated: true, completion: nil)
        }
        
        actionContinue.setValue(UIColor.label, forKey: "titleTextColor")
        actionDelete.setValue(UIColor.systemRed, forKey: "titleTextColor")
        alertController.addAction(actionContinue)
        alertController.addAction(actionDelete)

        controller.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func diaryButton(_ sender: Any) {
        let diaryViewController = storyBoard.instantiateViewController(withIdentifier: "Diary") as! DiaryViewController
        diaryViewController.indexPassed = indexPerson
        
        var viewControllerArray = self.navigationController!.viewControllers
        viewControllerArray.removeLast()
        viewControllerArray.append(diaryViewController)
        
        self.navigationController!.setViewControllers(viewControllerArray, animated: true)
    }
    
    @IBAction func unwindFromAppointmentEdit(_ unwindSegue: UIStoryboardSegue) {
        // Use data from the view controller which initiated the unwind segue
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                self.updateData()
            }
        }
    }
    
    @IBAction func deleteAction(_ sender: Any) {
        let alertController = UIAlertController(title: NSLocalizedString("Do you confirm you want to delete this appointment?", comment: ""), message: "", preferredStyle: .actionSheet)
        let actionContinue = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
        let errorTitle = NSLocalizedString("Error", comment: "")
        let errorMessage = NSLocalizedString("The elimination was not successful", comment: "")
        let error = UIAlertController(title: errorTitle, message: errorMessage, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            _ = self.navigationController?.popViewController(animated: true)
        }
        error.addAction(okAction)
        let actionDelete = UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .default){ (placehorder) in
            if appointmentAPI.delete(id: self.appointmentPassed.identifier!) == true {
                if authenticationAPI.isAuthenticated() {
                    synchronizationAPI.create(id: self.appointmentPassed.identifier!, op: -1)
                }
                
                _ = self.navigationController?.popViewController(animated: true)
            } else {
                self.present(error, animated: true)
            }
        }
        
        actionContinue.setValue(UIColor.label, forKey: "titleTextColor")
        actionDelete.setValue(UIColor.systemRed, forKey: "titleTextColor")
        alertController.addAction(actionContinue)
        alertController.addAction(actionDelete)

        self.present(alertController, animated: true, completion: nil)
    }
    
}
