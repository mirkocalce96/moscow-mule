//
//  PersonDetailsViewController.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 21/05/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import UIKit

class PersonDetailsViewController: UIViewController, UIAdaptivePresentationControllerDelegate {

    @IBOutlet weak var personColorView: UIView!
    @IBOutlet weak var firstLetterNamePersonLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateOfBirthLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var barBottom: UIView!
    
    var personPassed: Person!
    var controller: PersonTableViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.systemMaterial)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = barBottom.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        barBottom.insertSubview(blurEffectView, at: 0)
        
        guard personPassed != nil else {return }
        
        self.updateData()
    }
    
    func updateData() {
        let first: Character = self.personPassed!.name![self.personPassed!.name!.startIndex]
        
        personColorView.backgroundColor = self.personPassed!.color.first
        personColorView.layer.cornerRadius = personColorView.frame.height / 2
        firstLetterNamePersonLabel.text = String(first)
        nameLabel.text = personPassed.name!
        dateFormatter.dateFormat = "dd MMMM yyyy"
        dateOfBirthLabel.text = dateFormatter.string(from: self.personPassed.dateOfBirth!)
        if personPassed.gender == "Not Specified" {
            genderLabel.text = ""
        } else {
            genderLabel.text = NSLocalizedString(personPassed.gender!, comment: "")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PersonEditSegueIdentifier"  {
            if let navigationController = segue.destination as? UINavigationController {
                if let personTableViewController = navigationController.topViewController as? PersonTableViewController {
                    personTableViewController.personDetailsViewController = self
                    personTableViewController.personPassed = personPassed
                    controller = personTableViewController
                    segue.destination.presentationController?.delegate = self
                }
            }
        }
    }
    
    func presentationControllerDidAttemptToDismiss(_ presentationController: UIPresentationController) {
        let alertController = UIAlertController(title: NSLocalizedString("Do you confirm that you want to discard the changes made?", comment: ""), message: "", preferredStyle: .actionSheet)
        let actionContinue = UIAlertAction(title: NSLocalizedString("Continue to edit", comment: ""), style: .cancel, handler: nil)
        let actionDelete = UIAlertAction(title: NSLocalizedString("Discard", comment: ""), style: .default){ (placehorder) in
            self.controller.dismiss(animated: true, completion: nil)
        }
        
        actionContinue.setValue(UIColor.label, forKey: "titleTextColor")
        actionDelete.setValue(UIColor.systemRed, forKey: "titleTextColor")
        alertController.addAction(actionContinue)
        alertController.addAction(actionDelete)

        controller.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func unwindFromPersonEdit(_ unwindSegue: UIStoryboardSegue) {
        // Use data from the view controller which initiated the unwind segue
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                self.updateData()
            }
        }
    }
    
    @IBAction func deleteAction(_ sender: Any) {
        let errorTitle = NSLocalizedString("Error", comment: "")
        let errorMessage = NSLocalizedString("The elimination was not successful", comment: "")
        let error = UIAlertController(title: errorTitle, message: errorMessage, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            _ = self.navigationController?.popViewController(animated: true)
        }
        error.addAction(okAction)
        let alertController = UIAlertController(title: NSLocalizedString("Do you confirm you want to delete this person?", comment: ""), message: "", preferredStyle: .actionSheet)
        let actionContinue = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
        let actionDelete = UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .default){ (placehorder) in
            let identifier = self.personPassed.personId!
            if personAPI.delete(id: self.personPassed.personId!) == true {
                if authenticationAPI.isAuthenticated() {
                    synchronizationAPI.create(id: identifier, op: -1)
                }
                
                _ = self.navigationController?.popViewController(animated: true)
            } else {
                self.present(error, animated: true)
            }
        }
        
        actionContinue.setValue(UIColor.label, forKey: "titleTextColor")
        actionDelete.setValue(UIColor.systemRed, forKey: "titleTextColor")
        alertController.addAction(actionContinue)
        alertController.addAction(actionDelete)

        self.present(alertController, animated: true, completion: nil)
    }
}
