//
//  AppointmentDetailsView.swift
//  Moscow Mule
//
//  Created by Luigi Ascione on 10/04/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import UIKit

class AppointmentDetailsView: UIView {

    @IBOutlet weak var dateAppointmentDetails: UILabel!
    @IBOutlet weak var hourAppointmentDetails: UILabel!
    @IBOutlet weak var titleAppointmentDetails: UILabel!
    @IBOutlet weak var locationAppointmentDetails: UILabel!
    @IBOutlet weak var notesAppointmentDetails: UILabel!
    @IBOutlet weak var separatorAppointmentDetails: UIView!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var notificationsLabel: UILabel!
    @IBOutlet weak var firstAlertLabel: UILabel!
    @IBOutlet weak var secondAlertLabel: UILabel!
    @IBOutlet weak var personColorView: UIView!
    @IBOutlet weak var firstLetterNamePersonButton: UIButton!
    @IBOutlet weak var typeLabel: UILabel!
    
    @IBOutlet weak var appointmentTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var dateTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var notificationsTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var notesTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var notesBottomConstraint: NSLayoutConstraint!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("AppointmentDetailsView", owner: self, options: nil)
        
        addSubview(contentView)

        notesAppointmentDetails.numberOfLines = 0
        
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    override func sizeToFit() {
        super.sizeToFit()
        
        firstAlertLabel.sizeToFit()
        secondAlertLabel.sizeToFit()
        notesAppointmentDetails.sizeToFit()
        contentView.sizeToFit()
        separatorAppointmentDetails.sizeToFit()
        
        self.contentView.layoutSubviews()
        self.updateConstraints()
    }
    
    func getNeededHeight() -> CGFloat {
        var height = CGFloat(0)

        height += appointmentTopConstraint.constant
        height += typeLabel.frame.height
        height += titleTopConstraint.constant
        height += titleAppointmentDetails.frame.height
        height += locationAppointmentDetails.frame.height
        height += dateTopConstraint.constant
        height += dateAppointmentDetails.frame.height
        height += hourAppointmentDetails.frame.height
        height += notificationsTopConstraint.constant
        height += notificationsLabel.frame.height
        height += firstAlertLabel.frame.height
        height += secondAlertLabel.frame.height
        height += notesTopConstraint.constant
        height += notesAppointmentDetails.frame.height
        height += notesBottomConstraint.constant

        return height
    }

}
