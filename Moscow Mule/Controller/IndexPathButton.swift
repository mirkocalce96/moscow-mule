//
//  IndexPathButton.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 10/04/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import UIKit

class IndexPathButton: UIButton {

    var indexPath: IndexPath? = nil
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
