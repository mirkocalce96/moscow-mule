//
//  GradientEffectExtension.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 08/03/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import Foundation
import UIKit


extension UIView {

    func insertGradientEffect(firstColor: UIColor, secondColor: UIColor, thirdColor: UIColor?, cornerRadius: CGFloat){
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bounds

        if thirdColor == nil {
            gradientLayer.colors = [firstColor.cgColor, secondColor.cgColor]
            gradientLayer.locations =  [0.0, 1.0]
        } else {
            gradientLayer.colors = [firstColor.cgColor, secondColor.cgColor, thirdColor!.cgColor]
            gradientLayer.locations =  [0.0, 0.5, 1.0]
        }

        // Verticale
//        gradientLayer.startPoint = CGPoint(x: 1.0, y: 0.0)
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradientLayer.cornerRadius = cornerRadius
        gradientLayer.name = "gradient"

        if (self.layer.sublayers?[0].name == "gradient") {
            self.layer.sublayers?.remove(at: 0)
        }

        layer.insertSublayer(gradientLayer, at: 0)
    }
}

import UIKit

@IBDesignable
class GradientView: UIView {
    
    @IBInspectable var firstColor: UIColor = UIColor.clear {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var secondColor: UIColor = UIColor.clear {
        didSet {
            updateView()
        }
    }

    @IBInspectable var isHorizontal: Bool = true {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var isOblique: Bool = true {
        didSet {
            updateView()
        }
    }
    
    override class var layerClass: AnyClass {
        get {
            return CAGradientLayer.self
        }
    }
    
    func updateView() {
        let layer = self.layer as! CAGradientLayer
        layer.colors = [firstColor, secondColor].map {$0.cgColor}
        if isOblique {
            layer.startPoint = CGPoint(x: 0, y: 0)
            layer.endPoint = CGPoint (x: 1, y: 1)
        } else {
            if (isHorizontal) {
                layer.startPoint = CGPoint(x: 0, y: 0.5)
                layer.endPoint = CGPoint (x: 1, y: 0.5)
            } else {
                layer.startPoint = CGPoint(x: 0.5, y: 0)
                layer.endPoint = CGPoint (x: 0.5, y: 1)
            }
        }
    }
    
}
