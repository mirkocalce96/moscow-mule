//
//  ViewController.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 13/02/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import UIKit

var appointments: [Appointment] = []
var persons: [Person] = []
let requestedComponents: Set<Calendar.Component> = [.month, .day, .hour, .minute]
let appearance = UINavigationBarAppearance()
var resultSearchController: UISearchController?

extension UIViewController {
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension Date {
    var minute: Int { Calendar.current.component(.minute, from: self) }
    
    func totalDistance(from date: Date, resultIn component: Calendar.Component) -> Int? {
        return Calendar.current.dateComponents([component], from: self, to: date).value(for: component)
    }

    func compare(with date: Date, only component: Calendar.Component) -> Int {
        let days1 = Calendar.current.component(component, from: self)
        let days2 = Calendar.current.component(component, from: date)
        return days1 - days2
    }

    func hasSame(_ component: Calendar.Component, as date: Date) -> Bool {
        return self.compare(with: date, only: component) == 0
    }

    func nextDate(roundedTo minutes: Int) -> Date {  
        if minute % 5 != 0 {
         return Calendar.current.nextDate(after: self,
                                  matching: .init(minute: Int((Double(minute)/Double(minutes)).rounded(.up) * Double(minutes)) % 60),
                                  matchingPolicy: .nextTime)!
        }
        return self
    }
}

class HomePageViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UIAdaptivePresentationControllerDelegate {
    
    let userCalendar = Calendar.current
    var listFirstLetterNameArray: [String] = Array()
    var listNameProfileArray: [String] = Array()
    let effectView = UIVisualEffectView()
    let current = Date()
    let searchController = UISearchController(searchResultsController: nil)
    var appointmentsSections = [String:[Appointment]]()
    var appointmentsKeys: [String] = []
    var personsSections = [String:Int]()
    var personsKeys: [String] = []
    var values: [Int] = []
    let dateFormatter = DateFormatter();
    var details: Appointment? = nil
    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    var controller: UIViewController!
    var search: Bool = false
    var refreshControl = UIRefreshControl()

    @IBOutlet weak var warningLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var navigation: UINavigationItem!
    
    class var sharedInstance: UITableView {
        struct Singleton {
            static let instance = UITableView()
        }

        return Singleton.instance
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.hideKeyboardWhenTappedAround()
        
        navigationItem.searchController = searchController
        searchController.searchBar.delegate = self

        checkForScrollViewInView(view: self.view)
        
        tableView.register(UINib.init(nibName: "AppointmentTableViewCell", bundle: nil), forCellReuseIdentifier: "AppointmentIdentifier")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.scrollsToTop = true
        
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        tableView.addSubview(refreshControl)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !search {
            persons = personAPI.getAll()
            reloadAppointmentsTableViewSections()
            tableView.reloadData()
        }
    }
    
    @objc func refresh(_ sender: AnyObject) {
        synchronizationRest.synchronization()
        tableView.reloadData()
        refreshControl.endRefreshing()
    }
    
    func presentationControllerDidAttemptToDismiss(_ presentationController: UIPresentationController) {
        let alertTitle = NSLocalizedString("Do you confirm that you want to discard the changes made?", comment: "")
        let continueTitle = NSLocalizedString("Continue to edit", comment: "")
        let deleteTitle = NSLocalizedString("Discard", comment: "")
        let alertController = UIAlertController(title: alertTitle, message: "", preferredStyle: .actionSheet)
        let actionContinue = UIAlertAction(title: continueTitle, style: .cancel, handler: nil)
        let actionDelete = UIAlertAction(title: deleteTitle, style: .default){ (placehorder) in
            self.controller.dismiss(animated: true, completion: nil)
        }
        
        actionContinue.setValue(UIColor.label, forKey: "titleTextColor")
        actionDelete.setValue(UIColor.systemRed, forKey: "titleTextColor")
        alertController.addAction(actionContinue)
        alertController.addAction(actionDelete)

        controller.present(alertController, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        searchBarCancelButtonClicked(self.searchController.searchBar)
        
        if segue.identifier == "NewAppointmentSegueIdentifier"  {
            if let navigationController = segue.destination as? UINavigationController {
                if let newAppintmentViewController = navigationController.topViewController as? NewAppointmentTableViewController {
                    controller = newAppintmentViewController
                    segue.destination.presentationController?.delegate = self
                }
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return appointmentsSections.count + 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        default:
            let newSection = section - 1
            return appointmentsSections[appointmentsKeys[newSection]]!.count
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        switch indexPath.section {
        case 0:
            return false
        default:
            return true
        }
    }
    
    func tableView(_ : UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?{
        let errorTitle = NSLocalizedString("Error", comment: "")
        let errorMessage = NSLocalizedString("The elimination was not successful", comment: "")
        let error = UIAlertController(title: errorTitle, message: errorMessage, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { UIAlertAction in }
        error.addAction(okAction)
        
        let newSection = indexPath.section - 1
        let appointment: Appointment = appointmentsSections[appointmentsKeys[newSection]]![indexPath.row]
        let edit = UIContextualAction(style: .normal, title: NSLocalizedString("Edit", comment: "")) { (action, view, nil) in
        let appointmentDetailsViewController = self.storyBoard.instantiateViewController(withIdentifier: "AppointmentDetailsIdentifier") as! AppointmentDetailsViewController
            
            appointmentDetailsViewController.appointmentPassed = appointment
            appointmentDetailsViewController.indexPerson = persons.firstIndex(of: appointment.person!)
            
            self.navigationController!.pushViewController(appointmentDetailsViewController,animated: true)
            appointmentDetailsViewController.performSegue(withIdentifier: "AppointmentEditSegueIdentifier", sender: nil)
        }
        let delete = UIContextualAction(style: .destructive, title: NSLocalizedString("Delete", comment: "")) { (action, view, nil) in
            if (appointmentAPI.delete(id: (appointment.identifier!))) == true {
                if authenticationAPI.isAuthenticated() {
                    synchronizationAPI.create(id: appointment.identifier!, op: -1)
                }
                
                self.deleteRowFromSection(indexPath: indexPath)
            } else {
                self.present(error, animated: true)
            }
        }
        
        delete.image = UIImage(systemName: "trash")
        edit.image = UIImage(systemName: "square.and.pencil")
        
        return UISwipeActionsConfiguration(actions: [delete, edit])
    }
    
    func tableView(_ : UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?{
        let newSection = indexPath.section - 1
        let appointment: Appointment = appointmentsSections[appointmentsKeys[newSection]]![indexPath.row]
        let check = UIContextualAction(style: .normal, title: NSLocalizedString("Complete", comment: "")) { (action, view, nil) in
            if (appointmentAPI.check(id: appointment.identifier!) == true) {
                self.deleteRowFromSection(indexPath: indexPath)
            }
        }
        
        check.image = UIImage(systemName: "checkmark.circle")
        check.backgroundColor = .systemBlue
        
        return UISwipeActionsConfiguration(actions: [check])
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewAppointmentIdentifier") as! NewAppointmentTableViewCell
                        
            return cell
        default:
            let newSection = indexPath.section - 1
            let cell = tableView.dequeueReusableCell(withIdentifier: "AppointmentIdentifier") as! AppointmentTableViewCell
            let appointment: Appointment = appointmentsSections[appointmentsKeys[newSection]]![indexPath.row]
            
            dateFormatter.dateFormat = "HH:mm"
            let hour = dateFormatter.string(from: appointment.date!).capitalized
            let first: Character = appointment.person!.name![appointment.person!.name!.startIndex]

            cell.typeLabel.text! = NSLocalizedString("Appointment", comment: "")
            if appointment.checked {
                cell.typeLabel.text! += " \(NSLocalizedString("completed", comment: ""))"
            }
            
            cell.indexPerson = persons.firstIndex(of: appointment.person!)
            cell.firstLetterNamePersonButton.setTitle(String(first), for: .normal)
            cell.personColorView.backgroundColor = appointment.person!.color.first
            cell.personColorView.layer.cornerRadius = cell.personColorView.frame.height / 2
            cell.controller = self
            cell.indexPath = indexPath
            cell.hourLabel.text = "\(hour)"
            cell.separatorView.backgroundColor = appointment.person!.color.first
            cell.titleLabel.text = appointment.name
            cell.selectionStyle = .none
            cell.layer.shadowOffset = CGSize(width: tableView.frame.width, height: 50)
            cell.layer.shadowColor = UIColor.black.cgColor
            cell.appointment = appointment
            
            switch appointment.notifications {
            case false:
                cell.notificationsImageView.image = UIImage(systemName: "bell.slash")
            default:
                cell.notificationsImageView.image = UIImage(systemName: "bell")
            }
            
            return cell
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 44
        default:
            return 92
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 0
        default:
            return 0.00001
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 0
        default:
            return 28
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer: UITableViewHeaderFooterView = UITableViewHeaderFooterView()
        
        footer.contentView.backgroundColor = .systemBackground
        
        return footer
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header: UITableViewHeaderFooterView = UITableViewHeaderFooterView()
        
        header.contentView.backgroundColor = .systemBackground
        
        return header
    }
    
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        guard let footer = view as? UITableViewHeaderFooterView else { return }
        
        footer.textLabel?.textColor = UIColor.label
        footer.textLabel?.font = UIFont.systemFont(ofSize: 13)
        footer.textLabel?.frame = footer.frame
        footer.textLabel?.textAlignment = .left
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let newSection = section - 1
        guard let header = view as? UITableViewHeaderFooterView else { return }
        
        dateFormatter.dateFormat = "yyyy-MM-dd"

        let headerDate = dateFormatter.date(from: appointmentsKeys[newSection])
        let todayDate = Date()
        dateFormatter.dateFormat = "EEEE dd MMMM yyyy"
        
        let date = dateFormatter.string(from: headerDate!)
        let today = dateFormatter.string(from: todayDate)
        
        if date == today {
            header.textLabel?.text = NSLocalizedString("Today", comment: "")
        } else {
            header.textLabel?.text = date
        }

        header.textLabel?.textColor = UIColor.label
        header.textLabel?.font = UIFont.systemFont(ofSize: 13)
        header.textLabel?.frame = header.frame
        header.textLabel?.textAlignment = .center
    }

    func scrollViewDidScrollToTop(_ scrollView: UIScrollView) {
        tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableView.ScrollPosition.top, animated: true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            search = false
            reloadAppointmentsTableViewSections()
        } else {
            search = true
            navigationItem.title = NSLocalizedString("Research", comment: "")
            reloadAppointmentsSectionsByFilter(filter: searchText)
        }
        tableView.reloadData()
        searchController.searchBar.placeholder = searchText
        searchController.searchBar.showsCancelButton = true
    }
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchController.searchBar.text = ""
        reloadAppointmentsTableViewSections()
        tableView.reloadData()
        searchController.searchBar.placeholder = NSLocalizedString("Search", comment: "")
        searchController.searchBar.showsCancelButton = false
        self.navigationItem.title = NSLocalizedString("Home", comment: "")
        search = false
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if !search {
            searchController.searchBar.showsCancelButton = false
            self.navigationItem.title = NSLocalizedString("Home", comment: "")
            search = false
        }
    }
    
    func checkForScrollViewInView(view: UIView) {
        for subview in view.subviews as [UIView] {

            if subview.isKind(of: UITextView.self) {
                (subview as! UITextView).scrollsToTop = false
            }

            if subview.isKind(of: UIScrollView.self) {
                (subview as! UIScrollView).scrollsToTop = false
            }

            if subview.isKind(of: UITableView.self) {
                (subview as! UITableView).scrollsToTop = false
            }

            if (subview.subviews.count > 0) {
                self.checkForScrollViewInView(view: subview)
            }
        }
    }
    
    func reloadAppointmentsTableViewSections() {
        appointments = appointmentAPI.getAllNotChecked()
        
        appointmentsSections = [String:[Appointment]]()
        appointmentsKeys = [String]()
        
        guard !appointments.isEmpty else {
                warningLabel.text = NSLocalizedString("You have not entered any appointment", comment: "")
                warningLabel.isHidden = false
            
                return
        }
        warningLabel.isHidden = true
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        for appointment in appointments {
            let data: String = dateFormatter.string(from: appointment.date!)
            
            var array: [Appointment]! = appointmentsSections[data, default: []]
            array.append(appointment)
            appointmentsSections.updateValue(array, forKey: data)
        }
        
        appointmentsKeys = appointmentsSections.keys.sorted(by: <)
    }
    
    func reloadAppointmentsSectionsByFilter(filter: String) {
        appointments = appointmentAPI.getAllByFilter(filter: filter)
        
        appointmentsSections = [String:[Appointment]]()
        appointmentsKeys = [String]()
        
        guard !appointments.isEmpty else {
                warningLabel.text = NSLocalizedString("The search did not return any results", comment: "")
                warningLabel.isHidden = false
            
                return
        }
        warningLabel.isHidden = true
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        for appointment in appointments {
            let data: String = dateFormatter.string(from: appointment.date!)
            
            var array: [Appointment]! = appointmentsSections[data, default: []]
            array.append(appointment)
            appointmentsSections.updateValue(array, forKey: data)
        }
        
        appointmentsKeys = appointmentsSections.keys.sorted(by: <)
    }

    func deleteRowFromSection(indexPath: IndexPath) {
        let newSection = indexPath.section - 1

        appointmentsSections[appointmentsKeys[newSection]]!.remove(at: indexPath.row)
        
        self.tableView.beginUpdates()
        
        switch appointmentsSections[appointmentsKeys[newSection]]!.count {
        case 0:
            appointmentsSections.removeValue(forKey: appointmentsKeys[newSection])
            appointmentsKeys.remove(at: newSection)
            
            let indexSet = NSMutableIndexSet()
            indexSet.add(indexPath.section)
            self.tableView.deleteSections(indexSet as IndexSet, with: .automatic)
        default:
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
        }
        
        self.tableView.endUpdates()
        
        appointments = appointmentAPI.getAllNotChecked()
        
        guard !appointments.isEmpty else { warningLabel.isHidden = false; return }
        warningLabel.isHidden = true
    }

    @IBAction func unwindFromNewAppointment(segue: UIStoryboardSegue) {
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                self.reloadAppointmentsTableViewSections()
                self.tableView.reloadData()
            }
        }
    }
}
