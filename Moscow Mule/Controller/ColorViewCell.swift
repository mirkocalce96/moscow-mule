//
//  ColorViewCell.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 10/03/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import Foundation
import UIKit

class ColorViewCell: UICollectionViewCell {

    @IBOutlet weak var colorView: GradientView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        colorView.layer.shadowOffset = CGSize(width: 50, height: 50)
        colorView.layer.cornerRadius = colorView.frame.height / 2
        colorView.layer.masksToBounds = true
    }

}
