//
//  Authentication+CoreDataProperties.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 20/07/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//
//

import Foundation
import CoreData


extension Authentication {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Authentication> {
        return NSFetchRequest<Authentication>(entityName: "Authentication")
    }

    @NSManaged public var password: String?
    @NSManaged public var token: String?
    @NSManaged public var username: String?
    @NSManaged public var identifier: String?

}
