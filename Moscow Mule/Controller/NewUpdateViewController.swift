//
//  UpdateViewController.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 09/03/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import UIKit

let dateFormatter = DateFormatter()

class NewUpdateViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    var sections: [String : [String]]!
    var indexPassed: Int!
    var dpShowDateVisible = false
    var customDatePickerTableViewCell: CustomDatePickerTableViewCell!
    var customDateDetailTableViewCell: CustomDateDetailTableViewCell!
    var datePassed: Date? = nil
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(textFieldTextChanged(sender:)), name: UITextField.textDidChangeNotification, object: nil)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 150
        tableView.rowHeight = UITableView.automaticDimension
        
        customDateDetailTableViewCell = (tableView.dequeueReusableCell(withIdentifier: "CustomDateDetailIdentifier") as! CustomDateDetailTableViewCell)
        customDatePickerTableViewCell = (tableView.dequeueReusableCell(withIdentifier: "CustomDatePickerIdentifier") as! CustomDatePickerTableViewCell)
        
        customDatePickerTableViewCell.controller = self
        
        switch datePassed {
        case nil:
            customDatePickerTableViewCell.datePicker.date = Date().nextDate(roundedTo: 5)
        default:
            customDatePickerTableViewCell.datePicker.date = datePassed!.nextDate(roundedTo: 5)
        }
        
        customDatePickerTableViewCell.dpShowDateChanged()
        
        dateFormatter.dateFormat = "EEEE d MMMM yyyy, HH:mm"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        sections = updateAPI.dictionary
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 2
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 28
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 28
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer: UITableViewHeaderFooterView = UITableViewHeaderFooterView()
        
        footer.contentView.backgroundColor = .systemBackground
        
        return footer
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header: UITableViewHeaderFooterView = UITableViewHeaderFooterView()
        
        header.contentView.backgroundColor = .systemBackground
        
        switch section {
        case 1:
            header.textLabel?.text = NSLocalizedString("SYMPTOMS", comment: "")
        case 2:
            header.textLabel?.text = NSLocalizedString("FEVER", comment: "")
        case 3:
            header.textLabel?.text = NSLocalizedString("FOOD", comment: "")
        case 4:
            header.textLabel?.text = NSLocalizedString("SLEEP", comment: "")
        default:
            header.textLabel?.text = ""
        }
        
        return header
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else { return }
        
        header.textLabel?.textColor = UIColor.gray
        header.textLabel?.font = UIFont.systemFont(ofSize: 13)
        header.textLabel?.frame = header.frame
        header.textLabel?.textAlignment = .left
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 && indexPath.row == 0 {
            customDatePickerTableViewCell.toggleShowDateDatepicker()
            customDatePickerTableViewCell.dpShowDateChanged()
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCollectionViewIdentifier") as! CustomCollectionTableViewCell

            cell.items = sections["symptoms"]
            cell.controller = self

            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCollectionViewIdentifier") as! CustomCollectionTableViewCell

            cell.items = sections["fever"]
            cell.controller = self

            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCollectionViewIdentifier") as! CustomCollectionTableViewCell

            cell.items = sections["food"]
            cell.controller = self

            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCollectionViewIdentifier") as! CustomCollectionTableViewCell

            cell.items = sections["sleep"]
            cell.controller = self

            return cell
        default:
            switch indexPath.row {
            case 0:
                return customDateDetailTableViewCell
            default:
                return customDatePickerTableViewCell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if !dpShowDateVisible && indexPath.section == 0 &&  indexPath.row == 1 {
            return 0
        } else if indexPath.section == 0 && indexPath.row == 1 {
            return 170
        } else if indexPath.section == 0 && indexPath.row == 0 {
            return UITableView.automaticDimension
        } else {
            return 130
        }
    }
    
    @objc func textFieldTextChanged(sender: AnyObject) {
        //your code
    }

    @IBAction func newCustomUpdate(_ sender: Any) {
        let date = self.customDatePickerTableViewCell.datePicker.date
        let alert = UIAlertController(title: NSLocalizedString("Custom update", comment: ""), message: "\(dateFormatter.string(from: date))", preferredStyle: .alert)
        let add = UIAlertAction(title: NSLocalizedString("Add", comment: ""), style: .default) { (placehorder) in
            if let update = updateAPI.create(name: alert.textFields![0].text!, date: date, person: persons[self.indexPassed], id: nil) {
                if authenticationAPI.isAuthenticated() {
                      synchronizationAPI.create(id: update.identifier!, op: 1)
                }
                
                self.performSegue(withIdentifier: "backFromNewUpdate", sender: self)
            } else {
                let errorTitle = NSLocalizedString("Error", comment: "")
                let errorMessage = NSLocalizedString("The insertion was not successful", comment: "")
                let error = UIAlertController(title: errorTitle, message: errorMessage, preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                    UIAlertAction in
                    self.performSegue(withIdentifier: "backFromNewUpdate", sender: self)
                }
                error.addAction(okAction)
                self.present(error, animated: true)
            }
        }
        let cancel = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
        
        add.isEnabled = false
        
        alert.addTextField { (textField) in
            textField.placeholder = NSLocalizedString("Description", comment: "")
        }

        NotificationCenter.default.addObserver(forName: UITextField.textDidChangeNotification, object: alert.textFields?[0],
                                               queue: OperationQueue.main) { (notification) -> Void in
            let textFieldName = alert.textFields![0]
            add.isEnabled = !textFieldName.text!.isEmpty
        }
        
        alert.addAction(add)
        alert.addAction(cancel)
        
        self.present(alert, animated: true)
    }
}

class CustomDatePickerTableViewCell: UITableViewCell {
    
    var controller: NewUpdateViewController!
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func toggleShowDateDatepicker() {
        controller.dpShowDateVisible = !controller.dpShowDateVisible
     
        controller.tableView.beginUpdates()
        controller.tableView.endUpdates()
    }
    
    func dpShowDateChanged() {
        let dateFormatter = DateFormatter(); dateFormatter.dateFormat = "EEEE d MMMM yyyy, HH:mm"
        let mydt = dateFormatter.string(from: datePicker.date)
        
        controller.customDateDetailTableViewCell.detailLabel.text = mydt
    }
    
    
    @IBAction func dpShowDateAction(_ sender: Any) {
        dpShowDateChanged()
    }
    
}

class CustomDateDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}

class CustomCollectionTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var items: [String]!
    var controller: NewUpdateViewController!
    
    @IBOutlet weak var customCollectionView: UICollectionView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        customCollectionView.register(UINib.init(nibName: "NewUpdateCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "NewUpdateCollectionViewCell")
        customCollectionView.delegate = self
        customCollectionView.dataSource = self
        
        if let layout = customCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.sectionHeadersPinToVisibleBounds = true
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewUpdateCollectionViewCell", for: indexPath) as! NewUpdateCollectionViewCell

        cell.nameLabel.text = items[indexPath.row]

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var name: String!
        var alert: UIAlertController!
        var yes: UIAlertAction!
        var no: UIAlertAction!
        let date = self.controller.customDatePickerTableViewCell.datePicker.date
        
        name = items[indexPath.row]
        
        alert = UIAlertController(title: "\(NSLocalizedString("Add ", comment: ""))\"\(name!)\" \(NSLocalizedString("to your diary", comment: ""))?", message: "\(dateFormatter.string(from: date))", preferredStyle: .alert)
        yes = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default) { (placehorder) in
            if let update = updateAPI.create(name: name, date: date, person: persons[self.controller.indexPassed], id: nil) {
                if authenticationAPI.isAuthenticated() {
                      synchronizationAPI.create(id: update.identifier!, op: 1)
                }
                
                self.controller.performSegue(withIdentifier: "backFromNewUpdate", sender: self)
            } else {
                let errorTitle = NSLocalizedString("Error", comment: "")
                let errorMessage = NSLocalizedString("The insertion was not successful", comment: "")
                let error = UIAlertController(title: errorTitle, message: errorMessage, preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                    UIAlertAction in
                    self.controller.performSegue(withIdentifier: "backFromNewUpdate", sender: self)
                }
                error.addAction(okAction)
                self.controller.present(error, animated: true)
            }
        }
        no = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .cancel, handler: nil)
        
        alert.addAction(yes)
        alert.addAction(no)
        
        controller.present(alert, animated: true)
    }
    
    private func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderSectionIdentifier", for: indexPath as IndexPath)
            
            return headerView
        default:
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "FooterSectionIdentifier", for: indexPath as IndexPath)
            
            return footerView
        }
    }
    
}
