//
//  PersonColor.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 10/03/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import CoreData
import Foundation
import UIKit

public class PersonColor: NSObject, NSCoding {
    
    var first: UIColor
    var second: UIColor
    
    init(first: UIColor, second: UIColor) {
        self.first = first
        self.second = second
    }
    
    required convenience public init(coder decoder: NSCoder) {
        self.init(first: decoder.decodeObject(forKey: "first") as! UIColor, second: decoder.decodeObject(forKey: "second") as! UIColor)
    }

    public func encode(with coder: NSCoder) {
        coder.encode(self.first, forKey: "first")
        coder.encode(self.second, forKey: "second")
    }
    
}
