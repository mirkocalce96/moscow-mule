//
//  CoreDataController.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 17/02/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import Foundation
import CoreData

class CoreDataController: NSObject {
    
    fileprivate var context: NSManagedObjectContext
    
    class var sharedInstance: CoreDataController {
        struct Singleton {
            static let instance = CoreDataController()
        }

        return Singleton.instance
    }
    
    override init() {
        let appDelegate: AppDelegate = AppDelegate().sharedInstance()
        self.context = appDelegate.persistentContainer.viewContext
        super.init()
    }
    
    func getContext() -> NSManagedObjectContext {
        return self.context
    }
    
}
