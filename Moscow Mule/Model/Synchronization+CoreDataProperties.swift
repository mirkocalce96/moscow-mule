//
//  Synchronization+CoreDataProperties.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 28/07/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//
//

import Foundation
import CoreData


extension Synchronization {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Synchronization> {
        return NSFetchRequest<Synchronization>(entityName: "Synchronization")
    }

    @NSManaged public var identifier: String?
    @NSManaged public var operation: Int

}
