//
//  Person.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 19/02/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import UIKit
import Foundation

class PersonAttributes {
    
    private var colorAttributes: PersonColor, dateOfBirthAttributes: Date, genderAttributes: String, nameAttributes: String, personIdAttributes: String?
    
    var color: PersonColor {
        get {
            return self.colorAttributes
        }
        set(new) {
            self.colorAttributes = new
        }
    }
    
    var dateOfBirth: Date {
        get {
            return self.dateOfBirthAttributes
        }
        set(new) {
            self.dateOfBirthAttributes = new
        }
    }
    
    var gender: String {
        get {
            return self.genderAttributes
        }
        set(new) {
            self.genderAttributes = new
        }
    }
    
    var name: String {
        get {
            return self.nameAttributes
        }
        set(new) {
            self.nameAttributes = new
        }
    }
    
    var id: String? {
        get {
            return self.personIdAttributes
        }
        set(new) {
            self.personIdAttributes = new
        }
    }
    
    
    init() {
        self.colorAttributes = ColorAPI.sharedInstance.colors[0]
        self.dateOfBirthAttributes = Date()
        self.genderAttributes = ""
        self.nameAttributes = ""
        self.personIdAttributes = nil
    }
 
}
