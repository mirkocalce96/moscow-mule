//
//  Person+CoreDataClass.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 21/04/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Person)
public class Person: NSManagedObject {

}
