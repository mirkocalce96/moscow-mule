//
//  Appointment+CoreDataProperties.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 19/02/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//
//

import Foundation
import CoreData


extension Appointment {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Appointment> {
        return NSFetchRequest<Appointment>(entityName: "Appointment")
    }

    @NSManaged public var alerts: [Int]
    @NSManaged public var checked: Bool
    @NSManaged public var date: Date?
    @NSManaged public var identifier: String?
    @NSManaged public var name: String?
    @NSManaged public var notes: String?
    @NSManaged public var notifications: Bool
    @NSManaged public var place: String?
    @NSManaged public var person: Person?
    
}
