//
//  Update+CoreDataProperties.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 15/04/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//
//

import Foundation
import CoreData


extension Update {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Update> {
        return NSFetchRequest<Update>(entityName: "Update")
    }

    @NSManaged public var name: String?
    @NSManaged public var date: Date?
    @NSManaged public var identifier: String?
    @NSManaged public var person: Person?

}
