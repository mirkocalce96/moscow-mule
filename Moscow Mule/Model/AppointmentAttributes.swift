//
//  AppointmentAttributes.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 22/02/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import Foundation

class AppointmentAttributes {
    
    private var checkedAttributes: Bool, identifierAttributes: String?, nameAttributes: String, dateAttributes: Date, placeAttributes: String, personAttributes: Person?, notesAttributes: String, notificationsAttributes: Bool, alertsAttributes: [Int]
    
    var alerts: [Int] {
        get {
            return self.alertsAttributes
        }
        set(new) {
            self.alertsAttributes = new
        }
    }
    
    var checked: Bool {
        get {
            return self.checkedAttributes
        }
        set(new) {
            self.checkedAttributes = new
        }
    }
    
    var date: Date {
        get {
            return self.dateAttributes
        }
        set(new) {
            self.dateAttributes = new
        }
    }
    
    var identifier: String? {
        get {
            return self.identifierAttributes
        }
        set(new) {
            self.identifierAttributes = new
        }
    }
    
    var name: String {
        get {
            return self.nameAttributes
        }
        set(new) {
            self.nameAttributes = new
        }
    }
    
    var notifications: Bool {
        get {
            return self.notificationsAttributes
        }
        set(new) {
            self.notificationsAttributes = new
        }
    }
    
    var notes: String {
        get {
            return self.notesAttributes
        }
        set(new) {
            self.notesAttributes = new
        }
    }
    
    var person: Person? {
        get {
            return self.personAttributes
        }
        set(new) {
            self.personAttributes = new
        }
    }
    
    var place: String {
        get {
            return self.placeAttributes
        }
        set(new) {
            self.placeAttributes = new
        }
    }
    
    
    init() {
        self.checkedAttributes = false
        self.identifierAttributes = nil
        self.nameAttributes = ""
        self.dateAttributes = Date()
        self.placeAttributes = ""
        self.personAttributes = nil
        self.notesAttributes = ""
        self.notificationsAttributes = true
        self.alertsAttributes = []
    }
 
}
