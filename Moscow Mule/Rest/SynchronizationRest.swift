//
//  SynchronizationRest.swift
//  Moscow Mule
//
//  Created by Mirko Calce on 31/07/2020.
//  Copyright © 2020 Titolo. All rights reserved.
//

import Foundation

class SynchronizationRest {
    var backgroundTimer: Timer?
    
    class var sharedInstance: SynchronizationRest {
        struct Singleton {
            static let instance = SynchronizationRest()
        }

        return Singleton.instance
    }
    
    init() {
        print("\(Date()) | Class: SynchronizationRest | Function: init() | Input: void | Status: Started")
        print("\(Date()) | Class: SynchronizationRest | Function: init() | Input: void | Status: Finished | Return: void")
    }
    
    func validateToken() {
        authentication = nil
        if let instance = authenticationAPI.read() {
            let semaphore = DispatchSemaphore (value: 0)
            var request = URLRequest(url: URL(string: "\(server)/login?username=\(instance.username!)&password=\(instance.password!)")!,timeoutInterval: Double.infinity)
            
            request.httpMethod = "POST"
            request.timeoutInterval = 10

            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                DispatchQueue.main.async {
                    guard let data = data else {
                        authenticationAPI.logout()
                        return
                    }
                
                    let body = String(data: data, encoding: .utf8)!
                    let httpResponse = response as? HTTPURLResponse

                    guard httpResponse?.statusCode == 200 else {
                        authenticationAPI.logout()
                        return
                    }
                    
                    authentication = authenticationAPI.login(username: instance.username, password: instance.password, token: body)!
                }
                semaphore.signal()
            }

            task.resume()
            semaphore.wait()
        }
    }
    
    func upload() -> Bool {
        var check = false
        if authenticationAPI.isAuthenticated() {
            check = true
            let semaphore = DispatchGroup()
            
            let persons = personAPI.getAll()
            for person in persons {
                var request: URLRequest!
                
                var operation = 0
                if let s = synchronizationAPI.read(id: person.personId!) {
                    operation = s.operation
                }
                
                request = personRequest(identifier: person.personId!, operation: operation, person: person)
                
                guard request != nil else {
                    continue
                }
                
                request.addValue("Bearer \(authentication.token!)", forHTTPHeaderField: "Authorization")
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.timeoutInterval = 60
             
                let task = URLSession.shared.dataTask(with: request) { data, response, error in
                    semaphore.enter()
                    
                    guard data != nil else {
                        check = false

                        semaphore.leave()
                        return
                    }

                    let httpResponse = response as? HTTPURLResponse
                    guard httpResponse?.statusCode == 200 else {
                        check = false

                        semaphore.leave()
                        return
                    }

                    semaphore.leave()
                }
              
                task.resume()
                
                guard operation != -1 else {
                    continue
                }
                
                let appointments = appointmentAPI.getAllOfPerson(id: person.personId!)
                for appointment in appointments {
                    var operation = 0
                    if let s = synchronizationAPI.read(id: appointment.identifier!) {
                        operation = s.operation
                    }
                    
                    var request: URLRequest! = appointmentRequest(identifier: appointment.identifier!, operation: operation, appointment: appointment)
                    
                    guard request != nil else {
                        continue
                    }
                    
                    request.addValue("Bearer \(authentication.token!)", forHTTPHeaderField: "Authorization")
                    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                    request.timeoutInterval = 60
                    
                    let task = URLSession.shared.dataTask(with: request) { data, response, error in
                        semaphore.enter()
                        guard data != nil else {
                            check = false

                            semaphore.leave()
                            return
                        }

                        let httpResponse = response as? HTTPURLResponse
                        guard httpResponse?.statusCode == 200 else {
                            check = false

                            semaphore.leave()
                            return
                        }

                        semaphore.leave()
                    }
                
                    task.resume()
                }
                
                let updates = updateAPI.getAllOfPerson(id: person.personId!)
                for update in updates {
                    var operation = 0
                    if let s = synchronizationAPI.read(id: update.identifier!) {
                        operation = s.operation
                    }
                    
                    var request: URLRequest! = updateRequest(identifier: update.identifier!, operation: operation, update: update)
                    
                    guard request != nil else {
                        continue
                    }
                    
                    request.addValue("Bearer \(authentication.token!)", forHTTPHeaderField: "Authorization")
                    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                    request.timeoutInterval = 60
                    
                    let task = URLSession.shared.dataTask(with: request) { data, response, error in
                        semaphore.enter()
                        guard data != nil else {
                            check = false

                            semaphore.leave()
                            return
                        }

                        let httpResponse = response as? HTTPURLResponse
                        guard httpResponse?.statusCode == 200 else {
                            check = false

                            semaphore.leave()
                            return
                        }

                        semaphore.leave()
                    }
                
                    task.resume()
                }
                
                semaphore.wait()
            }
        }
        
        return check
    }

    func backgroundUpload() -> Bool {
        var check = false
        
        if authenticationAPI.isAuthenticated() {
            check = true
            let synchronizations = synchronizationAPI.getAll()

            let semaphore = DispatchGroup()

            for s in synchronizations {
                semaphore.enter()

                var request: URLRequest!
                
                let first = s.identifier!.startIndex
                let third = s.identifier!.index(s.identifier!.startIndex, offsetBy: 2)
                let id = "\(s.identifier![first..<third])"
                
                switch id {
                case "PE":
                    var person: Person? = nil
                    if s.operation != -1 {
                        person = personAPI.read(id: s.identifier!)
                    }
                    
                    request = personRequest(identifier: s.identifier!, operation: s.operation, person: person)
                case "AP":
                    var appointment: Appointment? = nil
                    if s.operation != -1 {
                        appointment = appointmentAPI.read(id: s.identifier!)
                    }
                    
                    request = appointmentRequest(identifier: s.identifier!, operation: s.operation, appointment: appointment)
                case "UP":
                    var update: Update? = nil
                    if s.operation != -1 {
                        update = updateAPI.read(id: s.identifier!)
                    }
                    
                    request = updateRequest(identifier: s.identifier!, operation: s.operation, update: update)
                default:
                    request = nil
                }
            
                guard request != nil else {
                    continue
                }
                
                request.addValue("Bearer \(authentication.token!)", forHTTPHeaderField: "Authorization")
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.timeoutInterval = 10
                
                let task = URLSession.shared.dataTask(with: request) { data, response, error in
                    guard data != nil else {
                        check = false

                        semaphore.leave()
                        return
                    }
                
                    let httpResponse = response as? HTTPURLResponse
                    guard httpResponse?.statusCode == 200 else {
                        check = false
                        
                        semaphore.leave()
                        return
                    }
                    
                    check = synchronizationAPI.delete(id: s.identifier!)
          
                    semaphore.leave()
                }
                
                task.resume()
            }
            
            semaphore.wait()
        }
        
        
        return check
    }
    
    func download() -> Bool {
        var check = false
        if authenticationAPI.isAuthenticated() {
            check = true
            let semaphore = DispatchSemaphore (value: 0)
            var request = URLRequest(url: URL(string: "\(server)/persons")!, timeoutInterval: Double.infinity)
            
            request.httpMethod = "GET"
            request.addValue("Bearer \(authentication.token!)", forHTTPHeaderField: "Authorization")
            request.timeoutInterval = 10
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data else {
                    check = false
                    
                    semaphore.signal()
                    return
                }
            
                let httpResponse = response as? HTTPURLResponse

                guard httpResponse?.statusCode == 200 else {
                    check = false
                    
                    semaphore.signal()
                    return
                }

                if let jsonArray = self.json_parseData(data) {
                    check = self.personsDataWriting(jsonArray: jsonArray)
                }
                semaphore.signal()
            }

            task.resume()
            semaphore.wait()
        }
        
        return check
    }
    
    @objc func backgroundSynchronization() -> Bool {
        return backgroundUpload() && download()
    }

    func synchronization() -> Bool {
        return upload() && download()
    }

    // funzione per la generazione del json a partire da un Data
    func json_parseData(_ data: Data) -> NSArray? {
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)
            
            return json as? NSArray
        } catch _ {
            return nil
        }
    }

    func personsDataWriting(jsonArray: NSArray) -> Bool {
        var check = true
        
        if jsonArray.count > 0 {
            for x in jsonArray {
                if let jsonPerson = x as? NSDictionary {
                    let new = PersonAttributes()

                    new.id = (jsonPerson["personId"] as! String)
                    new.name = jsonPerson["name"] as! String
                    new.dateOfBirth = timestampToDate(string: (jsonPerson["dateOfBirth"] as! String))!
                    new.gender = jsonPerson["gender"] as! String
                    new.color = colorAPI.stringToPersonColor(color: (jsonPerson["personColor"] as! String))
                    
                    var person: Person! = personAPI.read(id: new.id!)
                    if person != nil {
                        guard let _ = personAPI.update(attributes: new, person: person) else {
                            check = false
                            
                            continue
                        }
                    } else {
                        person = personAPI.create(attributes: new)
                        guard person != nil else {
                            check = false
                            
                            continue
                        }
                    }
                    
                    let jsonAppointments = jsonPerson["appointments"] as! NSArray
                    let jsonUpdates = jsonPerson["updates"] as! NSArray

                    check = check && appointmentsDataWriting(jsonArray: jsonAppointments, person: person)
                    check = check && updatesDataWriting(jsonArray: jsonUpdates, person: person)
                }
            }
        }
        
        return check
    }

    func appointmentsDataWriting(jsonArray: NSArray, person: Person) -> Bool {
        var check = true
        
        if jsonArray.count > 0 {
            for x in jsonArray {
                if let jsonAppointment = x as? NSDictionary {
                    let new = AppointmentAttributes()
                    
                    new.identifier = (jsonAppointment["identifier"] as! String)
                    new.name = jsonAppointment["name"] as! String
                    new.date = timestampToDate(string: (jsonAppointment["date"] as! String))!
                    new.checked = jsonAppointment["checked"] as! Bool
                    new.notes = jsonAppointment["notes"] as! String
                    new.notifications = jsonAppointment["notifications"] as! Bool
                    new.alerts = jsonAppointment["alerts"] as! [Int]
                    new.person = person
                    
                    if let appointment = appointmentAPI.read(id: new.identifier!) {
                        guard let _ = appointmentAPI.update(attributes: new, appointment: appointment) else {
                            check = false
                            
                            continue
                        }
                    } else {
                        guard let _ = appointmentAPI.create(attributes: new) else {
                            check = false
                            
                            continue
                        }
                    }
                }
            }
        }
        
        return check
    }

    func updatesDataWriting(jsonArray: NSArray, person: Person) -> Bool {
        var check = true
        
        if jsonArray.count > 0 {
            for x in jsonArray {
                if let jsonUpdate = x as? NSDictionary {
                    let identifier = (jsonUpdate["identifier"] as! String)
                    let name = jsonUpdate["name"] as! String
                    let date = timestampToDate(string: (jsonUpdate["date"] as! String))!

                    guard let _ = updateAPI.create(name: name, date: date, person: person, id: identifier) else {
                        check = false
                        
                        continue
                    }
                }
            }
        }
        
        return check
    }

    func personRequest(identifier: String, operation: Int, person: Person?) -> URLRequest? {
        var request: URLRequest!
        switch operation {
        case -1:
            request = URLRequest(url: URL(string: "\(server)/persons/delete?id=\(identifier)")!, timeoutInterval: Double.infinity)
            request.httpMethod = "POST"
        default:
            if person != nil {
                let parameters = "{\n        \"personId\": \"\(person!.personId!)\",\n        \"dateOfBirth\": \"\(dateToTimestamp(date: person!.dateOfBirth!)!)\",\n        \"gender\": \"\(person!.gender!)\",\n        \"name\": \"\(person!.name!)\",\n        \"personColor\": \"\(colorAPI.personColorToString(color: person!.color))\"\n}"
                let postData = parameters.data(using: .utf8)
                
                request = URLRequest(url: URL(string: "\(server)/persons/save")!, timeoutInterval: Double.infinity)
                request.httpMethod = "POST"
                request.httpBody = postData
            } else {
                return nil
            }
        }
        
        return request
    }

    func appointmentRequest(identifier: String, operation: Int, appointment: Appointment?) -> URLRequest? {
        var request: URLRequest!
        switch operation {
        case -1:
            request = URLRequest(url: URL(string: "\(server)/appointments/delete?id=\(identifier)")!, timeoutInterval: Double.infinity)
            request.httpMethod = "POST"
        default:
            if appointment != nil {
                let parameters = "{\n    \"identifier\": \"\(appointment!.identifier!)\",\n    \"name\": \"\(appointment!.name!)\",\n    \"date\": \"\(dateToTimestamp(date: appointment!.date!)!)\",\n    \"alert\": \(appointment!.alerts),\n    \"person\": {\"personId\": \"\(appointment!.person!.personId!)\"}\n}"
                let postData = parameters.data(using: .utf8)
                
                request = URLRequest(url: URL(string: "\(server)/appointments/save")!, timeoutInterval: Double.infinity)
                request.httpMethod = "POST"
                request.httpBody = postData
            } else {
                return nil
            }
        }
        
        return request
    }

    func updateRequest(identifier: String, operation: Int, update: Update?) -> URLRequest? {
        var request: URLRequest!
        switch operation {
        case -1:
            request = URLRequest(url: URL(string: "\(server)/updates/delete?id=\(identifier)")!, timeoutInterval: Double.infinity)
            request.httpMethod = "POST"
        default:
            if update != nil {
                let parameters = "{\n    \"identifier\": \"\(update!.identifier!)\",\n    \"name\": \"\(update!.name!)\",\n    \"date\": \"\(dateToTimestamp(date: update!.date!)!)\",\n    \"person\": {\n        \"personId\": \"\(update!.person!.personId!)\"}\n}"
                let postData = parameters.data(using: .utf8)
                
                request = URLRequest(url: URL(string: "\(server)/updates/save")!, timeoutInterval: Double.infinity)
                request.httpMethod = "POST"
                request.httpBody = postData
            } else {
                return nil
            }
        }
        
        return request
    }

    func timestampToDate(string: String) -> Date? {
        guard !string.isEmpty else {
            return nil
        }
        
        let dateFormatter = ISO8601DateFormatter()

        return dateFormatter.date(from: string)!
    }

    func dateToTimestamp(date: Date?) -> String? {
        guard date != nil else {
            return nil
        }
        
        let dateFormatter = ISO8601DateFormatter()

        return dateFormatter.string(from: date!)
    }
    
 
    func setOnBackgroundSynchronization() {
        if authenticationAPI.isAuthenticated() {
            if (backgroundTimer != nil && backgroundTimer!.isValid) {
                backgroundTimer!.invalidate()
            }
            
            backgroundTimer = Timer.scheduledTimer(timeInterval: 300, target: self, selector: #selector(backgroundSynchronization), userInfo: nil, repeats: true)
        }
    }
    
    func setOffBackgroundSynchronization() {
        backgroundTimer?.invalidate()
    }
}
